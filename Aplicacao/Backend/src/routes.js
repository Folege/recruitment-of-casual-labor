const express=require('express');
const multer=require('multer');
const {celebrate,Segments,Joi}= require('celebrate')//library for validation
const auth = require('./middleware/auth');

const authController = require('./controller/AuthController');
const multerConfig=require('./config/multer');
const userController=require('./controller/UserController')
const estivaController = require('./controller/EstivaController');
const dashboardController = require('./controller/DashbordController');
const userTipeController=require('./controller/UserTipeController');
const empresaController = require('./controller/EmpresaController');
const adminController = require('./controller/AdminController');

const routes=express.Router();

//Returns Candidaturas amounts
routes.get('/api/quantCandidaturas', auth, dashboardController.init);

//return notifications sent by users
routes.get(
  "/api/notificacoes",
  auth,
  celebrate({
    [Segments.QUERY]: Joi.object({
      anoMes: Joi.string().required(),
    }).unknown()
  }),
  dashboardController.notificacoes
);

//
routes.get(
  "/api/quantMes",
  auth,
  celebrate({
    [Segments.QUERY]: Joi.object({
      anoMes: Joi.string().required(),
    }).unknown()
  }),
  dashboardController.quantMes
);

//get max month in bd
routes.get("/api/getMaxMonth", auth, dashboardController.getMaxMonth );

//get min month in bd
routes.get("/api/getMinMonth", auth, dashboardController.getMinMonth);


//Route to list Estivas
routes.get('/api/list/:tipo',auth,
    celebrate({ 
        [Segments.PARAMS]:Joi.object().keys({
            tipo:Joi.string()
                    .valid('Pendente','Confirmada','Cancelada')
        }),

    }),
    estivaController.list
);

routes.get('/api/userList',auth,
    celebrate({
      [Segments.HEADERS]:Joi.object({
          tipo:Joi.number().required(),
      }).unknown()
    }),
    userController.list
);

routes.get('/api/getDesignacao',auth,
    celebrate({
      [Segments.HEADERS]:Joi.object({
          tipo:Joi.number().required(),
      }).unknown()
    }),
    userController.designacao
);

//Route to lock and un/lock an user acount
routes.put('/api/lock',auth,
    celebrate({
        [Segments.BODY]:Joi.object({
            id_usuario:Joi.number().required(),
            funcao:Joi.string().required()
        }).unknown()
    }),
    userController.lock
);


// route to register user
routes.post('/api/usuario'
    ,celebrate({
        [Segments.BODY]:Joi.object().keys({
            nome:Joi.string().required(),
            email:Joi.string().required().email(),
            userName:Joi.string().required(),
            id_tipo: Joi.number().required(),
            estadoConta:Joi.string().valid('Activa','Bloqueada')
        }),

        [Segments.HEADERS]:Joi.object({
            empresa_id:Joi.number().required()
        }).unknown()
    })
    ,userController.create
);

routes.put("/api/usuario/:id"
      ,auth,
      celebrate({
        [Segments.BODY]:Joi.object().keys({
          nome:Joi.string().required(),
          email:Joi.string().required().email(),
          userName:Joi.string().required(),
          password:Joi.string(),
          newPassword:Joi.string()
        })
      })
      ,userController.update
);

//@desc authenticate user and create token
//@acess public: no need for auth middleware
routes.post(
  "/api/login",
  celebrate({
    [Segments.BODY]: Joi.object().keys({
      userNameEmail: Joi.string().required().messages({
        "any.required": "Campo 'Email ou Nome de Usuário' é requerido",
      }),
      senha: Joi.string()
        .required()
        .pattern(new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$"))
        .messages({
          "string.pattern.base": "E-mail ou senha incorrectos. Insira suas informações de login novamente ou solicite um e-mail para obter acesso à sua conta.",
          "any.required": "Campo 'Senha' é requerido",
        }),
    }),
  }),
  authController.login
);

//@desc    get authenticated user
//@acess   private: must receive the authentication middleware by parameter
routes.get("/api/authUser", auth, authController.getAuthUser);

//@desc retrive fogotten passwword
//@acess public
routes.post(
  "/api/esqueceu-senha",
  celebrate({
    [Segments.BODY]: Joi.object().keys({
      userNameEmail: Joi.string().required().messages({
        "any.required": "Campo 'Email ou Nome de Usuário' é requerido",
      }),
    }),
  }),
  authController.forgotPassword
);

//@desc    verify if the password reset token as expired
//@acess   public
routes.get(
  "/api/password-reset-expires/:passwordResetToken",
  celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      passwordResetToken: Joi.string()
        .required()
        .pattern(new RegExp("^(?=.*?[a-z])(?=.*?[0-9]).{20,}$"))
        .messages({
          "string.pattern.base": "Token invalido.",
          "any.required": "Token reuerido.",
        }),
    }),
  }),
  authController.getPasswordResetExpired
);

//@desc    update password
//@acess   public
routes.post(
  "/api/nova-senha/:passwordResetToken",
  celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      passwordResetToken: Joi.string()
        .required()
        .pattern(new RegExp("^(?=.*?[a-z])(?=.*?[0-9]).{20,}$"))
        .messages({
          "string.pattern.base": "Token invalido.",
          "any.required": "Token reuerido.",
        }),
    }),
    [Segments.BODY]: Joi.object().keys({
      senha: Joi.string()
        .required()
        .pattern(new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$"))
        .messages({
          "string.pattern.base":
            "A senha deve: Ter pelo menos 8 caracteres; Ter pelo menos 1 letra (a, b, c...); Ter pelo menos 1 número (1, 2, 3...); Incluir caracteres em Maiúscula e Minúscula",
          "any.required": "Campo 'Senha' é requerido",
        }),
    }),
  }),
  authController.updatePassword
);

//route to upload the image to the server
routes.post('/api/upload'
    ,auth,
    multer(multerConfig).single('file')
    ,userController.fileConfig
);

routes.put('/api/upload/:idFicheiro'
      ,auth,multer(multerConfig).single('file')
      ,userController.updateFile
);

//update user by admin
routes.put(
  "/api/updateUser",
  auth,
  celebrate({
    [Segments.BODY]: Joi.object().keys({
      nome: Joi.string().required(),
      email: Joi.string().required().email(),
      userName: Joi.string().required(),
      tipoNome: Joi.string().required(),
      estadoConta: Joi.string().valid("Activa", "Bloqueada"),
      id_usuario: Joi.number().required(),
    }),
  }),
  adminController.updateUsuario
);

//delete user by admin
routes.delete(
  '/api/deleteUser/:idUsuario',
  auth,
  celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      idUsuario: Joi.number()
        .required()
        .messages({
          "any.required": "id é reuerido.",
        }),
    }),
  }),
  adminController.deleteUsuario
);

//get user tipe
routes.get('/api/getTipo',auth,userTipeController.index);

//add a new empresa
routes.post(
  "/api/empresa",
  auth,
  celebrate({
    [Segments.BODY]: Joi.object().keys({
      nome: Joi.string().required(),
      cidade: Joi.string().required(),
      avRua: Joi.string().required(),
      numeroEdificio: Joi.number().allow('', null),
      infoAdicional: Joi.string().allow('', null),
    }),
  }),
  empresaController.create
);

//get all empresas
routes.get('/api/empresa', auth, empresaController.list);

//get empresa by id
routes.get(
  '/api/getEmpresa/:id',
  auth,
  celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      id: Joi.number().required()
    })
  }),
  empresaController.getEmpresa
);

//add a new empresa
routes.put(
  "/api/empresa",
  auth,
  celebrate({
    [Segments.BODY]: Joi.object().keys({
      nome: Joi.string().required(),
      cidade: Joi.string().required(),
      avRua: Joi.string().required(),
      numeroEdificio: Joi.number().allow('',null),
      infoAdicional: Joi.string().allow('',null),
      id: Joi.number().required()
    }),
  }),
  empresaController.update
);

routes.get('/api/getTipo',auth,userTipeController.index);

routes.delete('/api/tipo/:id',auth,userTipeController.delete);
routes.put('/api/tipo/:id', 
  //auth,
  celebrate({
    [Segments.BODY]: Joi.object().keys({
      designacao: Joi.string().required(),
      roles: Joi.array().required(),
    })
  }),
  userTipeController.update
);

routes.get('/api/tipo/:id',auth,userTipeController.Show);

routes.post('/api/userTipe', 
  auth,
  celebrate({
    [Segments.BODY]: Joi.object().keys({
      designacao: Joi.string().required(),
      roles: Joi.array().required()
    })
  }),
  userTipeController.create
);

routes.get('/api/userTipeList', auth, userTipeController.list);

//delete empresa
routes.delete(
  '/api/deleteEmpresa/:idEmpresa'
  ,auth
  ,celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      idEmpresa: Joi.number()
    }),
  }),
  empresaController.delete
);

//get oser tipe roles by id
routes.get(
  '/api/getRoles/:idTipo',
  auth,
  celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      idTipo: Joi.number()
        .required()
        .messages({
          "any.required": "id é reuerido.",
        }),
    }),

  }),
  userTipeController.getTipeRolesById
);

routes.post(
  '/api/notificar',
  auth,
  celebrate({
    [Segments.BODY]: Joi.object().keys({
      data: Joi.string().required(),
      hora: Joi.string().required(),
      empresa: Joi.string().required(),
      endereco: Joi.array().required(),
      idEstivas: Joi.array().required(),
      idUsuario: Joi.number().required(),
    }),
  }),
  userController.notificar
);

routes.post(
  "/api/responderNotificacao",
  estivaController.responderNotificacao
);

routes.get('/api/candidaturaUSSD', estivaController.getMessageUSSD);
routes.post('/api/candidaturaUSSD', estivaController.createUSSD);


module.exports=routes;