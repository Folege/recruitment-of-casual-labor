require('dotenv').config();
const express= require('express');
const cors=require('cors');
const path =require('path')
const routes=require('./routes');
const {errors}=require('celebrate')

const app =express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(routes);
app.use('/files',express.static(path.resolve(__dirname,'..','tmp','uploads')));
app.use(errors());

app.listen(3333);