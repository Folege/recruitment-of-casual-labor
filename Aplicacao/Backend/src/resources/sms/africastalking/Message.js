const config = require("config");
const superagent = require("superagent");

///define credentias
const credentials = {
    apiKey: config.get("africastalking.apiKey"),
    username: config.get("africastalking.username"), // username is sandbox for sandbox applications
};
const AT = require("africastalking")(credentials);

// initialize AT's SMS service
const sms = AT.SMS;

class Message {
    constructor() {}

    sendMessage(to, message, from) {
        // create const options with fields to and message
        const options = {
            to: to,
            message: message,
            from: from,
        };   
        // That's it. AT will then send your SMSs to your Simulators
        sms.send(options).then(info => {
            // return information from Africa's Talking
            console.log(info);
            res.json(info);
        }).catch(err => {
            console.log(err);
        }); 
    }

    // Fetch all messages using a recursive function
    getMessagesRecursively(lastReceivedId) {
        const checkForMoreMessages = (responses) => {
            let messages = responses.SMSMessageData.Messages;

            // No more messages to fetch
            if (messages.length === 0) return "Done";

            // There are more messages
            messages.forEach((message) => {
                console.log(message);
                
                /*Notifica estivas na bd da africas talking
                superagent
                  .post("http://localhost:3333/api/responderNotificacao")
                  .send(message) // sends a JSON post body
                  .set("X-API-Key", "foobar")
                  .set("accept", "json")
                  .end((err, res) => {
                    console.log(res.text);
                  });
                */

                // Reassign the lastReceivedId
                lastReceivedId = message.id;
            });
            return this.getMessagesRecursively(lastReceivedId);
        }

        return sms.fetchMessages({ lastReceivedId }).then(checkForMoreMessages);
    }
}

module.exports = new Message();