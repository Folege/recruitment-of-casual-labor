const config = require("config");
const accountSid = config.get("twilio.accountSid");
const authToken = config.get("twilio.authToken");
const client = require("twilio")(accountSid, authToken);
const MessaginResponse = require("twilio").twiml.MessagingResponse;

class Message {
    constructor() {}

    sendMessage(to, body, from) {
        client.messages
          .create({
            body: body,
            from: from,
            to: to,
          })
          .then((message) => console.log(message.sid));
    }

    sendMessageResponse(response, message) {
      const twiml = new MessaginResponse();
      twiml.message(message);

      response.writeHead(200, {'Content-Type': 'text/xml'});
      response.end(twiml.toString());
    }
}

module.exports = new Message();