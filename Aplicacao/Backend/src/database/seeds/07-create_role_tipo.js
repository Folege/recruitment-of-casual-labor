
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('role_tipo').del()
    .then(async function () {
      
      const roles= await knex('role').select('id');
      const id_tipos= await knex('tipo').select('id as id_tipo')
                        .whereIn('designacao',['Root','Administrador'])
                        
      
      const role_tipo1 = roles.map(role => {
        return {
            id_tipo:id_tipos[0].id_tipo,
            id_role: role.id
        }
      });

      const role_tipo2 = roles.map(role => {
        return {
            id_tipo:id_tipos[1].id_tipo,
            id_role: role.id
        }
      });

      role_tipo=role_tipo1.concat(role_tipo2);

      // Associate all roles in tipe `Administrador`
      return knex('role_tipo').insert(role_tipo);
    });
};
