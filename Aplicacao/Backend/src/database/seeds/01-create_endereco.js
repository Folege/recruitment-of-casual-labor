
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('endereco').del()
    .then(function () {
      // Inserts seed entries
      return knex('endereco').insert([
        {
          avRua:'Avenida Martires de Inhaminga',
          cidade:'Maputo'
        }
      ]);
    });
};
