const URL_BASE='http://localhost:3333';
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('ficheiro').del()
    .then(function () {
      // Inserts seed entries
      return knex('ficheiro').insert([
        {
          nomeImagem:'ImagemPadrao.png',
          nomeImagemOriginal:'ImagemPadrao.png',
          url:`${URL_BASE}/files/${'ImagemPadrao.png'}`
        }
      ]);
    });
};
