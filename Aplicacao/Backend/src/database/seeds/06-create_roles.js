
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('role').del()
    .then(function () {
      // Inserts seed entries
      return knex('role').insert([
        {designacao:'PAINEL_ADMINISTRATIVO'},
        {designacao:'CONFIGURACOES'},
        {designacao:'GERIR_CANDIDATURAS'},
        {designacao:'GERIR_USUARIOS'},
        {designacao:'VISUALIZAR_USUARIOS'},
        {designacao:'GERIR_ADMINISTRADORES'},
      ]);
    });
};
