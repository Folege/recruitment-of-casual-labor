const bcrypt=require('bcryptjs');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('usuario').del()
    .then(async function () {
      const salt =  await bcrypt.genSalt(10);
      const password = await bcrypt.hash('Restiva@1212', salt);

      const {id}= await knex('tipo').select('id').where('designacao','root').first();
      // Inserts seed entries
      return knex('usuario').insert([
        {
          nome:'Super Administrador',
          email:'restiva@gmail.com',
          userName:'super.administrador',
          estadoConta:'Activa',
          password,
          id_tipo:id,
          id_empresa:1,
          id_Ficheiro:1
        },
      ]);
    });
};
