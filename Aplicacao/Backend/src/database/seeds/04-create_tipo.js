
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('tipo').del()
    .then(function () {
      // Inserts seed entries
      return knex('tipo').insert([
        {designacao:'Root'},
        {designacao:'Administrador'}
      ]);
    });
};
