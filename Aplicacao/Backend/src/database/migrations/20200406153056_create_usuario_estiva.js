
exports.up = function(knex) {
    return knex.schema.createTable('usuario_estiva', function (table) {
        table.increments('id').primary();
        table.datetime('dataNotificacao', {precision: 2}).notNullable();
        table.datetime('dataInducao', {precision: 2}).notNullable();
        table.text('mensagem').notNullable();
        table.integer('id_estiva').notNullable();
        table.integer('id_usuario');

        table.foreign('id_usuario').references('id').inTable('usuario').onDelete('SET NULL');
        table.foreign('id_estiva').references('id').inTable('estiva').onDelete('CASCADE');
    })
};

exports.down = function(knex) {
    knex.schema.dropTable('usuario_estiva');
};
