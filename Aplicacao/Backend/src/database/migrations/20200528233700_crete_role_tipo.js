
exports.up = function(knex) {
    return knex.schema.createTable('role_tipo', function (table) {
        table.integer('id_tipo');
        table.integer('id_role');
      
        table.primary(['id_role','id_tipo']);
        table.foreign('id_role').references('id').inTable('role').onDelete('CASCADE').onUpdate('CASCADE');
        table.foreign('id_tipo').references('id').inTable('tipo').onDelete('CASCADE').onUpdate('CASCADE');
    })
  
};

exports.down = function(knex) {
    knex.schema.dropTable('role_tipo');
};
