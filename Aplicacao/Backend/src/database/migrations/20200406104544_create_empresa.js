
exports.up = function(knex) {
    return knex.schema.createTable('empresa', function (table) {
        table.increments('id').primary();
        table.string('nome').notNullable();
       
        table.integer('id_endereco').notNullable();
        table.foreign('id_endereco').references('id').inTable('endereco');
     })
};

exports.down = function(knex) {
    knex.schema.dropTable('empresa');
};
