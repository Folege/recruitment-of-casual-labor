
exports.up = async function(knex) {
    return knex.schema.createTable('usuario', function (table) {
        table.increments('id').primary();
        table.string('nome').notNullable();
        table.string('email').unique();
        table.string('userName').notNullable();
        table.string("estadoConta").notNullable();
        table.string('password').notNullable();
        table.string('passwordResetToken');
        table.datetime('passwordResetExpires'); 
        table.integer('id_empresa');
        table.integer('id_tipo').notNullable();
        table.integer('id_Ficheiro');

        table.foreign('id_empresa').references('id').inTable('empresa').onDelete('CASCADE');
        table.foreign('id_tipo').references('id').inTable('tipo');
        table.foreign('id_Ficheiro').references('id').inTable('ficheiro').onDelete('SET NULL');
    })
};

exports.down = function(knex) {
    knex.schema.dropTable('analistaRh');
};
