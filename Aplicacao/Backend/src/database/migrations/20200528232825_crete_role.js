
exports.up = function(knex) {
    return knex.schema.createTable('role', function (table) {
        table.increments('id').primary();
        table.string('designacao').notNullable();
     })
};

exports.down = function(knex) {
    knex.schema.dropTable('role');
};
