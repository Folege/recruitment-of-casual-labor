
exports.up = function(knex) {
    return knex.schema.createTable('tipo', function (table) {
        table.increments('id').primary();
        table.enu('designacao',['normal','Administrador']).notNullable();
     })
};

exports.down = function(knex) {
    knex.schema.dropTable('tipo');
};
