
exports.up = function(knex) {
    return knex.schema.createTable('endereco', function (table) {
        table.increments('id').primary();
        table.string('avRua').notNullable();
        table.string('cidade').notNullable();
        table.integer('numeroEdificio').nullable();
        table.string('infoAdicional');
    })
};

exports.down = function(knex) {
    knex.schema.dropTable('endereco');
};
