 
exports.up = function(knex) {

    return knex.schema.createTable('estiva', function (table) {
       table.increments('id').primary();
       table.string('nome').notNullable();
       table.string('nr_BI',13).notNullable().unique();
       table.integer('telefone').notNullable();
       table.enu('estadoCandidatura',['pendente','confirmada','cancelada']).notNullable();
       table.datetime('dataCandidatura').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
       table.datetime('dataCancelamento');
       table.datetime('dataConfirmacao');
    })
  
};

exports.down = function(knex) {
  knex.schema.dropTable('estiva');
};
