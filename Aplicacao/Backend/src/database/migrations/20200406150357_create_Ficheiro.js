
exports.up = function(knex) {
    return knex.schema.createTable('ficheiro', function (table) {
        table.increments('id').primary();
        table.string('nomeImagem').notNullable();
        table.string('nomeImagemOriginal').notNullable();
        table.string('url').notNullable()
    })
};

exports.down = function(knex) {
    knex.schema.dropTable('Ficheiro');
};

