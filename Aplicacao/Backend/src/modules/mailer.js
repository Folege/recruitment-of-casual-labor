const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const path = require('path');
const config = require('config');


const transport = nodemailer.createTransport({
  host: config.get("mail.host"),
  port: config.get("mail.port"),
  auth: {
    user: config.get("mail.auth.user"),
    pass: config.get("mail.auth.pass"),
  },
});


transport.use('compile', hbs({
  viewPath: path.resolve('./src/resources/mail'),
  extName: '.html',
  viewEngine: {
    extName: '.hbs',
    partialsDir: path.resolve('./src/resources/mail'),
    layoutsDir: path.resolve('./src/resources/mail'),
    defaultLayout: 'email.body.hbs',
  }
}));

module.exports = transport;