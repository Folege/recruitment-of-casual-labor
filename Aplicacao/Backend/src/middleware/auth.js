const jwt = require("jsonwebtoken");
const config = require("config");

module.exports = function (req, res, next) {
  //get token from header
  const token = req.header("x-auth-token");

  //check no token
  if (!token) {
    return res
      .status(401)
      .json({ msg: "Token não encontrado, autorização negada." });
  }

  //verfy token
  try {
    const decoded = jwt.verify(token, config.get("jwtSecret")); //defined in default.json file
    req.user = decoded.user;
    next();
  } catch (error) {
    return res.status(401).json({ msg: "Token invalido." });
  }
};
