const connection = require('../database/connection');

module.exports = {
  async init(request, response) {
    try {
      const [estivaP] = await connection("estiva")
        .where("estadoCandidatura", "Pendente")
        .orWhere("estadoCandidatura","Indisponivel")
        .count("id as Pendente");

      const [estivaC] = await connection("estiva")
        .where("estadoCandidatura", "Confirmada")
        .count("id as Confirmada");

      const [estivaCa] = await connection("estiva")
        .where("estadoCandidatura", "Cancelada")
        .count("id as Cancelada");

      return response.json({Pendente:estivaP.Pendente, Confirmada: estivaC.Confirmada, Cancelada: estivaCa.Cancelada});
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server error: " + error.message);
    }
  },

  async notificacoes(req, res) {
    const { anoMes } = req.query;
    //console.log(anoMes);
    try {
      const notif = await connection("usuario_estiva as ue")
        .select(
          "ue.dataNotificacao",
          "ue.dataInducao",
          "e.nome as estiva",
          "e.estadoCandidatura",
          "e.nr_BI",
          "a.nome as remetente"
        )
        .join("estiva as e", "e.id", "ue.id_estiva")
        .join("usuario as a", "a.id", "ue.id_usuario")
        .where("ue.dataNotificacao", "like", anoMes + "%");

      res.send(notif);
    } catch (error) {
      console.log(error.message);
      res.status(500).send("Server error: " + error.message);
    }
  },

  async quantMes(req, res) {
    const { anoMes } = req.query;
    try {
        //seleciona estvas notificados mas que nao respoderam 
      const estivaP = await connection("estiva as e")
        .where("e.estadoCandidatura", "NaoConfirmada")
        .where("ue.dataNotificacao", "like", anoMes + "%")
        .count("e.id as NaoConfirmada")
        .join("usuario_estiva as ue", "ue.id_estiva", "e.id");

      const estivaC = await connection("estiva as e")
        .where("e.estadoCandidatura", "Confirmada")
        .where("ue.dataNotificacao", "like", anoMes + "%")
        .count("e.id as Confirmada")
        .join("usuario_estiva as ue", "ue.id_estiva", "e.id");

      const estivaCa = await connection("estiva as e")
        .where("e.estadoCandidatura", "Indisponivel")
        .where("ue.dataNotificacao", "like", anoMes + "%")
        .count("e.id as Indisponivel")
        .join("usuario_estiva as ue", "ue.id_estiva", "e.id");

      return res.json(estivaP.concat(estivaC).concat(estivaCa));
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server error: " + error.message);
    }
  },

  async getMaxMonth(req, res) {
    try {
      const [maxDate]  = await connection("usuario_estiva").max('dataNotificacao as max');
      var d = new Date(maxDate.max);
      const zeroPad = (num, places) => String(num).padStart(places, '0'); //changes number format from 1 to 001

      return res.send({maxMonth: d.getFullYear()+'-'+zeroPad(d.getMonth()+1, 2)});
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server error: " + error.message);
    }
  },

  async getMinMonth(req, res) {
    try {
      const [minDate] = await connection("usuario_estiva").min('dataNotificacao as min');
      var d = new Date(minDate.min);
      const zeroPad = (num, places) => String(num).padStart(places, '0'); //changes number format from 1 to 001

      return res.send({ minMonth: d.getFullYear() + '-' + zeroPad(d.getMonth() + 1, 2) });
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server error: " + error.message);
    }
  },

};