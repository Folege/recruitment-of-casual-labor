const connection=require('../database/connection');
const RandExp=require('randexp');
const bcrypt = require('bcryptjs');
const fs=require('fs');
const path=require('path');
const config = require("config");
const mailer = require("../modules/mailer");
var dateFormat = require("dateformat");
const messages = require("../resources/sms/africastalking/Message");

let file=null;
let id_Ficheiro=null;
let nomeImagem;
let nomeImagemOriginal;



module.exports = {
  async fileConfig(req, res) {
    file = req.file;

    if (!file) {
      //check if the standard image is stored in database
      id_Ficheiro = await connection("ficheiro")
        .select("id")
        .where("nomeImagem", "ImagemPadrao.png")
        .first();
      if (!id_Ficheiro) {
        nomeImagem = "ImagemPadrao.png";
        nomeImagemOriginal = "ImagemPadrao.png";
        const url = `${process.env.APP_URL}/files/${nomeImagem}`;
        [id_Ficheiro] = await connection("ficheiro")
          .returning("id")
          .insert({ nomeImagem, nomeImagemOriginal, url });
      }
      id_Ficheiro = id_Ficheiro.id;
    }

    res.send();
  },
  async create(req, res) {
    trxProvider = connection.transactionProvider();
    const { nome, email, userName, id_tipo, estadoConta } = req.body;
    const id_empresa = req.headers.empresa_id;

    try {
      const trx = await trxProvider(); //start the transaction
      //check if userName ou email exists
      const user = await trx("usuario")
        .select("id")
        .where("userName", userName)
        .orWhere("email", email)
        .first();

      if (user) {
        return res.status(400).json({
          message:
            "Nome de usuário ou e-mail já foi cadastrado, tente outro nome ou email.",
        });
      }

      //when the image was loaded from the user
      if (file) {
        nomeImagem = file.filename;
        nomeImagemOriginal = file.originalname;
        const url = `${process.env.APP_URL}/files/${nomeImagem}`;
        [id_Ficheiro] = await trx("ficheiro")
          .returning("id")
          .insert({ nomeImagem, nomeImagemOriginal, url });
      }

      const passwordGen = new RandExp(/^[A-Z][A-Za-z\d]{6,8}\d$/).gen();
      const salt = await bcrypt.genSalt(10);
      const password = await bcrypt.hash(passwordGen, salt);
      const [id] = await trx("usuario")
        .returning("id")
        .insert({
          nome,
          email,
          userName,
          password,
          id_empresa,
          id_tipo,
          estadoConta,
          id_Ficheiro,
        });
      file = null;

      const restivaHost = config.get("restiva.host"); //get frontend server host
      const restivaPort = config.get("restiva.port"); //get frontend server port
      const restivaEmail = config.get("restiva.email"); //get restiva email

      await mailer.sendMail(
        {
          to: email,
          from: restivaEmail,
          subject: "[RESTIVA] Cadastro na plataforma Restiva",
          template: "auth/dadosAcesso",
          context: { nome, restivaHost, restivaPort, passwordGen, userName },
        },
        async (error) => {
          if (error) {
            return res
              .status(400)
              .send({
                message: `Não foi possível enviar email para o usuario, por favor tente novamente. ${error}`,
              });
          }
        }
      );
      await trx.commit();
      return res.json({ id });
    } catch (error) {
      //Remove the previously stored image in case of an error
      if (file) {
        fs.unlink(
          path.resolve(__dirname, "..", "..", "tmp", "uploads", file.filename),
          function (err) {
            if (err) throw err;
            // if no error, file has been deleted successfully
            console.log("File deleted!");
          }
        );
      }
      console.log(error);
      return res.status(500).send({ message: error.message });
    }
  },

  async list(request, response) {
    try {
      const id_empresa = request.user.id_empresa;
      const id_tipo = request.headers.tipo;

      const tipo = await connection("tipo")
        .select("designacao")
        .where("id", id_tipo)
        .first();

      if (tipo.designacao == "Root" || tipo.designacao == "Administrador") {
        const usuario = await connection("usuario as u")
          .select(
            "u.id",
            "u.userName",
            "u.nome",
            "u.email",
            "u.estadoConta",
            "e.nome as empresa",
            "t.designacao as tipo",
            "f.url"
          )
          .join("tipo as t", "t.id", "u.id_tipo")
          .join("empresa as e", "e.id", "u.id_empresa")
          .join("ficheiro as f", "f.id", "u.id_Ficheiro");
        return response.json(usuario);
      } else {
        const usuario = await connection("usuario as u")
          .select(
            "u.id",
            "u.userName",
            "u.nome",
            "u.email",
            "u.estadoConta",
            "e.nome as empresa",
            "t.designacao as tipo",
            "f.url"
          )
          .join("tipo as t", "t.id", "u.id_tipo")
          .join("empresa as e", "e.id", "u.id_empresa")
          .join("ficheiro as f", "f.id", "u.id_Ficheiro")
          .where("u.id_empresa", id_empresa);
        return response.json(usuario);
      }
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server error: " + error.message);
    }
  },

  async designacao(request, response) {
    try {
      const id_tipo = request.headers.tipo;

      const tipo = await connection("tipo")
        .select("designacao")
        .where("id", id_tipo)
        .first();

      return response.json(tipo.designacao);
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server error: " + error.message);
    }
  },

  async update(req, res) {
    const { nome, email, userName, password, newPassword } = req.body;

    try {
      const userVerify = await connection("usuario")
        .select("id")
        .whereNot("id", req.params.id)
        .andWhere("email", email)
        .first();

      if (userVerify) {
        return res
          .status(400)
          .json({
            message:
              "Este email foi cadastrado por outro usuario, tente um email diferente.",
          });
      }

      if (password) {
        const user = await connection("usuario")
          .select("password")
          .where("id", req.params.id)
          .first();

        const salt = await bcrypt.genSalt(10);
        const newPassword2 = await bcrypt.hash(newPassword, salt);
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
          return res
            .status(400)
            .json({ message: "Senha incorrecta. tente novamente!" });
        } else {
          await connection("usuario")
            .update({
              nome,
              email,
              userName,
              password: newPassword2,
            })
            .where("id", req.params.id);

          return res.send();
        }
      } else {
        await connection("usuario")
          .update({
            nome,
            email,
            userName,
          })
          .where("id", req.params.id);

        return res.send();
      }
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },

  async updateFile(req, res) {
    file = req.file;
    const url = `${process.env.APP_URL}/files/${file.filename}`;
    try {
      const id_ficheiro = req.params.idFicheiro;
      const img = await connection("ficheiro")
        .select("nomeImagem")
        .where("id", id_ficheiro)
        .first();
      await connection("ficheiro")
        .update({
          nomeImagem: file.filename,
          nomeImagemOriginal: file.originalname,
          url,
        })
        .where("id", id_ficheiro);

      //delete the old image on the service
      fs.unlink(
        path.resolve(__dirname, "..", "..", "tmp", "uploads", img.nomeImagem),
        function (err) {
          if (err) throw err;
          //console.log('File deleted!');
        }
      );

      res.send();
    } catch (error) {
      res.status(500).send("Server error: " + error.message);
    }
  },

  async lock(request, response) {
    try {
      const id_usuario = request.body.id_usuario;
      const funcao = request.body.funcao;

      const usuario = await connection("usuario")
        .update({
          estadoConta: funcao,
        })
        .where("id", id_usuario);

      return response.json(usuario);
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server error: " + error.message);
    }
  },

  async notificar(req, res) {
    const { data, hora, empresa, endereco, idEstivas, idUsuario } = req.body;
    trxProvider = connection.transactionProvider();

    try {
        const trx = await trxProvider(); //start the transaction

        for (var i = 0; i < idEstivas.length; i++) {
            const estiva = await trx("estiva")
                .returning(['nome', 'nr_BI', 'telefone'])
                .update({ estadoCandidatura: "NaoConfirmada" })
                .where("id", idEstivas[i]);

            const msg = `${estiva[0].nome} com número de BI ${estiva[0].nr_BI}, foi solicitado para finalizar a sua candidatura.
            \nDADOS DA SOLICITAÇÃO 
            Empresa: ${empresa}
            Data: ${dateFormat(data,"dd/mm/yyyy")}   
            Hora: ${hora}
            Endereço: ${endereco}
            \nDigite *${config.get("africastalking.shortCode")}# para indicar sua disponibilidade.`;

            await trx("usuario_estiva").insert({
                dataNotificacao: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss"),
                dataInducao: dateFormat(
                Date.parse(data + " " + hora),
                "yyyy-mm-dd HH:MM:ss"
                ), //converts string => seconds => date
                mensagem: msg,
                id_estiva: idEstivas[i],
                id_usuario: idUsuario,
            });

            messages.sendMessage('+258'+estiva[0].telefone, msg, config.get("africastalking.shortCode"));
        }
        await trx.commit();
        
        if (i === 1)
            res.send({ message: `${i} Candidato notificado com sucesso.` });
        else
            res.send({ message: `${i} Candidatos notificados com sucesso.` });
    } catch (error) {
        console.log(error.message);
        res.status(500).send("Ocorreu um erro, tente novamente!");
    }
  },
};
