const connection = require('../database/connection');

module.exports = {
    async create(req, res) {
        trxProvider=connection.transactionProvider();
        const {designacao, roles}=req.body;
        try{
            const trx = await trxProvider();//start the transaction
            //check if userName ou email exists
            const tipouser = await trx("tipo")
            .select("id")
            .where("designacao",designacao)
            .first();

            if (tipouser) { 
                return res
                    .status(400)
                    .send({
                        message: `Já existe um tipo de usuario cadastrado com o nome "${designacao}".`,
                      });
            }

            const [id] = await trx('tipo').returning('id')
                        .insert({designacao});
            const id_tipo = id;

            for (let index = 0; index < roles.length; index++) {
               const id_ro =  await trx('role').select('id').where('designacao',roles[index]).first();
               const id_role = id_ro.id;
               await trx('role_tipo').insert({id_tipo,id_role});  
                
            }

            await trx.commit();
            return res.send({message: `Tipo de usuario "${designacao}" cadastrado com sucesso.`})
       
        }catch(error){
            console.log(error);
            return res.status(500).send({'erro':error.message});
        };
    },
    async index(req,res){
        
        let tipo=await connection('tipo').select('*').whereNot('designacao','root');
        const {designacao}=await connection('tipo').select('designacao').where('id',req.user.id_tipo).first();

        if(designacao!="Root"){
            tipo=tipo.filter(item=>item.designacao!= "Administrador");
        }

       
        return res.json(tipo);
    },

    async delete(req,res){

        const user=await connection('usuario').select('id').where('id_tipo',req.params.id).first();

        if(user){
            return res.status(401).json({message:"Este tipo nao pode ser removido pois existem usuarios associados"})
        }
        
        await connection('tipo').where('id',req.params.id).delete();
        return res.status(204).send();
    },
    
    async list (req, res) {
        trxProvider=connection.transactionProvider();
        
        try {
            const trx = await trxProvider();
            const tipo=await connection('tipo as t').
                select(
                    't.*',
                )
                .join('role_tipo as rl', 't.id','rl.id_tipo')
                .groupBy('t.id','t.designacao');

            let roles = [];
            for (let index = 0; index < tipo.length; index++) {

                const role =  await trx('role as r')
                    .select('r.designacao as roles',
                            'rl.id_tipo')
                    .join('role_tipo as rl', 'r.id','rl.id_role')
                    .where('rl.id_tipo',tipo[index].id);
                
                roles = roles.concat({tipo:tipo[index],roles:role});     
            }
            await trx.commit();
            return res.json(roles);

        } catch (error) {
            console.error(error.message);
            res.status(500).send("Server error: " + error.message); 
        }
    },

    async getTipeRolesById(req, res) {
        const { idTipo } = req.params;
        try {
            const roles = await connection('role_tipo as rt')
                .select('r.designacao')
                .join('role as r', 'r.id', 'rt.id_role')
                .where('rt.id_tipo', idTipo);

            if (!roles[0]) {
                return res.status(404).send({ message: "Não existem roles associadas a este tipo usuário." });
            }

            return res.send(roles);
        } catch (error) {
            console.error(error.message);
            res.status(500).send("Server error: " + error.message);
        }
    },

    async Show (req,res){
        const id =req.params.id;
        const tipo=await connection('tipo').where('id',id).select('*').first();

        if(!tipo){
            return res.status(400).json({message:'tipo de usuario nao encontrado'});
        }
        const roles = await connection('role')
                    .join('role_tipo','role.id','=','role_tipo.id_role')
                    .where('role_tipo.id_tipo',id)
                    .select('role.designacao');
        return res.json({tipo,roles});
    },

    async update(req,res){
        const {designacao,roles}=req.body;
        const id_tipo=req.params.id;

        try {
            const trx = await connection.transaction();

            const tipouser = await trx("tipo")
                .select("id")
                .where("designacao", designacao)
                .whereNot("id",id_tipo)
                .first();

            if (tipouser) {
                return res
                    .status(400)
                    .send({
                        message: `Já existe um tipo de usuario cadastrado com o nome "${designacao}".`,
                    });
            }

            const roles_ids = await trx('role').whereIn('designacao', roles).select('id');
            await trx('tipo').update({ 'designacao': designacao }).where('id', id_tipo);

            const role_tipo = roles_ids.map(role => {
                return {
                    id_tipo,
                    id_role: role.id
                }
            });
            await trx('role_tipo').delete().where('id_tipo', id_tipo);
            await trx('role_tipo').insert(role_tipo);

            await trx.commit();

            return res.json(role_tipo);
        } catch (error) {
            console.error(error.message);
            res.status(500).send("Server error: " + error.message);  
        } 
    }
    

}