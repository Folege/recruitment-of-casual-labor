const connection = require("../database/connection");

module.exports = {

    async create(req, res) {
        const { nome, cidade, avRua, numeroEdificio, infoAdicional} = req.body;
        try {
            const id = await connection('empresa')
                .select('id')
                .where('nome', nome)
                .first();
           
            if(id) {
               return res
                 .status(400)
                 .send({
                   message: `Já existe uma empresa cadastrada com o nome "${nome}".`,
                 });
           } 

            const [idEndereco] = await connection('endereco')
                .returning('id')
                .insert({cidade, avRua, numeroEdificio, infoAdicional});
    
            await connection('empresa').insert({nome, id_endereco: idEndereco})

            return res.send({message: `Empresa "${nome}" cadastrada com sucesso.`})
        } catch (error) {
            console.log(error);
            return res.status(500).send({ erro: error.message });
        }

    },

    async list(req, res) {
        try {
            const empresas = await connection('empresa as e')
                .select(
                'e.id',
                'e.nome',
                'end.cidade',
                'end.avRua',
                'end.numeroEdificio',
                'end.infoAdicional'
                )
                .join('endereco as end', 'end.id', 'e.id_endereco');
            
            return res.send(empresas);
        } catch (error) {
           console.log(error);
           return res.status(500).send({ erro: error.message }); 
        }
    },

    async getEmpresa(req, res) {
        const { id } = req.params;
        try {
            const empresa = await connection('empresa as e')
                .select(
                    'e.id',
                    'e.nome',
                    'end.cidade',
                    'end.avRua',
                    'end.numeroEdificio',
                    'end.infoAdicional'
                )
                .join('endereco as end', 'end.id', 'e.id_endereco')
                .where('e.id', id)
                .first();

            if (!empresa) {
                return res.status(404).json({ message: "Empresa não encotrada." })
            }

            return res.send(empresa);
        } catch (error) {
            console.log(error);
            return res.status(500).send({ erro: error.message });
        }
    },

    async update(req, res) {
        const { nome, cidade, avRua, numeroEdificio, infoAdicional, id } = req.body;
        try {
            const idEmp = await connection('empresa')
                .select('id')
                .where('nome', nome)
                .first();

            if ((idEmp) && (idEmp.id !== id)) {
                return res
                    .status(400)
                    .send({
                        message: `Já existe uma empresa cadastrada com o nome "${nome}".`,
                    });
            }

            const dados = await connection('empresa')
                .returning(['id_endereco','nome'])
                .update({nome})
                .where('id', id);
            
            if (!dados[0]) {
                return res.status(404).json({ message: "Empresa não encotrada." })
            }

            await connection('endereco')
                .update({ cidade, avRua, numeroEdificio, infoAdicional })
                .where('id', dados[0].id_endereco);
            
            return res.send({message:`Dados da empresa "${dados[0].nome}" actualizados com sucesso.`});
        } catch (error) {
            console.log(error);
            return res.status(500).send({ erro: error.message });
        }
    },
    
    async delete(req, res) {
        const { idEmpresa } = req.params; 

        try {
            
            const idEndereco = await connection('empresa')
                .where('id', idEmpresa)
                .select('id_endereco')
                .first();

            if(!idEndereco) {
                return res.status(404).json({ message: "Empresa não encontrada."})
            }

            await connection('empresa')
                .where('id', idEmpresa)
                .del();
            
            await connection('endereco')
                .where('id', idEndereco.id_endereco)
                .del();

            return res.send({ message: "Empresa(s) removida(s) com sucesso."});
        } catch (error) {
            console.log(error.message);
            res.status(500).send("Server error: " + error.message);
        }
    }

}