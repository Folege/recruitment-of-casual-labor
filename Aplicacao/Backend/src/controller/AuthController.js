const jwt = require("jsonwebtoken");
const config = require("config");
const bcrypt = require("bcryptjs");
const crypto = require("crypto");
const mailer = require("../modules/mailer")

const connection = require("../database/connection");

module.exports = {
  //@desc authenticate user and create token
  async login(req, res) {
    const { userNameEmail, senha } = req.body;

    try {
          var usuario = await connection("usuario")
            .select("*")
            .where("userName", userNameEmail)
            .first();

          //check if user exists
          if (!usuario) {
            usuario = await connection("usuario")
              .select("*")
              .where("email", userNameEmail)
              .first();
            if (!usuario) {
              return res
                .status(400)
                .json({ message: 
                  "E-mail ou senha incorrectos. Insira suas informações de login novamente ou solicite um e-mail para obter acesso à sua conta."  
                });
            }
          }

          //copare password
          const isMatch = await bcrypt.compare(senha, usuario.password);
          if (!isMatch) {
            return res
              .status(400)
              .json({ message: 
                "E-mail ou senha incorrectos. Insira suas informações de login novamente ou solicite um e-mail para obter acesso à sua conta." 
              });
          }

          //check if user is blocked
          if(usuario.estadoConta == "Bloqueada") {
            return res.status(401).json({
              message:
                "A sua conta foi bloqueada, por favor contacte o administrador para activar u mesma.",
            });
          }

          //create payload to save in the token
          const payload = {
            user: {
              id: usuario.id,
              id_empresa: usuario.id_empresa,
              id_tipo: usuario.id_tipo,
              id_Ficheiro: usuario.id_Ficheiro,
            },
          };

          //return jsonwebtoken -> to confirm that the user is registrated and ready form login
          jwt.sign(
            payload,
            config.get("jwtSecret"),
            { expiresIn: '8h' },
            (err, token) => {
              if (err) throw err;
              res.status(200).json({ token });
            }
          );
        } catch (error) {
      console.log(error.message);
      res.status(500).send("Server error: " + error.message);
    }
  },

  //@desc    get authenticated user
  async getAuthUser(req, res) {
    try {
      let user = await connection("usuario as u")
        .select(
          "u.nome",
          "u.email",
          "u.userName",
          "u.estadoConta",
          "t.designacao",
          "f.url",
          "e.nome as empresa",
          "end.cidade as endereco",
          "end.avRua as endereco",
          "end.numeroEdificio as endereco",
          "end.infoAdicional as endereco"
        )
        .join("empresa as e", "e.id", "u.id_empresa")
        .join("tipo as t", "t.id", "u.id_tipo")
        .join("ficheiro as f", "f.id", "u.id_Ficheiro")
        .join("endereco as end", "end.id", "e.id_endereco")
        .where("u.id", req.user.id)
        .first();

      res.send(user);
    } catch (error) {
      console.log(error.message);
      res.status(500).send("Server error: " + error.message);
    }
  },

  //@desc retrive fogotten passwword
  async forgotPassword(req, res) {
    const { userNameEmail } = req.body;

    try {
          var usuario = await connection("usuario")
            .select("*")
            .where("userName", userNameEmail)
            .first();

          //check if user exists
          if (!usuario) {
            usuario = await connection("usuario")
              .select("*")
              .where("email", userNameEmail)
              .first();
            if (!usuario) {
              return res.status(400).json({
                message: "Usuário não encotrado.",
              });
            }
          }

          //check if user is blocked
          if (usuario.estadoConta == "Bloqueada") {
            return res.status(401).json({
              message:
                "A sua conta foi bloqueada, por favor contacte o administrador para activar u mesma.",
            });
          }

          const token = crypto.randomBytes(20).toString("hex"); //token for password reset

          const now = new Date();
          now.setHours(now.getHours() + 1); //defines 1 hour for token expiration

          //save token and expire time
          if (userNameEmail.indexOf("@") > -1) {
            var email = await connection("usuario")
              .where("email", userNameEmail)
              .update(
                {
                  passwordResetToken: token,
                  passwordResetExpires: now,
                },
                ["email"]
              );
          } else {
            var email = await connection("usuario")
              .where("userName", userNameEmail)
              .update(
                {
                  passwordResetToken: token,
                  passwordResetExpires: now,
                },
                ["email"]
              );
          }

          const username = usuario.userName;
          const restivaUrl = config.get("restiva.url"); //front end url
          const restivaEmail = config.get("restiva.email"); //get restiva email
          mailer.sendMail(
            {
              to: email,
              from: restivaEmail,
              subject: "[RESTIVA] Link de Recuperação de Senha",
              template: "auth/esqueceuSenha",
              context: { token, restivaUrl, username },
            },
            (error) => {
              if (error) {
                return res.status(400).send({
                  message: `Não foi possível enviar o link, por favor tente novamente. ${error}`,
                });
              } else {
                return res.send({
                  message:
                    " Link de recuperação envido para o seu email! Caso não encontre na caixa principal verifique u caixa de spam",
                });
              }
            }
          );
        } catch (error) {
      console.log(error.message);
      res.status(500).send("Server error: " + error.message);
    }
  },

  async getPasswordResetExpired(req, res) {
    const { passwordResetToken } = req.params;
    try {
      const passwordResetExpires = await connection("usuario")
        .select("passwordResetExpires")
        .where("passwordResetToken", passwordResetToken)
        .first();

      if(!passwordResetExpires)
        return res.status(400).send({ message: "Token invalido."});

      const now = new Date();
      if(passwordResetToken < now.getHours())
        return res.send({expired:true}); //password reset token expired
      
      return res.send({ expired: false }); //password reset token is valid
    } catch (error) {
      console.log(error.message);
      res.status(500).send("Server error: " + error.message);
    }
  },

  async updatePassword(req, res) {
    var { senha } = req.body;
    const { passwordResetToken } = req.params;

    try {
      //encrypt password
      const salt =  await bcrypt.genSalt(10);
      senha = await bcrypt.hash(senha, salt);

      const id = await connection("usuario")
        .where("passwordResetToken", passwordResetToken)
        .update(
          {
            password: senha,
            passwordResetToken: null,
            passwordResetExpires: null,
          },
          ["id"]
        ); 
      
      if (!id[0])
       return res.status(400).send({ message: "Token invalido." }); 
      
      return res.send({message:"Senha alterada com sucesso, pode fazer login com u nova senha."});
    } catch (error) {
      console.log(error.message);
      res.status(500).send("Server error: " + error.message);
    } 
  }
};