const connection = require('../database/connection');
const UssdMenu = require("ussd-menu-builder");
const menu = new UssdMenu();
const config = require("config");
const shortCode = config.get("africastalking.shortCode");
const messageAT = require("../resources/sms/africastalking/Message"); //sends africas talking sms
const messageTwilio = require("../resources/sms/twilio/Message"); //sends twilio sms
var dateFormat = require("dateformat");

//sessions configurations
var sessions = {};
menu.sessionConfig({
    start: (sessionId, callback) => {
        // initialize current session if it doesn't exist
        // this is called by menu.run()
        if(!(sessionId in sessions)) sessions[sessionId] = {};
        callback();
    },
    end: (sessionId, callback) => {
        // clear current session
        // this is called by menu.end()
        delete sessions[sessionId];
        callback();
    },
    set: (sessionId, key, value, callback) => {
        // store key-value pair in current session
        sessions[sessionId][key] = value;
        callback();
    },
    get: (sessionId, key, callback) => {
        // retrieve value by key in current session
        let value = sessions[sessionId][key];
        callback(null, value);
    }
});


//Realiza fetch de menssagens 
/*
var timer = setInterval(fetchMessages, 30000);
function fetchMessages() {
    let lastReceivedId = 0;
    messageAT.getMessagesRecursively(lastReceivedId).then(console.log).catch(console.log)
}*/

module.exports = {
   
    async list (request, response) {
        try {
            const { tipo } = request.params;
           
           if (tipo === "Confirmada" ) {

               const estiv = await connection('estiva as e')
                .where('estadoCandidatura', tipo)
                .join('usuario_estiva as a','a.id_estiva', 'e.id');
                return response.json(estiv);
            } else {

               const esti = tipo==="Pendente"
                            ?await connection('estiva')
                            .where('estadoCandidatura', tipo)
                            .orWhere("estadoCandidatura","Indisponivel")
                            :await connection('estiva')
                            .where('estadoCandidatura', tipo)
                return response.json(esti);
            }
        
        } catch (error) {
            console.error(error.message);
            response.status(500).send("Server error: " + error.message);
        }
    }, 

    async getMessageUSSD(req, res) {
        res.send('Bem vindo a plataforma de recrutamento de estivas (RESTIVA), uma iniciativa do Porto de Maputo!');
    },

    async createUSSD(req, res) {
        menu.startState({
            run: () => {
                menu.con(
                  "Bem vindo a plataforma RESTIVA" +
                    "\n1. Efectuar Candidatura" +
                    "\n2. Cancelar Candidatura" +
                    "\n3. Activar a candidatura" +
                    "\n4. Actualizar os dados" +
                    "\n5. Disponibilidade"
                );
            },
            next: {
                '1':'efectuar',
                '2':'cancelar',
                '3':'activar',
                '4':'actualizar',
                '5':'disponibilidade'
            },
            defaultNext: 'opcaoInvalida'
        });

        menu.state("opcaoInvalida", {
          run: () => {
            menu.end("Opção Invalida!");
          }
        });

        menu.state('efectuar', {
            run: () => {
                menu.con('Insira seu nome completo');
            },
            next: {
                // using regex to match user input to next state (* is required for this lib)
                '*[a-zA-Z]+$': 'efectuar.nome'
            },
            defaultNext: 'valorInvalido' 
        });

        menu.state("valorInvalido", {
          run: () => {
            menu.end("Valor Invalido!");
          }
        });

        menu.state("efectuar.nome", {
          run: function () {
            let nome = menu.val;
            menu.session.set('nome', nome);
            menu.con("Insira o número do seu Bilhete de Identidade");
          },
          next: {
            "*^([0-9]{12}[a-zA-Z]{1})$": "efectuar.nome.bi",
          },
          defaultNext: 'valorInvalido'
        });

        menu.state("efectuar.nome.bi", {
          run: () => {
            let nr_BI = menu.val;
            menu.session.set('nr_BI', nr_BI)
            menu.con("Insira seu número de telefone");
          },
          next: {
            "*^8[234567]\\d{7}$": "efectuar.nome.bi.telefone",
          },
          defaultNext: 'valorInvalido'
        });

        menu.state("efectuar.nome.bi.telefone", {
          run: async () => {
            let telefone = menu.val;
            menu.session.set("telefone", telefone);

            let nome1 = menu.session.get("nome");
            let nr_BI1 = menu.session.get("nr_BI");
            
            var nome, nr_BI;
            await nome1.then(n => { nome = n });
            await nr_BI1.then(n => { nr_BI = n});
            
            menu.con("Efectuar candidatura com os seguintes dados:" +
              "\nNome: " + nome +
              "\nNr de BI: " + nr_BI +
              "\nNr de Telefone: " + telefone +
              "\n\n1.Confirmar" +
              "\n2.Cancelar"
            );
          },
          next: {
            "1": "efectuar.nome.bi.telefone.confirmar",
            "2": "efectuar.nome.bi.telefone.cancelar",
          },
          defaultNext: 'opcaoInvalida'
        });
        
        menu.state("efectuar.nome.bi.telefone.cancelar", {
          run: () => {
            menu.end("Operação Cancelada!");
          }
        });

        menu.state("efectuar.nome.bi.telefone.confirmar", {
            run: async () => {
                const nome1 = menu.session.get("nome");
                const nr_BI1 = menu.session.get("nr_BI");
                const telefone1 = menu.session.get("telefone");
                var nome, nr_BI, telefone;
                trxProvider = connection.transactionProvider();

                try {
                    await telefone1.then(n => { telefone = n });
                    await nome1.then(n => { nome = n });
                    await nr_BI1.then(n => { nr_BI = n});

                    const trx = await trxProvider();//start the transaction
                    var estiva = await trx('estiva')
                                .select('*')
                                .where('telefone', telefone)
                                .orWhere('nr_BI', nr_BI)
                                .first();

                    if (!estiva) {
                        await trx('estiva').insert({
                            nome, 
                            nr_BI, 
                            telefone, 
                            estadoCandidatura: "Pendente", 
                            dataCandidatura: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss")
                        });
                        menu.end("Candidatura efectuada com sucesso!");

                        const msg = "Candidatura efectuada com sucesso!" +
                            "\nOs dados que recebemos são:" +
                            "\nNome: " + nome +
                            "\nNr de BI: " + nr_BI +
                            "\nNr de Telefone: " + telefone;
                    
                        messageAT.sendMessage("+258"+telefone, msg, shortCode);
                    }
                    await trx.commit();

                    if(estiva && estiva.telefone == telefone && estiva.estadoCandidatura === 'Pendente') {
                        menu.end("Candidatura falhada!" + 
                            "\nO número " + telefone + " já foi cadastrado no sistema."
                        );
                    }
                
                    if (estiva && estiva.nr_BI === nr_BI && estiva.estadoCandidatura === 'Pendente') {
                        menu.end("Candidatura falhada!" +
                            "\nO número de BI " + nr_BI + " já foi cadastrado no sistema."
                        );
                    }

                    if (estiva && estiva.estadoCandidatura !== 'Pendente') {
                        menu.end("Candidatura falhada!" +
                            "\nJá foi cadastrado no sistema, digite *" + shortCode +
                            "# para actualizar a candidatura."
                        );
                    }
                        
                } catch (error) {
                    console.log(error.message);
                    menu.end("Ocorreu um erro, tente novamente!"); 
                }
            }
        });

        menu.run(req.body, (ussdResult) => {
            res.send(ussdResult);
        });
        // ***********Final da candidatura************

        //***********Inicio do cancelamento da candidatur**********/
        menu.state('cancelar', {
            run: () => {
                menu.con('Insira o número do seu Bilhete de Identidade');
            },
            next: {
                "*^([0-9]{12}[a-zA-Z]{1})$": "cancelar.bi",
            },
            defaultNext: 'valorInvalido' 
        });

        menu.state("cancelar.bi", {
            run: () => {
            nr_BI_cancelar = menu.val;
            
            menu.con("Pretende cacenlar a candidatura:" +
                "\nNr de BI: " + nr_BI_cancelar +
                "\n\n1.Confirmar" +
                "\n2.Cancelar"
            );
            menu.session.set("nr_BI_cancelar", nr_BI_cancelar);
            },
            next: {
                "1": "cancelar.bi.confirmar",
                "2": "cancelar.bi.cancelar",
            },
            defaultNext: 'opcaoInvalida'
        });

        menu.state("cancelar.bi.cancelar", {
            run: () => {
              menu.end("Operação Cancelada!");
            }
        });

        menu.state("cancelar.bi.confirmar", {
            run: async ()=>{
                const trxProvider= connection.transactionProvider()
                try {
                    nr_BI_cancelar= await menu.session.get('nr_BI_cancelar');

                    const trx = await trxProvider();
                    const estiva = await trx('estiva')
                                .select('*')
                                .where('nr_BI', nr_BI_cancelar)
                                .first();

                    if(estiva && estiva.estadoCandidatura !== "Cancelada"){
                        await trx('estiva')
                            .update({
                                estadoCandidatura:"Cancelada",
                                dataCancelamento:dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss")
                            })
                            .where('nr_BI',nr_BI_cancelar);

                        menu.end("Candidatura cancelada com sucesso!");
                    }

                    await trx.commit();

                    if (estiva && estiva.estadoCandidatura === 'Cancelada') {
                        menu.end("Esta candidatura ja foi cancelada!");
                    }

                    if(!estiva) {
                        menu.end("Cancelamento falhada!" + 
                            "\nO número de bi " + nr_BI_cancelar + " nao esta associado a nenhuma candidatura."
                        );
                    }

                } catch (error) {
                    console.log(error.message);
                    menu.end("Ocorreu um erro, tente novamente!"); 
                }
            }
        });

        //***********Activar candidatura**********/

        menu.state('activar', {
          run: () => {
              menu.con('Insira o número do seu Bilhete de Identidade');
          },
          next: {
              "*^([0-9]{12}[a-zA-Z]{1})$": "activar.bi",
          },
          defaultNext: 'valorInvalido' 
        });

        menu.state('activar.bi', {
          run: async () => {
            const trxProvider= connection.transactionProvider()
            try {
              nr_BI_activar= menu.val;

              const trx = await trxProvider();
              const estiva = await trx('estiva')
                          .select('*')
                          .where('nr_BI', nr_BI_activar)
                          .first();
              if(estiva && estiva.estadoCandidatura === "Cancelada"){
                await trx('estiva')
                    .update({
                        estadoCandidatura:"Pendente",
                    })
                    .where('nr_BI',nr_BI_activar);

                menu.end("Candidatura activada com sucesso!");
              }
    
              await trx.commit();
    
              if (estiva && estiva.estadoCandidatura === 'Pendente') {
                  menu.end("Esta candidatura ja esta activa!");
              }
    
              if(!estiva) {
                  menu.end("A Activacao a falhou!" + 
                      "\n O número de bi " + nr_BI_activar + " nao esta associado a nenhuma candidatura."
                  );
              }
              
            } catch (error) {
              console.log(error.message);
              menu.end("Ocorreu um erro, tente novamente!"); 
            }
              
          }
        });

        //***********Actualizar candidatura**********/
        menu.state('actualizar', {
          run: () => {
              menu.con('Insira o número do seu Bilhete de Identidade da candidatura que pretende actualizar');
          },
          next: {
              "*^([0-9]{12}[a-zA-Z]{1})$": "actualizar.bi",
          },
          defaultNext: 'valorInvalido' 
        });

        menu.state('actualizar.bi',{
          run:()=>{
            menu.session.set('nr_bi_actualizar',menu.val);
            menu.con('Que informacao pretende actualizar?'+
                      '\n1. Nome '+
                      '\n 2. Telefone'
            )
          },
          next:{
            '1':'actualizar.bi.nome',
            '2':'actualizar.bi.telefone'
          },
          defaultNext: 'actualizar.bi'
        });

        //##############Actualiazar numero de nome##############
        menu.state('actualizar.bi.nome',{
          run: () => {
            menu.con('Insira o novo nome completo.');
          },
          next: {
              "*[a-zA-Z]{3,50}$": "actualizar.bi.nome.executar",
          },
          defaultNext: 'valorInvalido' 
        })

        menu.state('actualizar.bi.nome.executar',{
          run:async ()=>{

            const trxProvider= connection.transactionProvider()
            try {
              const nr_BI_actualizar= await menu.session.get('nr_bi_actualizar');
              const nome=menu.val;

              const trx = await trxProvider();
              const estiva = await trx('estiva')
                          .select('*')
                          .where('nr_BI', nr_BI_actualizar)
                          .first();
              if(estiva){
                await trx('estiva')
                    .update({
                        nome
                    })
                    .where('nr_BI',nr_BI_actualizar);

                menu.end("Nome actualizado para: "+ nome);
              }
    
              await trx.commit();
    
              if(!estiva) {
                  menu.end("A actualizcao falhou!" + 
                      "\n O número de bi " + nr_BI_actualizar   + " nao esta associado a nenhuma candidatura."
                  );
              }
              
            } catch (error) {
              console.log(error.message);
              menu.end("Ocorreu um erro, tente novamente!"); 
            }
          }
        })

        //##############Actualiazar numero de telefone###########
        menu.state('actualizar.bi.telefone',{
          run: () => {
            menu.con('Insira o novo numero de telefone.');
          },
          next: {
            "*^8[234567]\\d{7}$": "actualizar.bi.telefone.executar",
          },
          defaultNext: 'valorInvalido' 
        })

        menu.state('actualizar.bi.telefone.executar',{
          run:async ()=>{

            const trxProvider= connection.transactionProvider()
            try {
              const nr_BI_actualizar= await menu.session.get('nr_bi_actualizar');
              const telefone=menu.val;

              const trx = await trxProvider();
              const estiva = await trx('estiva')
                          .select('*')
                          .where('nr_BI', nr_BI_actualizar)
                          .first();
              if(estiva){
                await trx('estiva')
                    .update({
                        telefone
                    })
                    .where('nr_BI',nr_BI_actualizar);

                menu.end("Numero actualizado para: "+ telefone);
              }
    
              await trx.commit();
    
              if(!estiva) {
                  menu.end("A actualizcao falhou!" + 
                      "\n O número de bi " + nr_BI_actualizar   + " nao esta associado a nenhuma candidatura."
                  );
              }

              if(estiva && estiva.telefone == telefone) {
                menu.end("Candidatura falhada!" + 
                    "\nO número " + telefone + " já esta associado a outra candidatura"
                );
              }
              
            } catch (error) {
              console.log(error.message);
              menu.end("Ocorreu um erro, tente novamente!"); 
            }
          }
        })


        /**********************  5. Disponibilidade **********************************/

        menu.state("disponibilidade", {
          run: () => {
            menu.con(
              "Indique sua disponibilidade" +
                "\n1.Disponível" +
                "\n2.Indisponível" +
                "\n3.Voltar"
            );
          },
          next: {
            "1": "disponibilidade.bi",
            "2": "disponibilidade.bi",
            "3": "__start__"
          },
          defaultNext: "valorInvalido",
        });

        menu.state("disponibilidade.bi", {
          run: () => {
            const opcao = menu.val;
            menu.session.set('opcao', opcao);
            menu.con("Insira seu número de BI");
          },
          next: {
            "*^([0-9]{12}[a-zA-Z]{1})$": "disponibilidade.bi.defenir",
          },
          defaultNext: "valorInvalido",
        });

      menu.state("disponibilidade.bi.defenir", {
        run: async () => {
          const nrBI = menu.val;
          const opcao = await menu.session.get('opcao');
          const mensagem = await updateEstadoCandidatura(opcao, nrBI);
          menu.end(mensagem);
        }
      });

    },

    async responderNotificacao(req, res) {
        console.log(req.body);        
        const { from, text} = req.body;
        const telefone = from.slice(-9);
        var msg;
        
        if(text != 1 && text != 2) {
            msg = "Opção invalida!";
        }
        try {
            const trxProvider = connection.transactionProvider();
            const trx = await trxProvider();
            const estiva = await trx('estiva').select('*')
                .where('telefone',telefone)
                .andWhere('estadoCandidatura','NaoConfirmada')
                .first();

            if (((!estiva && (text == 1 || text == 2)) || !estiva)) {
                msg = "Notificação invalida, digite *"+shortCode+"# para efectuar ou actualizar candidatura.";
            }

            if(text == 1 && estiva) {
               await trx('estiva').update({estadoCandidatura: 'Confirmada'}).where('telefone',telefone); 
                msg = "Disponibilidade confirmada.";
            } else if (text == 2 && estiva) {
                await trx('estiva').update({ estadoCandidatura: 'Indisponivel' }).where('telefone', telefone);
                msg = "Candidatura cancelada, digite *" + shortCode +"# para actualizar a candidatura.";
            }
            await trx.commit();
            
            //sends reply to de user who sent the message 
            messageAT.sendMessage(from, msg, shortCode);
            
            //messageTwilio.sendMessage(from, msg, config.get("twilio.virtualNumber"));
            //messageTwilio.sendMessageResponse(res, msg);

            res.send(msg)   
        } catch (error) {
            console.log(error.message);
        } 
    },

};


async function updateEstadoCandidatura(opcao, nrBI) { //opcao: 1.disponivel e 2.indisponivel
  const estadoCandidatura = (opcao == 1 ? "Confirmada" : "Indisponivel");
  var msg, smsMessage;

  try {
    const trxProvider = connection.transactionProvider();
    const trx = await trxProvider();
    
    const estiva = await trx('estiva as e')
      .select(
        'e.nome',
        'e.telefone',
        'ue.dataInducao',
        'emp.nome as empresa',
        'end.cidade as endereco',
        'end.avRua as endereco',
        'end.numeroEdificio as endereco',
        'end.infoAdicional as endereco'
      )
      .join('usuario_estiva as ue', 'ue.id_estiva', 'e.id')
      .join('usuario as u', 'u.id', 'ue.id_usuario')
      .join('empresa as emp', 'emp.id', 'u.id_empresa')
      .join('endereco as end', 'end.id', 'emp.id_endereco')
      .where('nr_BI', nrBI)
      .andWhere('estadoCandidatura', 'NaoConfirmada')
      .first();

    if(!estiva) 
      msg = "Operação falhada! Candidato não solicitado."
    else {
      await trx('estiva').update({ estadoCandidatura, dataConfirmacao: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss")})
        .where('nr_BI', nrBI);

      if(opcao == 1) {
        smsMessage = `${estiva.nome}, sua disponibilidade foi confirmada com sucesso, dirija-se a: 
          Empresa: ${estiva.empresa}
          Endereço: ${estiva.endereco} 
          Data: ${estiva.dataInducao.getDate()}/${estiva.dataInducao.getMonth() + 1}/${estiva.dataInducao.getFullYear()}
          Hora: ${dateFormat(estiva.dataInducao.getTime(), 'UTC:HH:MM')}`;
      } else {
        smsMessage = `${estiva.nome}, sua disponibilidade foi actualizada para Indisponível. `+
        `Aguarde a próxima solicitação.`;
      }

      messageAT.sendMessage('+258'+estiva.telefone, smsMessage, shortCode);
      msg = "Disponibilidade actualizada com sucesso.";
    }
    await trx.commit();

    return msg;
  } catch (error) {
    console.log(error);
    return "Operação falhada! Ocorreu um erro no servidor.";
  }
}