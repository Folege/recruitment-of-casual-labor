const connection = require('../database/connection');
const fs = require('fs');
const path = require('path');

module.exports = {
    async updateUsuario(req, res) {
        trxProvider = connection.transactionProvider();
        const { nome, email, userName, tipoNome, estadoConta, id_usuario} = req.body;

        try {
            const trx = await trxProvider();//start the transaction
            const id_tipo  = await trx('tipo') //get new tipo id
                .select('id').where('designacao', tipoNome).first();

            const user = await trx('usuario').returning('userName').where('id', id_usuario)
                .update({ nome, email, userName, estadoConta, id_tipo: id_tipo.id});
            
            await trx.commit();
            res.send({ message: `Dados do usuário "${user[0]}" actualizados com sucesso.`})
        } catch (error) {
        
            console.log(error);
            return res.status(500).send({ message: error.message });
        }
    },

    async deleteUsuario(req, res) {
        trxProvider = connection.transactionProvider();
        const { idUsuario } = req.params; 

        try {
            const trx = await trxProvider();//start the transaction
            
            const idFicheiro = await trx('usuario')
                .where('id', idUsuario)
                .select('id_Ficheiro')
                .first();

            if(!idFicheiro) {
                return res.status(404).json({ message: "Usuário não encontrado."})
            }
           
            await trx('usuario')
                .where('id', idUsuario)
                .del();

            const nomeImagem  = await trx('ficheiro')
                .where('id', idFicheiro.id_Ficheiro)
                .select('nomeImagem')
                .first()
            
            if(nomeImagem.nomeImagem !== "ImagemPadrao.png") {
                await trx('ficheiro')
                    .where('id', idFicheiro.id_Ficheiro)
                    .del();
                
                //remove file from server
                fs.unlink(path.resolve(__dirname, '..', '..', 'tmp', 'uploads', nomeImagem.nomeImagem), function (err) {
                    if (err) throw err;
                    // if no error, file has been deleted successfully
                    console.log('File deleted!');
                });
            }
            
            await trx.commit();

            return res.send({ message: "Usuário removido com sucesso."});
        } catch (error) {
            console.log(error.message);
            res.status(500).send("Server error: " + error.message);
        }
    }
}