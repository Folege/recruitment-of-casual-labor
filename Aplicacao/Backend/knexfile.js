// Update with your config settings.

module.exports = {

  development: {
    client: 'mssql',  
    connection: {  
      user: 'sa',  
      password: '1234',  
      server: "localhost\\SQLEXPRESS", 
      port: 1433,
      database : 'DB_RECRUTAMENTO',   
      options: {
        encrypt: true ,
        enableArithAbort: true
      } 
    },
    migrations:{
      directory:'./src/database/migrations'
    },
    seeds:{
      directory:'./src/database/seeds'
    }

  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};
