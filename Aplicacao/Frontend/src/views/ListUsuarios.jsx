import React, {useState, useEffect} from 'react';
import { Link ,useLocation, useHistory} from "react-router-dom";
import jwt from "jsonwebtoken";

import MUIDataTable from "mui-datatables";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import { MdLockOutline, MdLockOpen, MdDelete, MdBorderColor} from "react-icons/md";
import Skeleton from "react-loading-skeleton";

import { 
  Row, 
  Col, 
  Card, 
  CardHeader, 
  CardBody, 
  Alert, 
} from "reactstrap";

import UsuariosModal from "components/AdminComp/UsuariosModal.jsx";
import DataTableText from "components/AdminComp/DataTableText.js";
import api from "../services/api";

export default function ListUsuarios() {
    const location = useLocation();
    //State of Alerts
    const [alertVisible, setAlertVisible] = useState(!location.state?false:true);
    const onDismiss = () => setAlertVisible(false);
    const [mensagemAlerta, setMensagemAlerta] = useState(
      "Usuário cadastrado com sucesso, credenciais de acesso enviadas por email."
    );
    const [colorAlerta,setAlertColor]=useState("success");
    const [editar, setEditar] = useState("excluded"); //Edit user column state
    const [selectableRows, setSelectableRows] = useState("none");
    const [addNovoBtn, setAddNovoBtn] = useState(false);
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);
    const [users, setUsers] = useState([]);
    const [formValues, setFormValues] = useState('');
    const [tipo, setTipo]=useState([]);
    const [idUser, setIdUser] = useState('');
    const history = useHistory();
    const token = localStorage.getItem("token");
    const decoded = jwt.verify(token, "secretRestivaToken");
    const idTipo = decoded.user.id_tipo;
    const [lock,setLock] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);
    const [designacao,setDesignacao] = useState("");
    
    const handleBloquear = (selectedRows,dis,funcao) => {      
      for (let inde = 0; inde < selectedRows.data.length; inde++) {
        const id = selectedRows.data[inde].index;
        const data ={
          id_usuario:dis[id].data[0].key,
          funcao: funcao
        }
        api.put('lock',data, {
          headers: {
            "x-auth-token": token
          }
        }).then(response => {
          setLock(funcao);
        })
      }

      if (funcao === "Bloqueada") {
        setMensagemAlerta(`Conta(s) Bloqueada(s) Com Sucesso `);
        setAlertColor('danger');
      }else{
        setMensagemAlerta(`Conta(s) Activada(s) Com Sucesso `);
        setAlertColor("success");
      }
      history.push('#alerta');
      setAlertVisible(true);
  
    };
    
    const handleApagar = (selectedRows, dis) => {
      for (let inde = 0; inde < selectedRows.data.length; inde++) {
        const id = selectedRows.data[inde].index;
        const idUsuario =  dis[id].data[0].key;
        
        api.delete(`deleteUser/${idUsuario}`, {
          headers: {
            "x-auth-token": token
          },
          validateStatus: (status) => {
            return status;
          }
        }).then(response => {
          if (response.status === 200) {
            setAlertColor('success');
            setLock("apagar");
          } else {
            setAlertColor("danger");
          }
          setMensagemAlerta(response.data.message);
          setAlertVisible(true);
          
        }, (error) => {
          setMensagemAlerta(`Ocorreu um erro,tente novamente! ${error.message}`);
          setAlertColor('danger');
          setAlertVisible(true);
        });
        history.push("#alerta");
      }
    };

    
    const handleEdit = (id) => {
      setIdUser(id);
      const perfil = users.filter( function(user, index, users) {
        return user.id === id;
      });

      const nome = perfil[0].nome.split(" ",2);
      setFormValues({
        primeiroNome: nome[0],
        apelido: nome[1],
        empresa: perfil[0].empresa,
        tipo: perfil[0].tipo,
        usuario: perfil[0].userName,
        email: perfil[0].email,
        estado: perfil[0].estadoConta,
        imgSrc: perfil[0].url
      });

      api.get('getTipo',{
        headers: {
          "x-auth-token": token,
        }
      }).then(response=>{
        setTipo(response.data);
      });

      toggle();
    }

    useEffect(() => {
      api.get(`getRoles/${idTipo}`, {
          headers: {
            "x-auth-token": token,
          },
          validateStatus: (status) => {
            return status;
          },
        }).then((response) => {
          if (response.status === 200) {
            response.data.map((role) => {
              if (role.designacao === "GERIR_USUARIOS") {
                setAddNovoBtn(true);
                setEditar(true);
                setSelectableRows("multiple");
              } 
              return "";
            });
          } else {
            setAlertColor("danger");
            setMensagemAlerta(
              `Ocorreu um erro, tente novamente!! ${response.data.message}`
            );
            setAlertVisible(true);
          }
        },(error) => {
          setMensagemAlerta(`Ocorreu um erro,tente novamente! ${error.message}`);
          setAlertColor('danger');
          setAlertVisible(true); 
        });

      api.get('userList',{
        headers:{
          "x-auth-token": token,
          "tipo" : idTipo, 
        },validateStatus: (status) => {
            return status;
          },
      }).then(response => {
        if(response.status === 200) {
          setUsers(response.data);
          setIsLoaded(true);
        } else {
          setAlertColor("danger");
          setMensagemAlerta(`Ocorreu um erro, tente novamente! ${response.data.message}`);
          setAlertVisible(true);
        }
      },(error) => {
          setMensagemAlerta(`Ocorreu um erro,tente novamente! ${error.message}`);
          setAlertColor('danger');
          setAlertVisible(true); 
      });

      api.get('getDesignacao',{
        headers:{
          "x-auth-token": token,
          "tipo" : idTipo,
        },validateStatus: (status) => {
            return status;
          },
      }).then(response => {
        if(response.status === 200)
          setDesignacao(response.data);
        else {
          setAlertColor("danger");
          setMensagemAlerta(`Ocorreu um erro, tente novamente! ${response.data.message}`);
          setAlertVisible(true);
        }
      },(error) => {
          setMensagemAlerta(`Ocorreu um erro,tente novamente! ${error.message}`);
          setAlertColor('danger');
          setAlertVisible(true); 
      });
      
    }, [lock,idUser,token,idTipo]);
    
    const cols = ["Usuário", "Nome", "Email", "Tipo", "Empresa", "Estado", 
      { name: "Editar", options: { display: editar}}];
    
    const options = {
      selectableRows: selectableRows,
      filter: false,
      print: false,
      download: false,
      pagination: false,
      sort: false,
      textLabels: DataTableText.textLabels,
      responsive: "stackedFullHeight",
      isRowSelectable: (dataIndex) => {
        if (designacao ==="Root" && users[dataIndex].tipo !== "Root") {
          return true;
          
        } else {
          if (users[dataIndex].tipo === "Administrador"||users[dataIndex].tipo === "Root") {
            return false;
          }else{
            return true;
          }
          
        }
        
      },
      onRowsDelete: (deletedRows) => {
        if (deletedRows.data[0].dataIndex === 0) {
          setAlertVisible(true);
          return false;
        }
      },
      customToolbarSelect: (selectedRows,dis, set) => (
        <div>
          <Tooltip title="Bloquear Conta">
            <IconButton
              onClick={() => {
                handleBloquear(selectedRows,dis,"Bloqueada");
              }}
            >
              <MdLockOutline />
            </IconButton>
          </Tooltip>
          <Tooltip title="Activar Conta">
            <IconButton
              onClick={() => {
                handleBloquear(selectedRows,dis,"Activa");
              }}
            >
              <MdLockOpen />
            </IconButton>
          </Tooltip>
          <Tooltip title="Apagar Conta">
            <IconButton
              onClick={() => {
                handleApagar(selectedRows,dis);
              }}
            >
              <MdDelete />
            </IconButton>
          </Tooltip>
        </div>
       
      ),
    };

    return (
      <div className="content">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <Row>
                  <Col>
                    <Alert
                      id="alerta"
                      className="col-12"
                      color={colorAlerta}
                      isOpen={alertVisible}
                      toggle={onDismiss}
                    >
                      {mensagemAlerta}
                    </Alert>
                  </Col>
                </Row>
                {isLoaded ? (
                  !addNovoBtn ? null : (
                    <Link
                      className="btn btn-round btn-outline-success"
                      to="/admin/adicionar-novo"
                    >
                      Adicionar Novo
                    </Link>
                  )
                ) : (
                  <div className="col-sm-3">
                    <Skeleton height={25} />
                  </div>
                )}
              </CardHeader>

              <CardBody>
                <div id="tabela-overflow-x">
                  <div id="tabela-content">
                    {isLoaded ? (
                      <MUIDataTable
                        title={"Lista de Usuários"}
                        data={users.map((user) => [
                          <div key={user.id}>
                            <img
                              alt=""
                              src={user.url}
                              className="rounded-circle img-fluid zoom"
                              style={{
                                width: "40px",
                                height: "40px",
                                backgroundSize: "cover",
                              }}
                            />
                            {user.userName}
                          </div>,
                          user.nome,
                          user.email,
                          user.tipo,
                          user.empresa,
                          <span
                            className={
                              user.estadoConta === "Bloqueada"
                                ? "text-danger"
                                : "text-success"
                            }
                          >
                            {user.estadoConta}
                          </span>,
                          designacao === "Root" ? (
                            <MdBorderColor
                              size={20}
                              color="#0000FF"
                              onClick={(e) => handleEdit(user.id)}
                              style={{ cursor: "pointer" }}
                            />
                          ) : user.tipo === "Administrador" ||
                            user.tipo === "Root" ? (
                            <MdBorderColor
                              size={20}
                              color="#5f5f5e7d"
                              style={{ cursor: "not-allowed" }}
                            />
                          ) : (
                            <MdBorderColor
                              size={20}
                              color="#0000FF"
                              onClick={(e) => handleEdit(user.id)}
                              style={{ cursor: "pointer" }}
                            />
                          ),
                        ])}
                        columns={cols}
                        options={options}
                      />
                    ) : (
                      <div>
                        <Skeleton height={100} />
                        <Skeleton height={25} count={10} />
                      </div>
                    )}
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col>
            <UsuariosModal
              idUser={idUser}
              setIdUser={setIdUser}
              setAlertColor={setAlertColor}
              setMensagemAlerta={setMensagemAlerta}
              setAlertVisible={setAlertVisible}
              toggle={toggle}
              history={history}
              token={token}
              modal={modal}
              formValues={formValues}
              tipo={tipo}
            />
          </Col>
        </Row>
      </div>
    );
        
    
}