import React, {useState, useEffect} from 'react';
import { Link, useHistory } from "react-router-dom";
import MUIDataTable from "mui-datatables";
import DataTableText from "components/AdminComp/DataTableText.js";
import Skeleton from "react-loading-skeleton";
import dateFormat from "dateformat";

import {
  FaRegCalendarCheck,
  FaRegCalendarTimes,
  FaRegClock,
  FaRegEye,
  FaCaretDown,
} from "react-icons/fa";

import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  UncontrolledCollapse,
} from "reactstrap";

import api from "../services/api";

export default function Dashboard() {
  const zeroPad = (num, places) => String(num).padStart(places, '0'); //changes number format from 1 to 001
  const [contEst, setContEst] = useState({"Pendente": 0,"Confirmada": 0,"Cancelada": 0});
  const token = localStorage.getItem("token");
  const [quantMes,setQuantMes] = useState([]);
  const [notificacoes, setNotificacoes] = useState([]);
  const [maxMonth, setMaxMonth] = useState("");
  const [minMonth, setMinMonth] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  const history = useHistory();

  const date = new Date();
  var mes = date.getMonth() + 1;
  const [anoMes, setAnoMes] = useState(
    date.getFullYear() + "-" + zeroPad(mes, 2)
  );

  
  dateFormat.i18n = {
    dayNames: [
      'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb',
      'Domingo segunda terça quarta quinta sexta sábado'
    ],
    monthNames: [
      'Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez',
      'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'
    ],
    timeNames: [
      'a', 'p', 'am', 'pm', 'A', 'P', 'AM', 'PM'
    ]
  };

  useEffect(() => {
    api.get("quantCandidaturas", {
      headers: {
        "x-auth-token": token,
      },
    })
    .then((response) => {
      setContEst(response.data);
    });

    api.get('getMaxMonth', {
      headers: {
        "x-auth-token": token,
      },
    })
    .then((response) => {
      setMaxMonth(response.data);
    }, (error) => {
      console.log(error); 
    });

    api.get('getMinMonth', {
      headers: {
        "x-auth-token": token,
      },
    })
    .then((response) => {
      setMinMonth(response.data);
    }, (error) => {
      console.log(error); 
    });

    api.get(`notificacoes?anoMes=${anoMes}`, {
      headers: {
        "x-auth-token": token,
      },
    })
    .then((response) => {
      setNotificacoes(response.data);
      setIsLoaded(true);
    }, (error) => {
      console.log(error); 
    });
      
    api.get(`quantMes?anoMes=${anoMes}`,{
      headers:{
        "x-auth-token": token,
      }
    }).then(response => {
      setQuantMes(response.data);
    }, (error) => {
      console.log(error); 
    }); 
  },[token, anoMes]);

    //Data for MuiDataTable
    const cols = [ 
      "Remetente",
      "Data/Hora",
      "Estiva",
      "Nr. de BI do Estiva",
      "Data/Hora Indicada",
      "Disponibilidade",
    ];

    const options = {
      download: false,
      print: false,
      filter: false,
      selectableRows: false,
      textLabels: DataTableText.textLabels,
      sort: false,
      responsive: "stackedFullHeight",
      rowsPerPageOptions: [15, 20, 30, 50],
    };


  return (
    <>
      <div className="content">
        <Row>
          <Col lg="4" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center text-primary">
                      <FaRegClock size={50} />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Pendetes</p>
                      <CardTitle tag="p">
                        <p>{zeroPad(contEst.Pendente, 2)}</p>
                      </CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <hr />
                <div className="stats">
                  <Link to="/admin/candidaturas/" className="stats">
                    <FaRegEye /> ver candidaturas pedentes
                  </Link>
                </div>
              </CardFooter>
            </Card>
          </Col>

          <Col lg="4" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center text-success">
                      <FaRegCalendarCheck size={50} />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Confirmadas</p>
                      <CardTitle tag="p">
                        <p>{zeroPad(contEst.Confirmada, 2)}</p>
                      </CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <hr />
                <div className="stats">
                  <span
                    onClick={() =>
                      history.push({
                        pathname: "/admin/candidaturas/",
                        state: 1,
                      })
                    }
                    className="stats linkStyle"
                  >
                    <FaRegEye /> ver candidaturas confirmadas
                  </span>
                </div>
              </CardFooter>
            </Card>
          </Col>

          <Col lg="4" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center text-danger">
                      <FaRegCalendarTimes size={50} />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Canceladas</p>
                      <CardTitle tag="p">
                        <p>{zeroPad(contEst.Cancelada, 2)}</p>
                      </CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <hr />
                <div className="stats">
                  <span
                    onClick={() =>
                      history.push({
                        pathname: "/admin/candidaturas/",
                        state: 2,
                      })
                    }
                    className="stats linkStyle"
                  >
                    <FaRegEye /> ver candidaturas canceladas
                  </span>
                </div>
              </CardFooter>
            </Card>
          </Col>
        </Row>
        <Row className="mt-5">
          <Col md={8} className="mb-0">
            <h5 className="float-left m-0">Notificações</h5>
          </Col>
          <Col className="mb-0" md={4}>
            <Form className="mb-0">
              <FormGroup row className="mb-0">
                <Label for="exampleSelect" sm={2} className="mb-0">
                  Mês
                </Label>
                <Col sm={10} className="mb-0">
                  <Input
                    type="month"
                    value={anoMes}
                    onChange={(e) => setAnoMes(e.target.value)}
                    min={minMonth.minMonth}
                    max={maxMonth.maxMonth}
                    name="select"
                    id="exampleSelect"
                    className="mb-0"
                  />
                </Col>
              </FormGroup>
            </Form>
          </Col>
          <Col sm={12}>
            <hr className="mb-0" />
          </Col>
        </Row>
        <Row className="mt-3">
          <Col xs={12}>
            <Card className="rounded-bottom">
              <CardHeader id="notf-1">
                <Row className="mt-2 mb-2">
                  <Col md="3">
                    <strong>{dateFormat(anoMes, "mmmm 'de' yyyy")}</strong>
                  </Col>
                  <Col md="2" className="d-none d-md-none d-lg-block">
                    Disponibilidades:
                  </Col>
                  <Col md="2" className="d-none d-md-none d-lg-block">
                    <span className="text-info">
                      Não Confirmadas:{" "}
                      {quantMes.map((Cont) => [
                        <span>{Cont.NaoConfirmada}</span>,
                      ])}
                    </span>
                  </Col>
                  <Col md="2" className="d-none d-md-none d-lg-block">
                    <span className="text-success">
                      Confirmadas:{" "}
                      {quantMes.map((Cont) => [<span>{Cont.Confirmada}</span>])}
                    </span>
                  </Col>
                  <Col md="2" className="d-none d-md-none d-lg-block">
                    <span className="text-danger">
                      Indisponíveis:{" "}
                      {quantMes.map((Cont) => [
                        <span>{Cont.Indisponivel}</span>,
                      ])}
                    </span>
                  </Col>
                  <Col md="1">
                    <FaCaretDown className="float-right mt-0" size={26} />
                  </Col>
                </Row>
              </CardHeader>

              <UncontrolledCollapse
                toggler="#notf-1"
                defaultOpen={true}
                className="p-3"
              >
                <CardBody id="tabela-overflow-x">
                  <div id="tabela-content">
                    <Row className="mb-3">
                      <Col xs="3" className="d-block d-md-block d-lg-none">
                        Disponibilidades:
                      </Col>
                      <Col xs="3" className="d-block d-md-block d-lg-none">
                        <span className="text-info">
                          Não Confirmadas:{" "}
                          {quantMes.map((Cont) => [
                            <span>{Cont.NaoConfirmada}</span>,
                          ])}
                        </span>
                      </Col>
                      <Col xs="3" className="d-block d-md-block d-lg-none">
                        <span className="text-success">
                          Confirmadas:{" "}
                          {quantMes.map((Cont) => [
                            <span>{Cont.Confirmada}</span>,
                          ])}
                        </span>
                      </Col>
                      <Col xs="3" className="d-block d-md-block d-lg-none">
                        <span className="text-danger">
                          Indisponíveis:{" "}
                          {quantMes.map((Cont) => [
                            <span>{Cont.Indisponivel}</span>,
                          ])}
                        </span>
                      </Col>
                      <Col></Col>
                    </Row>          
                      {isLoaded ? (
                        <MUIDataTable
                          title={"Notificações Enviadas"}
                          data={notificacoes.map((notif) => [
                            notif.remetente,
                            dateFormat(
                              notif.dataNotificacao,
                              "UTC:dd-mm-yyyy HH:MM:ss"
                            ),
                            notif.estiva,
                            notif.nr_BI,
                            dateFormat(notif.dataInducao, "UTC:dd-mm-yyyy HH:MM:ss"),
                            <span
                              className={
                                notif.estadoCandidatura === "Confirmada"
                                  ? "text-success"
                                  : notif.estadoCandidatura === "Indisponivel"
                                  ? "text-danger"
                                  : "text-info"
                              }
                        >
                          {notif.estadoCandidatura === "Confirmada"
                            ? "Confirmada"
                            : notif.estadoCandidatura === "Indisponivel"
                            ? "Indisponível"
                            : "Não Confirmada"}
                        </span>,
                      ])}
                      columns={cols}
                      options={options}
                    />
                      ) : (
                        <div>
                          <Skeleton height={100} />
                          <Skeleton height={25} count={10} />
                        </div>
                      )}
                    
                    
                  </div>
                </CardBody>
              </UncontrolledCollapse>
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );
}


