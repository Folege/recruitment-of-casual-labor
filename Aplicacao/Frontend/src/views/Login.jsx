import React, {useState} from 'react';
import { Link, useHistory } from 'react-router-dom';
import jwt from "jsonwebtoken";

import {
    AvForm,
    AvField,
} from "availity-reactstrap-validation";

import {
    Row,
    Col,
    Button,
    FormText,
} from 'reactstrap';

import LoginTheme from "../layouts/LoginTheme.jsx";
import api from "../services/api";
import auth from "auth";
import routes from "routes";


export default function Login() {
  const history = useHistory();
  const [alertVisible, setAlertVisible] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [alertColor, setAlertColor] = useState('danger');
  
  
  async function handleSubmit(event, values) {
    setAlertVisible(false);//close de alert case is open

    const data = {
      userNameEmail: values.usuario,
      senha: values.senha
    };
    
    await api.post('login', data,{
      validateStatus:(status)=>{
        return status;
      }
      }).then(response => {
        if(response.status === 200){
          auth.login(response.data.token);
          redirecionar();
        } else {
          if(response.status === 401) setAlertColor('info');
          else setAlertColor('danger');
          setAlertMessage(response.data.message);
          setAlertVisible(true);
        }
    }, (error) => {
        setAlertMessage(error);
        setAlertVisible(true);
    });
  };

  async function redirecionar() {

    const token = localStorage.getItem("token");
    const decoded = jwt.verify(token, "secretRestivaToken");
    const idTipo = decoded.user.id_tipo;

    await api.get(`getRoles/${idTipo}`, {
      headers: {
        "x-auth-token": token,
      },
      validateStatus: (status) => {
        return status;
      }
    }).then((response) => {
      if (response.status === 200) {
        var roles = response.data;
        
        var validRoles = roles.filter((elem) => {
          if (elem.designacao === "PAINEL_ADMINISTRATIVO")
            return elem;
          if (elem.designacao === "GERIR_CANDIDATURAS")
            return elem;
          if (elem.designacao === "GERIR_USUARIOS")
            return {designacao:"VISUALIZAR_USUARIOS"};
          return  "";
        });
        console.log(validRoles);

        var path;
        if(validRoles[0]) {
          routes.map((route) => {
            if (route.role === validRoles[0].designacao)
              path = route.path;
            return "";
          }); 
        } else {
          path = "/perfil";
        }
        
        history.push("/admin" + path);
      } else {
        history.push('/admin/perfil');
      }
    }, (error) => {
      console.log(error.message);
    });
    
  };

  return (
    <>
      <LoginTheme 
        alertOnDismiss={() => setAlertVisible(false)} 
        alertVisible={alertVisible} 
        alertMessage={alertMessage}
        alertColor={alertColor}
      >
        <Row className="justify-content-center mb-4 mt-4">
          <h4 className=" text-verde-r">Bem Vindo</h4>
        </Row>
        <Row className="justify-content-center">
          <Col xs={11}>
            <AvForm onValidSubmit={handleSubmit}>
              <Row>
                <Col>
                  <AvField
                    name="usuario"
                    id="usuario"
                    type="text"
                    label="E-mail ou Nome de Usuário"
                    validate={{
                      minLength: {
                        value: 3,
                        errorMessage: "Usuário invalido",
                      },
                      required: {
                        errorMessage: "Este campo é requerido.",
                      },
                    }}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <AvField
                    name="senha"
                    id="senha"
                    type="password"
                    label="Senha"
                    validate={{
                      required: {
                        errorMessage: "Este campo é requerido.",
                      },
                    }}
                  />
                  <FormText>
                    <Link to="/esqueceu-senha">Esqueceu a senha?</Link>
                  </FormText>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Button
                    type="submit"
                    className="btn btn-success bg-verde-r float-right"
                  >
                    Entrar
                  </Button>
                </Col>
              </Row>
            </AvForm>
          </Col>
        </Row>
      </LoginTheme>
    </>
  );
}