import React, {useState} from 'react';
import { Link } from "react-router-dom";
import { MdKeyboardBackspace } from "react-icons/md";

import {
    AvForm,
    AvField,
} from "availity-reactstrap-validation";

import {
    Row,
    Col,
    Button,
} from 'reactstrap';

import LoginTheme from "../layouts/LoginTheme.jsx";
import api from "../services/api";

export default function RecuperarSenha() {
  const [alertVisible, setAlertVisible] = useState(false);
  const [alertMessage, setAlertMessage] = useState(
    "Não foi possível enviar o link, por favor tente novamente."
  );
  const [alertColor, setAlertColor] = useState("danger");

  async function handleSubmit(event, values) {
    setAlertVisible(false); //close de alert case is open

    const data = {
      userNameEmail: values.usuario,
    };

    await api.post('esqueceu-senha', data,{
      validateStatus:(status)=>{
        return status;
      }
      }).then(response => {
        if(response.status === 200){
          setAlertColor('success');
        } else {
          if (response.status === 401) setAlertColor("info");
          else setAlertColor("danger");
        }
        setAlertMessage(response.data.message);
        setAlertVisible(true);
    }, (error) => {
        setAlertMessage(error);
        setAlertVisible(true);
    });
  }

  return (
    <LoginTheme
      alertOnDismiss={() => setAlertVisible(false)}
      alertVisible={alertVisible}
      alertMessage={alertMessage}
      alertColor={alertColor}
    >
      <Row className="justify-content-center mb-3">
        <Col xs={11}>
          <h4 className="text-verde-r">Esqueceu a senha?</h4>
          <p style={{ fontSize: "11px", color: "#9A9A9A" }}>
            Digite o endereço de email ou nome de usuário da sua conta RESTIVA.
            Você receberá um link para criar uma nova senha por e-mail.
          </p>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col xs={11}>
          <AvForm onValidSubmit={handleSubmit}>
            <Row>
              <Col>
                <AvField
                  name="usuario"
                  id="usuario"
                  type="text"
                  label="E-mail ou Nome de Usuário"
                  validate={{
                    minLength: {
                      value: 3,
                      errorMessage: "Usuário invalido",
                    },
                    required: {
                      errorMessage: "Este campo é requerido.",
                    },
                  }}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <Button
                  type="submit"
                  className="btn btn-success bg-verde-r float-right"
                >
                  Enviar
                </Button>
              </Col>
            </Row>
          </AvForm>
        </Col>
      </Row>
      <Row>
        <Col>
          <Link
            to="/login"
            className="float-left mt-3"
            style={{ color: "#9A9A9A", fontSize: "11px" }}
          >
            <MdKeyboardBackspace size={30} />
            Voltar para login
          </Link>
        </Col>
      </Row>
    </LoginTheme>
  );
}