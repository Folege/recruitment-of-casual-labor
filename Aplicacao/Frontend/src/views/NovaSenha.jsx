import React, {useEffect, useState} from 'react';
import { Link, useParams, useHistory } from "react-router-dom";
import swal from "sweetalert";

import {
    AvForm,
    AvField,
} from "availity-reactstrap-validation";

import {
    Row,
    Col,
    Button,
} from 'reactstrap';

import LoginTheme from "../layouts/LoginTheme.jsx"
import api from "services/api";

export default function NovaSenha() {
  const [alertVisible, setAlertVisible] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const [alertColor, setAlertColor] = useState("danger");
  const [display, setDisplay] = useState(true);
  const { passwordResetToken } = useParams();
  const history = useHistory();
  
  useEffect(() => {
    api
      .get(`password-reset-expires/${passwordResetToken}`, {
        validateStatus: (status) => {
          return status;
        },
      })
      .then(
        (response) => {
          if (response.status === 200 && response.data.expired === false)
            setDisplay(false);
           if(response.data.message === "Token invalido.")
            history.push('/esqueceu-senha');
        },
        (error) => {
          /*setAlertMessage(error);
          setAlertVisible(true);*/
          console.log(error);
        }
      );
  }, ([passwordResetToken, history]));

  async function handleSubmit(event, values) {
    setAlertVisible(false);//close de alert case is open

    const data = {
      senha: values.senha
    };
    
    await api.post(`nova-senha/${passwordResetToken}`, data,{
      validateStatus:(status)=>{
        return status;
      },
      }).then(response => {
        if(response.status === 200){
          swal(
            "Senha alterada!",
            "Já pode realizar login com a nova senha.",
            "success"
          );
          history.push('/login');
        } else {
          setAlertColor('danger');
          setAlertMessage(response.data.message);
          setAlertVisible(true);
        }
    }, (error) => {
        setAlertMessage(error);
        setAlertVisible(true);
    });
  };
  
  return (
    <LoginTheme
      alertOnDismiss={() => setAlertVisible(false)}
      alertVisible={alertVisible}
      alertMessage={alertMessage}
      alertColor={alertColor}
    >
      {display ? (
        <Row>
          <Col xs={11}>
            <h4 className="text-verde-r">Link expirado!</h4>
            <p>
              Este link expirou, para solicitar outro link de recuperação de
              senha clique no botão a baixo.
            </p>
            <Link to="/esqueceu-senha" className="btn btn-success bg-verde-r">
              Solicitar
            </Link>
          </Col>
        </Row>
      ) : (
        <>
          <Row className="justify-content-center mb-3">
            <Col xs={11}>
              <h4 className="text-verde-r">Defina nova senha.</h4>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col xs={11}>
              <AvForm onValidSubmit={handleSubmit}>
                <Row>
                  <Col>
                    <AvField
                      name="senha"
                      id="senha"
                      type="password"
                      label="Senha"
                      validate={{
                        required: {
                          errorMessage: "Este campo é requerido.",
                        },
                        pattern: {
                          value: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/,
                          errorMessage:
                            "A SENHA DEVE: Ter pelo menos 8 caracteres; Ter pelo menos 1 letra (a, b, c...); Ter pelo menos 1 número (1, 2, 3...); Incluir caracteres em Maiúscula e Minúscula.",
                        },
                      }}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <AvField
                      name="csenha"
                      id="csenha"
                      type="password"
                      label="Confirmar Senha"
                      validate={{
                        match: {
                          value: "senha",
                          errorMessage:
                            "Por favor, forneça o mesmo valor novamente.",
                        },
                        required: {
                          errorMessage: "Este campo é requerido.",
                        },
                      }}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Button
                      type="submit"
                      className="btn btn-success bg-verde-r float-right"
                    >
                      Confirmar
                    </Button>
                  </Col>
                </Row>
              </AvForm>
            </Col>
          </Row>
        </>
      )}
    </LoginTheme>
  );
}