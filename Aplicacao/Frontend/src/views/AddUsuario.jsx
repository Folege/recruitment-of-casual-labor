import React, {useState,useEffect}from 'react';
import { Link, useHistory} from 'react-router-dom';
import { MdKeyboardBackspace } from "react-icons/md";
import UploadImages from "yagoubi-upload-images";
import jwt from "jsonwebtoken";

import {
    AvForm,
    AvGroup,
    AvInput,
    AvFeedback,
    AvField,
} from 'availity-reactstrap-validation';

import {
    Row,
    Col,
    Card,
    CardHeader,
    CardTitle,
    CardBody,
    Container,
    Label,
    Button,
    Alert
} from 'reactstrap';

import api from '../services/api';



export default function AddUsuario() {
    const token = localStorage.getItem("token");
    const decoded = jwt.verify(token, "secretRestivaToken");
    const idTipoUserLogado = decoded.user.id_tipo;
    const idEmpresaUserLogado=decoded.user.id_empresa;

    const [primeiroNome,setPrimeiroNome]=useState('');
    const [apelido,setApelido]=useState('');
    const [email,setEmail]= useState('');
    const [empresa,setEmpresa]=useState([]);
    const [tipo,setTipo]= useState([]);
    const [tipoBack,setTipoBack]=useState([])
    const [id_tipo,setid_tipo]= useState('');
    const [id_empresa,setid_empresa]=useState('');
    const [tipoUserLogado,setTipoUserLogado]=useState('');
    const [estadoConta,setEstadoConta]= useState('Activa');
    const [file,setFile]= useState(null);
    //State of Alerts
    const [alertVisible, setAlertVisible] = useState(false);
    const onDismiss = () => setAlertVisible(false);
    const [mensagemAlerta,setMensagemAlerta]=useState('Usuario cadastrado com sucesso.');
    const [colorAlerta,setColorAlerta]=useState("success");
    
    const history=useHistory();

    const uploadImage = (files) => {
      setFile(files[0]);
    }


    useEffect(()=>{
      api.get('getTipo',{
        headers: {
          "x-auth-token": token,
        }
      }).then(response=>{
        setTipo(response.data.filter(item=>item.designacao !== "Administrador"))
        setTipoBack(response.data);
      });
    
      api.get(`/tipo/${idTipoUserLogado}`,{
        headers: {
          "x-auth-token": token,
        },
        validateStatus:(status)=>{
          return status;
        }
      })
      .then(response=>{
          setTipoUserLogado(response.data.tipo['designacao']);
          if(tipoUserLogado){
            buscarEmpresa();
          }
          
      })

      
    },[token,idTipoUserLogado,tipoUserLogado]);

    async function buscarEmpresa(){
      
      if(!['Administrador','Root'].includes(tipoUserLogado)){
        const response = await api.get(`getEmpresa/${idEmpresaUserLogado}`,{
          headers: {
            "x-auth-token": token,
          }
        });
        setEmpresa(response.data.nome);
      }else{
        const response = await api.get(`Empresa`,{
          headers: {
            "x-auth-token": token,
          }
        }); 
        if(tipoUserLogado!=="Root"){
          setEmpresa(response.data.filter(item=>item.nome!=="MPDC"))
        }else{
          setEmpresa(response.data);
        }
        
      }
    }

    function handleChangeEmpresa(id,text){
      setid_empresa(id);
      if(text!=="MPDC"){
        setTipo(tipo.filter(item=>item.designacao !== "Administrador"));
      }else{
        setTipo(tipoBack);
      }
    }

    async function handleAddUsuario(e){
      e.preventDefault();
      const Filedata = new FormData();
      Filedata.append('file',file);

      var nome=primeiroNome+' '+apelido;
      const data={
        nome,
        email,
        userName:primeiroNome.toLowerCase()+'.'+apelido.toLowerCase(),
        id_tipo,
        estadoConta
      }
 
      console.log(data);
      api.post('/upload',Filedata,
        {
          headers: {
            "x-auth-token": token,
          }
        });
      
      api.post('/usuario',data,{
        headers: {
          "x-auth-token": token,
          empresa_id: id_empresa
        },
        validateStatus:(status)=>{
          return status;
        }
      }).then(response=>{
        if(response.status===200){
          history.push({
            pathname:"/admin/usuarios",
            state:{alertSucesso:true}
          }); 
        }
        else{
          setMensagemAlerta(response.data.message);
          setColorAlerta('danger');
          history.push('#alerta');
          setAlertVisible(true);
        }
    },
    (error)=>{
      console.log(error);
      setMensagemAlerta(`Ocorreu um erro durante o cadastro, tente novamente!${error.message}`);
      setColorAlerta('danger');
      setAlertVisible(true);
      history.push('#alerta');
    });
        
      
  }


    return (
      <div className="content">
        <Row >
          <Alert
              id="alerta"
              className="col-12"
              color={colorAlerta}
              isOpen={alertVisible}
              toggle={onDismiss}
            >
              <label>Ocorreu um erro durante o cadastro, tente novamente!</label><br/> 
              {mensagemAlerta}
            </Alert>
        </Row>

        <Row className="justify-content-center">
            <Col md="11">
              <Card>
                <CardHeader>
                  <CardTitle tag="h5">Adicionar Novo Usuário</CardTitle>
                </CardHeader>
                <CardBody>
                  <Container>
                    <AvForm onValidSubmit={handleAddUsuario} id="Form">
                      <Row>
                        <Col>
                          <strong>Dados Pessoais</strong>
                          <hr />
                        </Col>
                      </Row>

                      <Row>
                        <Col md={6}>
                          <AvGroup>
                            <Label for="primeiroNome">Primeiro Nome</Label>
                            <AvInput
                              name="primeiroNome"
                              id="primeiroNome"
                              required
                              value={primeiroNome}
                              onChange={e=>setPrimeiroNome(e.target.value)}
                            />
                            <AvFeedback>Este campo deve ser preechido.</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col md={6}>
                          <AvGroup>
                            <Label for="apelido">Apelido</Label>
                            <AvInput name="apelido" id="apelido" required
                               value={apelido}
                               onChange={e=>setApelido(e.target.value)}
                            />
                            <AvFeedback>Este campo deve ser preechido.</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>

                      <Row>
                        <Col md={6}>
                          <AvGroup>
                            <Label for="empresa">Empresa</Label>
                            {
                              ['Administrador','Root'].includes(tipoUserLogado)
                              ? 
                              <AvField type="select" name="empresa"
                                onChange={e=>handleChangeEmpresa(e.target.value,e.nativeEvent.target[e.nativeEvent.target.selectedIndex].text)}
                                required
                              >
                                <option value="">Selecione a empresa</option>
                                {
                                  empresa.map(item=>{
                                    return(
                                      <option key={item.id} name={item.nome} value={item.id} >{item.nome}</option>
                                    )
                                  })
                                }
                              </AvField>
                              :
                                <AvInput
                                  name="empresa"
                                  id="empresa"
                                  value={empresa}
                                  disabled
                                />
                            }
                          </AvGroup>
                        </Col>
                        <Col md={6}>
                          <AvGroup>
                            <AvField type="select" name="tipo" label="Tipo"
                              
                              onChange={e=>setid_tipo(e.target.value)}
                              required
                            >
                              <option value="">Selecione o tipo de usuario</option>
                              {
                                tipo.map(item=>{
                                  return(
                                    <option key={item.id} value={item.id} >{item.designacao}</option>
                                  )
                                })
                              }
                            </AvField>
                          </AvGroup>
                        </Col>
                      </Row>

                      <Row className="mt-5">
                        <Col sm={5} md={3}>
                          <UploadImages
                            value={file}
                            name="file"
                            onChange={(file) => uploadImage(file)}
                            color="#6BD098"
                          />
                          <hr />
                        </Col>
                      </Row>

                      <Row className="mt-5">
                        <Col>
                          <strong>Dados de Acesso</strong>
                          <hr />
                        </Col>
                      </Row>

                      <Row>
                        <Col md={6}>
                          <AvField
                            name="usuario"
                            id="usuario"
                            type="text"
                            label="Nome de Usuário"
                            disabled
                            validate={{
                              minLength: {
                                value: 3,
                                errorMessage:
                                  "Por favor, forneça ao menos 3 caracteres.",
                              },
                              required: {
                                errorMessage: "Este campo deve ser preechido.",
                              },
                            }}
                            value={primeiroNome.toLowerCase()+'.'+apelido.toLowerCase()}
                          />
                        </Col>
                        <Col md={6}>
                          <AvField
                            name="email"
                            id="email"
                            type="email"
                            label="E-mail"
                            validate={{
                              email: {
                                value: 3,
                                errorMessage:
                                  "Por favor, forneça um endereço eletrônico válido.",
                              },
                              required: {
                                errorMessage: "Este campo deve ser preechido.",
                              },
                            }}
                            value={email}
                            onChange={e=>setEmail(e.target.value)}
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col md={6}>
                          <AvField type="select" name="estado" label="Estado da Conta"
                            value={estadoConta}
                            onChange={e=>setEstadoConta(e.target.value)}
                          >
                            <option value="Activa">
                              Activa
                            </option>
                            <option value="Bloqueada" disabled>
                              Bloqueada
                            </option>
                          </AvField>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Link
                            to="/admin/dashboard"
                            className="float-left mt-3"
                            style={{ color: "gray" }}
                          >
                            <MdKeyboardBackspace size={30} /> Cancelar
                          </Link>
                          <Button
                            type="submit"
                            className="float-right btn btn-round btn-outline-success"
                          >
                            Adicionar
                          </Button>
                        </Col>
                      </Row>
                    </AvForm>
                  </Container>
                </CardBody>
              </Card>
            </Col>
          </Row>
      </div>
    );
}