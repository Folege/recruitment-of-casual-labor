import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import logo from "assets/img/404_1.png"

export default function NotFound() {
    return (
      <div className="content">
        <Container>
          <Row className="mt-5">
            <Col xs={12} className="text-center">
              <h2>404 Página Não Encontrada</h2>
              <p>Não foi possível encontrar a página solicitada.</p>
              <img src={logo} alt="Logo da RESTIVA" className="mb-3" height="500px"/>
            </Col>
          </Row>
        </Container>
      </div>
    );
}