import React, { useState } from 'react';
import classnames from 'classnames';

import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    TabContent, 
    TabPane, 
    Nav, 
    NavItem, 
    NavLink,
} from "reactstrap";

import EmpresasContainer from 'components/AdminComp/EmpresasContainer.jsx';
import TiposUsuariosContainer from 'components/AdminComp/TiposUsuariosContainer.jsx';

export default function Configuracoes() {
    //Tabs consts
    const [activeTab, setActiveTab] = useState('1');
    const toggle = tab => {
        if (activeTab !== tab) setActiveTab(tab);
    }


    return (
        <div className="content">
            <Row>
                <Col xs={12}>
                    <Card>
                        <CardHeader>
                            <Nav tabs>
                                <NavItem>
                                    <NavLink
                                        className={classnames({ active: activeTab === '1' })}
                                        onClick={() => { toggle('1'); }}
                                    >
                                        Empresas
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        className={classnames({ active: activeTab === '2' })}
                                        onClick={() => { toggle('2'); }}
                                    >
                                        Tipos de Usuários
                                    </NavLink>
                                </NavItem>
                            </Nav>
                        </CardHeader>
                        <CardBody>
                            <TabContent activeTab={activeTab}>
                                <TabPane tabId="1">
                                    <EmpresasContainer />
                                    
                                </TabPane>
                                <TabPane tabId="2">
                                    <TiposUsuariosContainer />
                                </TabPane>
                            </TabContent>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </div>
    );
};