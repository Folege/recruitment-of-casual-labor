import React, { useState ,useEffect} from "react";
import { Link ,useHistory} from "react-router-dom";
import { MdKeyboardBackspace } from "react-icons/md";
import UploadImages from "yagoubi-upload-images";
import jwt from "jsonwebtoken";
import Skeleton from "react-loading-skeleton";

import {
  AvForm,
  AvGroup,
  AvInput,
  AvFeedback,
  AvField,
} from "availity-reactstrap-validation";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Row,
  Col,
  Container,
  Label,
  FormText,
  Alert
} from "reactstrap";

import api from "services/api";

export default function Perfil() {
  //funcao que captura imagem
  const uploadImage = (files) => { 
    setFile(files[0]);
  };
  
  //estado dos cMPOS RELACIONADOS COM SENHA
  const [display, setDisplay] = useState(true);
  const handleMudarSenha = () => setDisplay(!display);

  const [primeiroNome,setPrimeiroNome]=useState('');
  const [apelido,setApelido]=useState('');
  const [email,setEmail]= useState('');
  const [tipoNome,setTipoNome]= useState('');
  const [empresa,setEmpresa]= useState('');
  const [password,setPassword]= useState('');
  const [newPassword,setNewPassword]= useState('');
  const [url,setUrl]= useState('');
  const [file,setFile]= useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const token = localStorage.getItem("token");

  const history=useHistory()

  //State of Alerts
  const [alertVisible, setAlertVisible] = useState(false);
  const onDismiss = () => setAlertVisible(false);
  const [mensagemAlerta,setMensagemAlerta]=useState('Perfil actualizado com sucesso.');
  const [colorAlerta,setColorAlerta]=useState("success");

  useEffect(()=>{
    api.get("authUser", {
      headers: {
        "x-auth-token": token,
      },
      validateStatus: (status) => {
        return status;
      },
    })
    .then((response) => {
      if (response.status === 200) {
        const resultado = response.data.nome.split(" ", 2);
        setPrimeiroNome(resultado[0]);
        setApelido(resultado[1]);
        setEmail(response.data.email);
        setTipoNome(response.data.designacao);
        setEmpresa(response.data.empresa);
        setUrl(response.data.url);
        setIsLoaded(true);
      } else console.log(response.data);
    });
  },[token])

  async function handleUpdateUsuario(e){
    e.preventDefault();
    console.log(password,newPassword)
    const decoded = jwt.verify(token,'secretRestivaToken');
    var nome=primeiroNome+' '+apelido;
    
    let data;

    //verify if the fields to change password are visible
    if(display){
      data={
        nome,
        email,
        userName:primeiroNome.toLowerCase()+'.'+apelido.toLowerCase()
      }
    }else{
      data={
        nome,
        email,
        password,
        newPassword,
        userName:primeiroNome.toLowerCase()+'.'+apelido.toLowerCase()
      }
    }
    if(file){
      const Filedata = new FormData();
      Filedata.append('file',file);
      api.put(`/upload/${decoded.user.id_Ficheiro}`,Filedata,{
        headers: {
          "x-auth-token": token,
        },
      });
    }
      
    api.put(`usuario/${decoded.user.id}`,data,{
      headers: {
        "x-auth-token": token,
      },
      validateStatus:(status)=>{
        return status;
      }
    }).then(response=>{
      if(response.status===200){
        setMensagemAlerta(`Perfil actualizado com sucesso.`);
        setColorAlerta('success');
      }else{
          setMensagemAlerta(`Ocorreu um erro na actualização, tente novamente! ${response.data.message}`);
          setColorAlerta('danger');
      }
    },error=>{
      setMensagemAlerta(`Ocorreu um erro durante o cadastro, tente novamente! ${error}`);
      setColorAlerta('danger');
    });

    history.push('#alerta');
    setAlertVisible(true);
  }

  return (
    <>
      <div className="content">
        <Row>
          <Col md="4">
            <Card className="card-user">
              <div className="image">
                <img
                  alt="Porto de Maputo"
                  src={require("assets/img/mpdc.jpg")}
                />
              </div>
              <CardBody>
                {isLoaded ? (
                  <>
                    <div className="author">
                      <a href="#pablo" onClick={(e) => e.preventDefault()}>
                        <img
                          alt="..."
                          className="avatar border-gray"
                          src={url}
                          style={{
                            width: "120px",
                            height: "120px",
                            backgroundSize: "cover",
                          }}
                        />
                        <h5 className="title">
                          {primeiroNome + " " + apelido}
                        </h5>
                      </a>
                    </div>
                    <p className="description text-center">
                      Usuário:{" "}
                      {primeiroNome.toLowerCase() + "." + apelido.toLowerCase()}{" "}
                      <br />
                      Email: {email} <br />
                      Empresa: {empresa} <br />
                      Tipo de Usuário: {tipoNome}
                    </p>
                  </>
                ) : (
                  <>
                    <div className="author d-flex justify-content-center col-12">
                      <div className="avatar ">
                        <Skeleton circle={true} height={135} width={135} />
                      </div>
                    </div>
                    <h5 className="title">
                      <Skeleton className="mb-3" height={25} />
                    </h5>
                    <Skeleton height={15} count={4} />
                  </>
                )}
              </CardBody>
              <CardFooter>
                <hr />
              </CardFooter>
            </Card>
          </Col>

          <Col md={8}>
            <Row>
              <Alert
                id="alerta"
                className="col-12"
                color={colorAlerta}
                isOpen={alertVisible}
                toggle={onDismiss}
              >
                {mensagemAlerta}
              </Alert>
            </Row>
            <Card>
              <CardHeader>
                <CardTitle tag="h5">Actualizar Perfil</CardTitle>
              </CardHeader>
              <CardBody>
                <Container>
                  <AvForm onValidSubmit={handleUpdateUsuario}>
                    <Row>
                      <Col>
                        <strong>Dados Pessoais</strong>
                        <hr />
                      </Col>
                    </Row>

                    <Row>
                      <Col md={6}>
                        <AvGroup>
                          <Label for="primeiroNome">Primeiro Nome</Label>
                          <AvInput
                            name="primeiroNome"
                            id="primeiroNome"
                            value={primeiroNome}
                            onChange={(e) => setPrimeiroNome(e.target.value)}
                            required
                          />
                          <AvFeedback>Este campo é requerido.</AvFeedback>
                        </AvGroup>
                      </Col>
                      <Col md={6}>
                        <AvGroup>
                          <Label for="apelido">Apelido</Label>
                          <AvInput
                            name="apelido"
                            id="apelido"
                            value={apelido}
                            onChange={(e) => setApelido(e.target.value)}
                            required
                          />
                          <AvFeedback>Este campo é requerido.</AvFeedback>
                        </AvGroup>
                      </Col>
                    </Row>

                    <Row>
                      <Col md={6}>
                        <AvGroup>
                          <Label for="empresa">Empresa</Label>
                          <AvInput
                            name="empresa"
                            id="empresa"
                            value={empresa}
                            disabled
                          />
                          <AvFeedback>Este campo é requerido.</AvFeedback>
                        </AvGroup>
                      </Col>
                      <Col md={6}>
                        <AvGroup>
                          <AvField
                            type="select"
                            name="tipo"
                            label="Tipo"
                            disabled
                          >
                            <option>{tipoNome}</option>
                          </AvField>
                        </AvGroup>
                      </Col>
                    </Row>

                    <Row className="mt-5">
                      <Col sm={5} md={6}>
                        <UploadImages
                          onChange={(file) => uploadImage(file)}
                          color="#6BD098"
                        />
                        <hr />
                      </Col>
                    </Row>

                    <Row className="mt-5">
                      <Col>
                        <strong>Dados de Acesso</strong>
                        <hr />
                      </Col>
                    </Row>

                    <Row>
                      <Col md={6}>
                        <AvField
                          name="userName"
                          id="usuario"
                          type="text"
                          label="Nome de Usuário"
                          value={
                            primeiroNome.toLowerCase() +
                            "." +
                            apelido.toLowerCase()
                          }
                          disabled
                        />
                      </Col>
                      <Col md={6}>
                        <AvField
                          name="email"
                          id="email"
                          type="email"
                          label="E-mail"
                          value={email}
                          onChange={(e) => setEmail(e.target.value)}
                          validate={{
                            email: {
                              value: 3,
                              errorMessage:
                                "Por favor, forneça um endereço eletrônico válido.",
                            },
                            required: {
                              errorMessage: "Este campo é requerido.",
                            },
                          }}
                        />
                      </Col>
                    </Row>
                    <Row className="mt-2">
                      <Col>
                        <Button
                          className="btn btn-round btn-outline-primary"
                          onClick={handleMudarSenha}
                        >
                          Alterar Senha
                        </Button>
                      </Col>
                    </Row>

                    {display ? null : (
                      <>
                        <Row>
                          <Col>
                            <AvField
                              name="password"
                              id="senhaAntig"
                              label="Senha Antiga"
                              type="password"
                              onChange={(e) => setPassword(e.target.value)}
                              validate={{
                                minLength: {
                                  value: 8,
                                  errorMessage: "Senha invalida.",
                                },
                                required: {
                                  errorMessage: "Este campo é requerido.",
                                },
                              }}
                            />
                            <FormText>
                              <Link to="#">Esqueci minha senha</Link>
                            </FormText>
                          </Col>
                        </Row>
                        <Row style={{ display: display }}>
                          <Col md={6}>
                            <AvField
                              name="newPassword"
                              id="senha"
                              type="password"
                              label="Senha"
                              validate={{
                                minLength: {
                                  value: 8,
                                  errorMessage:
                                    "Senha invalida. Senha deve ter no mínimo 8 caracteres.",
                                },
                                required: {
                                  errorMessage: "Este campo é requerido.",
                                },
                              }}
                            />
                          </Col>
                          <Col md={6}>
                            <AvField
                              name="newPasswordConfirm"
                              id="csenha"
                              type="password"
                              label="Confirmar Senha"
                              onChange={(e) => setNewPassword(e.target.value)}
                              validate={{
                                match: {
                                  value: "newPassword",
                                  errorMessage:
                                    "Por favor, forneça o mesmo valor novamente.",
                                },
                                required: {
                                  errorMessage: "Este campo é requerido.",
                                },
                              }}
                            />
                          </Col>
                        </Row>
                      </>
                    )}

                    <Row>
                      <Col>
                        <Link
                          to="/admin/dashboard"
                          className="float-left mt-3"
                          style={{ color: "gray" }}
                        >
                          <MdKeyboardBackspace size={30} /> Cancelar
                        </Link>
                        <Button
                          type="submit"
                          className="float-right btn btn-round btn-outline-success"
                        >
                          Actualizar
                        </Button>
                      </Col>
                    </Row>
                  </AvForm>
                </Container>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );
}


