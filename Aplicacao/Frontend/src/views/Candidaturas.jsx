import React, { useState,useEffect } from "react";
import MUIDataTable from "mui-datatables";
import DataTableText from "components/AdminComp/DataTableText.js";
import Skeleton from "react-loading-skeleton";
import dateFormat from "dateformat";
import PropTypes from "prop-types";

import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import ChatIcon from "@material-ui/icons/Chat";
import IconButton from "@material-ui/core/IconButton";
import { useLocation, useHistory } from "react-router-dom";


import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Alert
} from "reactstrap";

import api from "../services/api";
import NotificarModal from "../components/AdminComp/NotificarModal.jsx";


export default function Candidaturas() {
  const [isLoadedP, setIsLoadedP] = useState(false);
  const [isLoadedC, setIsLoadedC] = useState(false);
  const [isLoadedCa, setIsLoadedCa] = useState(false);
  const [quantEstivas, setQuantEstivas] = useState(1);
  const [estivasCa, setEstivaCa] = useState([]);
  const [estivasC, setEstivaC] = useState([]); 
  const token = localStorage.getItem("token");
  //State of Modal component
  const [modal, setModal] = useState(false);
  const toggleModal = () => setModal(!modal);
  const [formValues, setFormValues] = useState("");
  const [idEstivas, setIdEstivas] = useState("");
  const history = useHistory();
  
  //State of Alerts
  const [alertVisible, setAlertVisible] = useState(false);
  const onDismiss = () => setAlertVisible(false);
  const [alertColor, setAlertColor] = useState("success");
  const [alertMessage, setAlertMessage] = useState("1 candidato foi notificado com sucesso.");

  //close notification modal
  const handleCancelarNotificar = () => {
    toggleModal();
    setQuantEstivas(1); //reinicia a quantidade de estivas selecionados
  }

  //pop up notification modal
  const handeleNotificar = (selectedRows, tableRows) => {
    setQuantEstivas(selectedRows.data.length);

    var idsArray = [];
    for (let i = 0; i < selectedRows.data.length; i++) {
      tableRows.map(row => {
        if (row.dataIndex === selectedRows.data[i].dataIndex)
          idsArray[i] = row.data[0];
        return "";
      })
    }

    setIdEstivas(idsArray);
    toggleModal();
  }

  const [estivasP,setEstivaP] = useState([]);
  useEffect(() => {
    api.get('list/Pendente',{
      headers: {
        "x-auth-token": token,
      } 
    }).then(response => {
      setEstivaP(response.data);
      setIsLoadedP(true);
    })

    api.get('list/Confirmada',{
      headers: {
        "x-auth-token": token,
      }
      
    }).then(response => {
      setEstivaC(response.data);
      setIsLoadedC(true);
    });

    api.get('list/Cancelada',{
      headers: {
        "x-auth-token": token,
      } 
    }).then(response => {
      setEstivaCa(response.data);
      setIsLoadedCa(true);
    })

    api.get('authUser', {
      headers: {
        "x-auth-token": token,
      }, validateStatus: (status) => {
        return status;
      },
    }).then(response => {
      if (response.status === 200) {
        setFormValues({
          empresa: response.data.empresa,
          endereco: response.data.endereco
        });
        
      }
    });
    
  },[token, alertMessage]);

    //Data for MuiDataTable Pendetes
    const colsPedentes = [{
        name: "id",
        options: {
          display: false,
          viewColumns: false,
        },
      },
      "Nome", 
      "Telefone", 
      "Numero de BI", 
      "Data de Candidatura"
    ];
    const rowsPedentes = estivasP.map((estivaP) => [
        estivaP.id,
        estivaP.nome,
        estivaP.telefone,
        estivaP.nr_BI,
        dateFormat(estivaP.dataCandidatura,"UTC:dd-mm-yyyy HH:MM:ss")
    ]);
    
    const optionsPedentes = {
      print: false,
      filter: false,
      textLabels: DataTableText.textLabels,
      sort: false,
      responsive: "stackedFullHeight",
      rowsPerPageOptions: [15,20,30, 50],
      customToolbarSelect: (selectedRows, dis) => (
        <Tooltip title="Notificar">
          <IconButton
            onClick={() => {
              handeleNotificar(selectedRows, dis);
            }}
          >
            <ChatIcon />
          </IconButton>
        </Tooltip>
      ),
      
    };

   
    //Data for MuiDataTable Confirmadas
  const colsConfirmadas = ["Nome", "Telefone", "Numero de BI", "Data de Notificação", "Data de Confirmação"];
    const rowsConfirmadas = estivasC.map((estivaC) => [    
      estivaC.nome,
      estivaC.telefone,
      estivaC.nr_BI,
      dateFormat(estivaC.dataNotificacao, "UTC:dd-mm-yyyy HH:MM:ss"),
      dateFormat(estivaC.dataConfirmacao,"UTC:dd-mm-yyyy HH:MM:ss")    
      ]);
      
    const optionsConfirmadas = {
      print: false,
      filter: false,
      selectableRows: false,
      textLabels: DataTableText.textLabels,
      sort: false,
      responsive: "stackedFullHeight",
      rowsPerPageOptions: [15, 20, 30, 50],
    };

    //Data for MuiDataTable Canceladas
    const colsCanceladas = ["Nome", "Telefone", "Numero de BI", "Data de Candidatura", "Data de Cancelamento"];
    const rowsCanceladas = estivasCa.map((estivaCa) => [
      estivaCa.nome,
      estivaCa.telefone,
      estivaCa.nr_BI,
      dateFormat(estivaCa.dataCandidatura,"UTC:dd-mm-yyyy HH:MM:ss"),
      dateFormat(estivaCa.dataCancelamento,"UTC:dd-mm-yyyy HH:MM:ss")
      ]);

    const optionsCanceladas = {
      print: false,
      filter: false,
      selectableRows: false,
      textLabels: DataTableText.textLabels,
      sort: false,
      responsive: "stackedFullHeight",
      rowsPerPageOptions: [15, 20, 30, 50],
    };

    //******************************************************** inicio dos tabs *******************************************/
    function TabPanel(props) {
      const { children, value, index, ...other } = props;

      return (
        <Typography
          component="div"
          role="tabpanel"
          hidden={value !== index}
          id={`full-width-tabpanel-${index}`}
          aria-labelledby={`full-width-tab-${index}`}
          {...other}
        >
          {value === index && <Box p={3}>{children}</Box>}
        </Typography>
      );
    }

    TabPanel.propTypes = {
      children: PropTypes.node,
      index: PropTypes.any.isRequired,
      value: PropTypes.any.isRequired,
    };

    function a11yProps(index) {
      return {
        id: `full-width-tab-${index}`,
        "aria-controls": `full-width-tabpanel-${index}`,
      };
    }

    const location = useLocation();
    const [value, setValue] = React.useState(location.state ? location.state : 0);

    const handleChange = (event, newValue) => {
      setValue(newValue);
    };

    const theme = createMuiTheme({
      palette: {
        primary: {
          main: "#6bd098",
        },
      },
    });
    //***************************************** fim dos tabs ********************************** */
    
    return (
      <div className="content">
        <Row>
          <Col lg="12">
            <Card>
              <CardHeader>
                <AppBar position="static" color="default">
                  <MuiThemeProvider theme={theme}>
                    <Tabs
                      value={value}
                      onChange={handleChange}
                      indicatorColor="primary"
                      textColor="primary"
                      variant="fullWidth"
                      aria-label="full width tabs example"
                    >
                      <Tab label="Pendetes" {...a11yProps(0)} />
                      <Tab label="Confirmadas" {...a11yProps(1)} />
                      <Tab label="Canceladas" {...a11yProps(2)} />
                    </Tabs>
                  </MuiThemeProvider>
                </AppBar>
              </CardHeader>

              <CardBody>
                <TabPanel value={value} index={0}>
                  <Container>
                    <hr style={{ border: "2px solid #51CBCE" }} />

                    <Row>
                      <Alert
                        className="col-12"
                        color={alertColor}
                        isOpen={alertVisible}
                        toggle={onDismiss}
                        id="alerta"
                      >
                        {alertMessage}
                      </Alert>
                    </Row>

                    <Row>
                      <Col id="tabela-overflow-x">
                        <div id="tabela-content">
                          {isLoadedP ? (
                            <MUIDataTable
                              title={"Candidaturas Pedentes"}
                              data={rowsPedentes}
                              columns={colsPedentes}
                              options={optionsPedentes}
                            />
                          ) : (
                            <div>
                              <Skeleton height={100} />
                              <Skeleton height={25} count={10} />
                            </div>
                          )}
                        </div>
                      </Col>
                    </Row>

                    <Row>
                      <Col>
                          <NotificarModal
                            modal={modal}
                            toggleModal={toggleModal}
                            quantEstivas={quantEstivas}
                            handleCancelarNotificar={handleCancelarNotificar}
                            setQuantEstivas={setQuantEstivas}
                            setAlertVisible={setAlertVisible}
                            setAlertColor={setAlertColor}
                            setAlertMessage={setAlertMessage}
                            formValues={formValues}
                            token={token}
                            idEstivas={idEstivas}
                            history={history}
                          />
                      </Col>
                    </Row>

                    </Container>
                </TabPanel>

                <TabPanel value={value} index={1}>
                  <Container id="tabela-overflow-x">
                    <hr style={{ border: "2px solid #6BD098" }} />
                    <div id="tabela-content">
                      {isLoadedC ? (
                        <MUIDataTable
                          title={"Candidaturas Confirmadas"}
                          data={rowsConfirmadas}
                          columns={colsConfirmadas}
                          options={optionsConfirmadas}
                        />
                      ) : (
                        <div>
                          <Skeleton height={100} />
                          <Skeleton height={25} count={10} />
                        </div>
                      )}
                    </div>
                  </Container>
                </TabPanel>
                <TabPanel value={value} index={2}>
                  <Container id="tabela-overflow-x">
                    <hr style={{ border: "2px solid #EF8157" }} />
                    <div id="tabela-content">
                      {isLoadedCa ? (
                        <MUIDataTable
                          title={"Candidaturas Canceladas"}
                          data={rowsCanceladas}
                          columns={colsCanceladas}
                          options={optionsCanceladas}
                        />
                      ) : (
                        <div>
                          <Skeleton height={100} />
                          <Skeleton height={25} count={10} />
                        </div>
                      )}
                    </div>
                  </Container>
                </TabPanel>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
}
