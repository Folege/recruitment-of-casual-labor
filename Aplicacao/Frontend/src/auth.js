import jwt from "jsonwebtoken";

class Auth {
    constructor() {
        this.autheticated = false;
    }

    login(token) {
        localStorage.setItem('token', token);
        localStorage.setItem('isAutheticated',true);
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.setItem('isAutheticated', false);
        window.location.replace('/login');
    }

    isAutheticated() {
        this.autheticated = localStorage.getItem('isAutheticated');
        if(this.autheticated == null || this.isTokenExpired()) 
            this.autheticated = false;
       
        return this.autheticated;
    }

    isTokenExpired() {
        const token = localStorage.getItem("token");
        var expired = false;
        jwt.verify(token, 'secretRestivaToken', function(err) {
            if (err) expired = true; // Manage different errors here (Expired, untrusted...)
        });
        return expired;
    }

}

export default new Auth();
