import React from "react";
import { Route } from 'react-router-dom';

import Dashboard from "views/Dashboard.jsx";
import Perfil from "views/Perfil.jsx";
import Candidaturas from "views/Candidaturas.jsx";
import ListUsuarios from "views/ListUsuarios.jsx";
import AddUsuario from "views/AddUsuario.jsx";
import Login from "views/Login.jsx";
import RecuperarSenha from "views/RecuperarSenha.jsx";
import NovaSenha from "views/NovaSenha.jsx";
import Configuracoes from "views/Configuracoes.jsx";


var routes = [
  {
    path: "/dashboard",
    name: "Painel Administrativo",
    icon: "nc-icon nc-bank",
    component: Dashboard,
    layout: "/admin",
    role: "PAINEL_ADMINISTRATIVO",
  },
  {
    path: "/candidaturas",
    name: "Candiadaturas",
    icon: "nc-icon nc-badge",
    component: Candidaturas,
    layout: "/admin",
    role: "GERIR_CANDIDATURAS",
  },
  {
    path: "/usuarios",
    name: "Usuários",
    icon: "nc-icon nc-single-02",
    component: ListUsuarios,
    layout: "/admin",
    role: "VISUALIZAR_USUARIOS",
  },
  {
    path: "/adicionar-novo",
    name: "Adicionar Novo",
    icon: "nc-icon nc-simple-add ml-4",
    component: AddUsuario,
    layout: "/admin",
    role:"GERIR_USUARIOS",
  },
  {
    path: "/perfil",
    name: "Seu Perfil",
    icon: "nc-icon nc-circle-10 ml-4",
    component: Perfil,
    layout: "/admin",
    role: "PADRAO",
  },
  {
    path: "/config",
    name: "Configurações",
    icon: "nc-icon nc-settings-gear-65",
    component: Configuracoes,
    layout: "/admin",
    role: "CONFIGURACOES",
  },
];

//default route shown when uploading them admin layout
export const RotaPadrao =[{
  path: "/perfil",
  name: "Seu Perfil",
  icon: "nc-icon nc-circle-10 ml-4",
  component: Perfil,
  layout: "/admin",
  role: "PADRAO",
}];

export const FrontRoutes = () => {
  return (
    <>
      <Route path="/login" exact component={Login} />
      <Route path="/esqueceu-senha" component={RecuperarSenha} />
      <Route path="/nova-senha/:passwordResetToken" component={NovaSenha} />
      
    </>
  );
}

export default routes;
