import React from "react";
import { Link } from "react-router-dom";
import Skeleton from "react-loading-skeleton";

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container,
} from "reactstrap";

import auth from "auth";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      dropdownOpen: false,
      color: "transparent",
    };
    this.toggle = this.toggle.bind(this);
    this.dropdownToggle = this.dropdownToggle.bind(this);
    this.sidebarToggle = React.createRef();
  }
  handleLogOut() {
    auth.logout();
  }
  toggle() {
    if (this.state.isOpen) {
      this.setState({
        color: "transparent"
      });
    } else {
      this.setState({
        color: "dark"
      });
    }
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  dropdownToggle(e) {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }
  getBrand() {
    let brandName = "404 Página Não encontrada";
    this.props.routes.map((prop, key) => {
      if (window.location.href.indexOf(prop.layout + prop.path) !== -1) {
        brandName = prop.name;
      }
      return null;
    });
    return brandName;
  }
  openSidebar() {
    document.documentElement.classList.toggle("nav-open");
    this.sidebarToggle.current.classList.toggle("toggled");
  }
  // function that adds color dark/transparent to the navbar on resize (this is for the collapse)
  updateColor() {
    if (window.innerWidth < 993 && this.state.isOpen) {
      this.setState({
        color: "dark"
      });
    } else {
      this.setState({
        color: "transparent"
      });
    }
  }
  componentDidMount() {
    window.addEventListener("resize", this.updateColor.bind(this));
  }
  componentDidUpdate(e) {
    if (
      window.innerWidth < 993 &&
      e.history.location.pathname !== e.location.pathname &&
      document.documentElement.className.indexOf("nav-open") !== -1
    ) {
      document.documentElement.classList.toggle("nav-open");
      this.sidebarToggle.current.classList.toggle("toggled");
    }
  }
  render() {
    return (
      // add or remove classes depending if we are on full-screen-maps page or not
      <Navbar
        color={
          this.props.location.pathname.indexOf("full-screen-maps") !== -1
            ? "dark"
            : this.state.color
        }
        expand="lg"
        className={
          this.props.location.pathname.indexOf("full-screen-maps") !== -1
            ? "navbar-absolute fixed-top"
            : "navbar-absolute fixed-top " +
              (this.state.color === "transparent" ? "navbar-transparent " : "")
        }
      >
        <Container fluid>
          <div className="navbar-wrapper">
            <div className="navbar-toggle">
              <button
                type="button"
                ref={this.sidebarToggle}
                className="navbar-toggler"
                onClick={() => this.openSidebar()}
              >
                <span className="navbar-toggler-bar bar1" />
                <span className="navbar-toggler-bar bar2" />
                <span className="navbar-toggler-bar bar3" />
              </button>
            </div>
            <NavbarBrand>{this.getBrand()}</NavbarBrand>
          </div>
          <NavbarToggler onClick={this.toggle}>
            <span className="navbar-toggler-bar navbar-kebab" />
            <span className="navbar-toggler-bar navbar-kebab" />
            <span className="navbar-toggler-bar navbar-kebab" />
          </NavbarToggler>
          {this.props.isLoaded ? (
            <Collapse
              isOpen={this.state.isOpen}
              navbar
              className="justify-content-end"
            >
              <Nav navbar>
                <Dropdown
                  nav
                  isOpen={this.state.dropdownOpen}
                  toggle={(e) => this.dropdownToggle(e)}
                >
                  <DropdownToggle caret nav>
                    <label className="mr-1">
                      <b className="text-danger">{this.props.empresa}</b> |{" "}
                    </label>
                    <img
                      src={this.props.imageUrl}
                      alt=""
                      className="rounded-circle img-fluid mr-1 zoomUsuarioLogado"
                      style={{
                        width: "30px",
                        height: "30px",
                        backgroundSize: "cover",
                      }}
                    />
                    <p>
                      <span className="d-md-block">{this.props.userName}</span>
                    </p>
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem tag="a" style={{ paddingRight: "0px" }}>
                      <Link
                        to="/admin/perfil"
                        style={{
                          color: "inherit",
                          textDecoration: "none",
                          paddingRight: "105px",
                        }}
                      >
                        Perfil
                      </Link>
                    </DropdownItem>
                    <DropdownItem
                      tag="a"
                      style={{ paddingRight: "0px" }}
                      onClick={this.handleLogOut}
                    >
                      Sair
                    </DropdownItem>
                  </DropdownMenu>
                </Dropdown>
              </Nav>
            </Collapse>
          ) : (
            <Collapse
              isOpen={this.state.isOpen}
              navbar
              className="justify-content-end"
            >
              <div className="col-sm-4">
                <Skeleton height={25} />
              </div>
            </Collapse>
          )}
        </Container>
      </Navbar>
    );
  }
}

export default Header;
