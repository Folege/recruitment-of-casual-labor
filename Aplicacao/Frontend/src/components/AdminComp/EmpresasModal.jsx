import React from 'react';
import { AvForm, AvField,} from "availity-reactstrap-validation";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col } from 'reactstrap';
import api from 'services/api';

export default function EmpresasModal(props) {
    
  const token = localStorage.getItem("token");

  async function handleSubmit (event, values) {
    props.setMensagemAlerta(''); //garante que o metodo useeffect vai sempre renderizar de novo em empresasContainer
    if(props.idEmpresa) {
      values.id = props.idEmpresa;
      await api.put('empresa', values, {
        headers: {
          "x-auth-token": token,
        },
        validateStatus: (status) => {
          return status;
        }
      }).then(response => {
        if (response.status === 200) {
          props.setColorAlerta('success');
        } else {
          props.setColorAlerta("danger");
        }
        props.setMensagemAlerta(response.data.message);
        props.setVisibleAlerta(true);
        props.history.push("#alerta");
      }, (error) => {
        props.setMensagemAlerta(`Tente novamente, ocorreu o seguinte erro no cadastro: ${error.message}`);
        props.setColorAlerta("danger");
        props.setVisibleAlerta(true);
      });
    } else {
      await api.post('empresa', values, {
        headers: {
          "x-auth-token": token,
        },
        validateStatus: (status) => {
          return status;
        }
      }).then(response => {
        if (response.status === 200) {
          props.setColorAlerta('success');
        } else {
          props.setColorAlerta("danger");
        }
        props.setMensagemAlerta(response.data.message);
        props.setVisibleAlerta(true);
        props.history.push("#alerta");
      }, (error) => {
        props.setMensagemAlerta(`Tente novamente, ocorreu o seguinte erro no cadastro: ${error.message}`);
        props.setColorAlerta("danger");
        props.setVisibleAlerta(true);
      });
    }  
    props.toggleEmpresas();
  }

    return (
      <Modal
        isOpen={props.modalEmpresas}
        toggle={props.toggleEmpresas}
        returnFocusAfterClose={false}
        backdrop="static"
        style={{ maxWidth: "800px" }}
      >
        <ModalHeader toggle={props.toggleEmpresas}>
          {props.titleEmpresas}
        </ModalHeader>
        <ModalBody>
          <AvForm
            onValidSubmit={handleSubmit}
            model={props.formValues}
            id="formulario"
          >
            <Row>
              <Col>
                <AvField
                  name="nome"
                  id="nome"
                  type="text"
                  label="Nome da Empresa"
                  validate={{
                    required: {
                      errorMessage: "Este campo é requerido.",
                    },
                  }}
                />
              </Col>
            </Row>
            <Row className="mt-3">
              <Col>
                <span>Endereço</span>
                <hr />
              </Col>
            </Row>
            <Row>
              <Col md="4">                
                <AvField
                  name="cidade"
                  id="cidade"
                  list="cidades"
                  label="Cidade"
                  minlength="2" 
                  validate={{
                    required: {
                      errorMessage: "Este campo é requerido.",
                    },
                  }}
                />
                <datalist id="cidades">
                  <option value="Maputo">Maputo</option>
                  <option value="Matola">Matola</option>
                  <option value="Nampula">Nampula</option> 
                  <option value="Beira">Beira </option>
                  <option value="Nacala">Nacala</option>  
                </datalist> 
              </Col>
              <Col md="5">
                <AvField
                  name="avRua"
                  id="avRua"
                  type="text"
                  label="Av/Rua"
                  validate={{
                    required: {
                      errorMessage: "Este campo é requerido.",
                    },
                  }}
                />
              </Col>
              <Col md="3">
                <AvField
                  name="numeroEdificio"
                  id="numeroEdificio"
                  type="number"
                  label="Número do Edifício"
                  validate={{
                    max: {
                      value: "999999999",
                      errorMessage: "valor invalido.",
                    },
                  }}
                />
              </Col>

              <Col xs="12" className="mt-2">
                <AvField
                  name="infoAdicional"
                  id="infoAdicional"
                  type="text"
                  label="Informação Adicional"
                  placeholder="Informação que ajude a localizar a empresa."
                />
              </Col>
            </Row>
          </AvForm>
        </ModalBody>
        <ModalFooter>
          <Button type="submit" form="formulario" color="primary">
            Adicionar
          </Button>{" "}
          <Button color="secondary" onClick={props.toggleEmpresas}>
            Cancelar
          </Button>
        </ModalFooter>
      </Modal>
    );
}