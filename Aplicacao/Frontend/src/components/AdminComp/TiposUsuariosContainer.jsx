import React, { useState,useEffect} from 'react';
import MUIDataTable from "mui-datatables";
import { MdBorderColor,MdDelete} from "react-icons/md";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import { useHistory } from "react-router-dom";
import Skeleton from "react-loading-skeleton";

import {
    Row,
    Col,
    Container,
    Button,
    Badge,
    Alert
} from "reactstrap";

import DataTableText from "components/AdminComp/DataTableText.js";
import TiposUsuariosModal from "components/AdminComp/TiposUsuariosModal.jsx";
import api from "../../services/api";
import jwt from "jsonwebtoken";

export default function TiposUsuariosContainer() {
    //Modal Tipo de Usario States
    const [modalTiposUsuarios, setModalTiposUsuarios] = useState(false);
    const toggleTiposUsuarios = () => setModalTiposUsuarios(!modalTiposUsuarios);
    const [titleTiposUsuarios, setTitleTiposUsuarios] = useState("Adicionar Nova");
    const [idTiposUsuarios, setIdTiposUsuarios] = useState("");
    const [tipoUsuarios, setTipoUsuarios] = useState([]);
    const token = localStorage.getItem("token");

    const [formValues, setFormValues] = useState({});

    //alert states
    const [visibleAlerta, setVisibleAlerta] = useState(false);
    const onDismiss = () => {setVisibleAlerta(false);setMensagemAlerta("");}
    const [mensagemAlerta, setMensagemAlerta] = useState("");
    const [colorAlerta, setColorAlerta] = useState("success");
    const [isLoaded, setIsLoaded] = useState(false);
    const decoded = jwt.verify(token, "secretRestivaToken");
    const idTipo = decoded.user.id_tipo;

    const history = useHistory();
   
    useEffect(() => {
        api.get("userTipeList", {
          headers: {
            "x-auth-token": token,
          },
        })
        .then((response) => {
          setTipoUsuarios(response.data);
          setIsLoaded(true);
        });

    },[mensagemAlerta,token,idTiposUsuarios,idTipo]);

    //Consts for MuiDataTable
    const cols = [
        {
            name: "id",
            options: {
              display: false,
              viewColumns: false,
            },
        },
        "Designação",
        "Funcionalidades Atribuídas / Roles",
        "Editar"
    ];

    DataTableText.textLabels.selectedRows.delete = "Apagar";
    const options = {
        download: false,
        print: false,
        filter: false,
        textLabels: DataTableText.textLabels,
        sort: false,
        responsive: "stackedFullHeight",
        rowsPerPageOptions: [15, 20, 30, 50],

        customToolbarSelect: (selectedRows,dis, set) => (
            <div>
              <Tooltip title="Apagar Conta">
                <IconButton
                  onClick={() => {
                    handleApagar(selectedRows,dis);
                  }}
                >
                  <MdDelete />
                </IconButton>
              </Tooltip>
            </div>
           
          ),
    };

    const handleAdicionarNovo = () => {
        setFormValues('');
        setTitleTiposUsuarios("Adicionar Novo");
        setIdTiposUsuarios(false);
        toggleTiposUsuarios();
    }

    const handleEditar =async (id) => {
        await api.get(`tipo/${id}`,{
            headers:{
                "x-auth-token": token, 
            },validateStatus: (status) => {
                return status;
            }
        })
        .then(response => {
            if(response.status === 200){
                const roles= response.data.roles.map(role=>role.designacao);
                setFormValues({
                    designacao:response.data.tipo.designacao,
                    roles
                });
            }
            
        });
            
        setTitleTiposUsuarios("Editar");
        setIdTiposUsuarios(id);
        toggleTiposUsuarios();
    }

    const handleApagar = (selectedRows, dis) => {

        for (let i = 0; i < selectedRows.data.length; i++) {
            const id = selectedRows.data[i].index;
            const idTipo =  dis[id].data[0];
            setIdTiposUsuarios(idTipo);
            api.delete(`tipo/${idTipo}`, {
                headers: {
                  "x-auth-token": token
                },
                validateStatus: (status) => {
                  return status;
                }
            })
            .then(response => {
              if(response.status===204){
                setColorAlerta('success');
                setMensagemAlerta("Tipo(s) de Usuario(s) removido com sucesso");
              }else{
                setMensagemAlerta(`Erro: ${response.data.message}`);
                setColorAlerta('danger');
              }
            }, (error) => {
                setMensagemAlerta(`Erro: ${error.message}`);
                setColorAlerta('danger');
            });
            setVisibleAlerta(true);
            history.push("#alerta");
        
        }
        
    }

    return (
      <Container>
        <Row>
          <Col xs="12">
            <Row>
              <Col xs="12">
                <Alert
                  id="alerta"
                  color={colorAlerta}
                  isOpen={visibleAlerta}
                  toggle={onDismiss}
                >
                  {mensagemAlerta}
                </Alert>
              </Col>
              <Col xs="12">
                <Button
                  className="btn btn-round btn-outline-success"
                  onClick={handleAdicionarNovo}
                >
                  Adicionar Novo
                </Button>
              </Col>
            </Row>
            <Row>
              <Col id="tabela-overflow-x">
                <div id="tabela-content">
                  {isLoaded ? (
                    <MUIDataTable
                      title={"Tipos de Usuários"}
                      
                      data= {tipoUsuarios.map((userType) => [
                          userType.tipo.id,
                          userType.tipo.designacao,
                          <div>
                              {
                                userType.roles.map((Type) => [ 
                                  <Badge color="secondary">{Type.roles}</Badge> 
                              ])
                              }
                              
                          </div>, 
                          userType.tipo.designacao === "Root"||userType.tipo.designacao === "Administrador" ? (
                            <MdBorderColor
                              size={20}
                              color="#5f5f5e7d"
                              style={{ cursor: "not-allowed" }}
                            />
                          ) : (
                            <MdBorderColor
                              size={20}
                              color="#0000FF"
                              onClick={(e) => handleEditar(userType.tipo.id)}
                              style={{ cursor: "pointer" }}
                              
                            />
                          ),
                        ])}
                      columns={cols}
                      options={options}
                  />
                  ) : (
                    <div>
                      <Skeleton height={100} />
                      <Skeleton height={25} count={10} />
                    </div>
                  )}
                </div>
              </Col>
            </Row>
            <Row>
              <Col>
                <TiposUsuariosModal
                  modalTiposUsuarios={modalTiposUsuarios}
                  toggleTiposUsuarios={toggleTiposUsuarios}
                  titleTiposUsuarios={titleTiposUsuarios}
                  idTiposUsuarios={idTiposUsuarios}
                  setMensagemAlerta={setMensagemAlerta}
                  setColorAlerta={setColorAlerta}
                  setVisibleAlerta={setVisibleAlerta}
                  history={history}
                  formValues={formValues}
                  setIdTiposUsuarios={setIdTiposUsuarios}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
} 