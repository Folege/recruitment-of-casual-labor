import React from 'react';
import { AvForm, AvField, AvCheckboxGroup, AvCheckbox } from "availity-reactstrap-validation";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col } from 'reactstrap';
import api from '../../services/api';

export default function TiposUsuariosModal(props) {

    const token = localStorage.getItem("token");

    function handleSubmit (event, values) {
        const data={
            designacao:values.designacao,
            roles:values.roles
        }

        if(props.idTiposUsuarios){
            submitUpdate(data)
        }else{
            submitCreate(data);
        }
        props.toggleTiposUsuarios();
    }

    async function submitUpdate(data){
        api.put(`tipo/${props.idTiposUsuarios}`,data,{
            headers: {
              "x-auth-token": token,
            },
            validateStatus:(status)=>{
              return status;
            }
          }).then(response => {
            if (response.status === 200) {
                props.setIdTiposUsuarios(0); // make useEffect reload data for datable
                props.setColorAlerta('success');
                props.setMensagemAlerta("Informacao do tipo de usuario actualizado com sucesso");
            } 
            else {
                props.setColorAlerta("danger");
                props.setMensagemAlerta(response.data.message);
            }
                
            props.setVisibleAlerta(true);
            props.history.push("#alerta");
        },(error) => {
            props.setMensagemAlerta(`Tente novamente, Ocorreu o seguinte erro durnte a actualizacao: ${error.message}`);
            props.setColorAlerta("danger");
            props.setVisibleAlerta(true);
        });
    }

    async function submitCreate(data){

        api.post('/userTipe',data,{
            headers: {
              "x-auth-token": token,
            },
            validateStatus:(status)=>{
              return status;
            }
          }).then(response => {
            if (response.status === 200) {
                props.setIdTiposUsuarios(0); // make useEffect reload data for datable
                props.setColorAlerta('success');
            } else {
                props.setColorAlerta("danger");
            }
                props.setMensagemAlerta(response.data.message);
                props.setVisibleAlerta(true);
                props.history.push("#alerta");
        },(error) => {
            props.setMensagemAlerta(`Tente novamente, ocorreu o seguinte erro no cadastro: ${error.message}`);
            props.setColorAlerta("danger");
            props.setVisibleAlerta(true);
        });

    }

    return (
        <Modal 
            isOpen={props.modalTiposUsuarios} 
            toggle={props.toggleTiposUsuarios} 
            returnFocusAfterClose={false} 
            backdrop="static"
            style={{ maxWidth: "800px" }}
        >
            <ModalHeader toggle={props.toggleTiposUsuarios}>{props.titleTiposUsuarios}</ModalHeader>
            <ModalBody>
                <AvForm
                    onValidSubmit={handleSubmit}
                    model={props.formValues}
                    id="formulario"
                >
                    <Row>
                        <Col>
                            <AvField 
                                name="designacao"
                                id="designacao"
                                type="text"
                                label="Designação"
                                validate={{
                                    required: {
                                        errorMessage: "Este campo é requerido.",
                                    },
                                }}
                            />
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col>
                            <AvCheckboxGroup 
                                name="roles" 
                                label="Funcionalidades Atribuídas / Roles" 
                                validate={{
                                    required: {
                                        errorMessage: "Este campo é requerido.",
                                    },
                                }}
                            >
                                <AvCheckbox customInput label="PAINEL_ADMINISTRATIVO" value="PAINEL_ADMINISTRATIVO" />
                                <AvCheckbox customInput label="CONFIGURACOES" value="CONFIGURACOES" />
                                <AvCheckbox customInput label="GERIR_CANDIDATURAS" value="GERIR_CANDIDATURAS" />
                                <AvCheckbox customInput label="GERIR_USUARIOS" value="GERIR_USUARIOS" />
                                <AvCheckbox customInput label="VISUALIZAR_USUARIOS" value="VISUALIZAR_USUARIOS" />  
                            </AvCheckboxGroup>
                        </Col>
                    </Row>  
                </AvForm>
            </ModalBody>
            <ModalFooter>
                <Button type="submit"  form="formulario" color="primary">Adicionar</Button>{' '}
                <Button color="secondary" onClick={props.toggleTiposUsuarios}>Cancelar</Button>
            </ModalFooter>
        </Modal>
    );
}