import React from "react";

import {
  AvForm,
  AvGroup,
  AvInput,
  AvFeedback,
  AvField,
} from "availity-reactstrap-validation";

import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Label,
} from "reactstrap";

import api from "services/api";

export default function UsuariosModal(props) {

    async function handleSubmit(event, values) {
      const data = {
        nome: values.primeiroNome + " " + values.apelido,
        email: values.email,
        userName:
          values.primeiroNome.toLowerCase() +
          "." +
          values.apelido.toLowerCase(),
        tipoNome: values.tipo,
        estadoConta: values.estado,
        id_usuario: props.idUser,
      };

      await api.put("updateUser", data, {
          headers: {
            "x-auth-token": props.token,
          },
          validateStatus: (status) => {
            return status;
          },
        }).then(
          (response) => {
            if (response.status === 200) {
              props.setAlertColor("success");
            } else {
              props.setAlertColor("danger");
            }
            props.setIdUser(0); //make use effect change
            props.setMensagemAlerta(response.data.message);
            props.setAlertVisible(true);
            props.toggle(); //close modal
            props.history.push("#alerta");
          },
          (error) => {
            props.setMensagemAlerta(
              `Ocorreu um erro,tente novamente! ${error.message}`
            );
            props.setAlertColor("danger");
            props.setAlertVisible(true);
          }
        );
    }

    return (
      <Modal
        isOpen={props.modal}
        toggle={props.toggle}
        backdrop="static"
        returnFocusAfterClose="false"
        style={{ maxWidth: "800px" }}
      >
        <ModalHeader toggle={props.toggle}>Editar</ModalHeader>
        <ModalBody>
          <AvForm
            onValidSubmit={handleSubmit}
            model={props.formValues}
            id="formEditar"
          >
            <Row>
              <Col xs={12} className="text-center">
                <div className="author">
                  <span>
                    <img
                      alt="..."
                      className="avatar border-gray rounded-circle img-fluid"
                      src={props.formValues.imgSrc}
                      style={{
                        width: "150px",
                        height: "150px",
                        backgroundSize: "cover",
                      }}
                    />
                    <h5 className="title text-primary">
                      {props.formValues.primeiroNome + " " + props.formValues.apelido}
                    </h5>
                  </span>
                </div>
              </Col>
            </Row>
            <Row>
              <Col>
                <strong>Dados Pessoais</strong>
                <hr />
              </Col>
            </Row>

            <Row>
              <Col md={6}>
                <AvGroup>
                  <Label for="primeiroNome">Primeiro Nome</Label>
                  <AvInput name="primeiroNome" id="primeiroNome" required />
                  <AvFeedback>Este campo deve ser preechido.</AvFeedback>
                </AvGroup>
              </Col>
              <Col md={6}>
                <AvGroup>
                  <Label for="apelido">Apelido</Label>
                  <AvInput name="apelido" id="apelido" required />
                  <AvFeedback>Este campo deve ser preechido.</AvFeedback>
                </AvGroup>
              </Col>
            </Row>

            <Row>
              <Col md={6}>
                <AvGroup>
                  <Label for="empresa">Empresa</Label>
                  <AvInput name="empresa" id="empresa" disabled />
                  <AvFeedback>Este campo deve ser preechido.</AvFeedback>
                </AvGroup>
              </Col>
              <Col md={6}>
                <AvGroup>
                  <AvField type="select" name="tipo" label="Tipo">
                    {props.tipo.map((item) => {
                      return (
                        <option key={item.id} value={item.designacao}>
                          {item.designacao}
                        </option>
                      );
                    })}
                  </AvField>
                </AvGroup>
              </Col>
            </Row>

            <Row className="mt-5">
              <Col>
                <strong>Dados de Acesso</strong>
                <hr />
              </Col>
            </Row>

            <Row>
              <Col md={6}>
                <AvField
                  name="usuario"
                  id="usuario"
                  type="text"
                  label="Nome de Usuário"
                  disabled
                  placeholder="primeironome.apelido"
                  validate={{
                    minLength: {
                      value: 3,
                      errorMessage: "Por favor, forneça ao menos 3 caracteres.",
                    },
                    required: {
                      errorMessage: "Este campo deve ser preechido.",
                    },
                  }}
                />
              </Col>
              <Col md={6}>
                <AvField
                  name="email"
                  id="email"
                  type="email"
                  label="E-mail"
                  validate={{
                    email: {
                      value: 3,
                      errorMessage:
                        "Por favor, forneça um endereço eletrônico válido.",
                    },
                    required: {
                      errorMessage: "Este campo deve ser preechido.",
                    },
                  }}
                />
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <AvField type="select" name="estado" label="Estado da Conta">
                  <option value="Activa">Activa</option>
                  <option value="Bloqueada">Bloqueada</option>
                </AvField>
              </Col>
            </Row>
          </AvForm>
        </ModalBody>
        <ModalFooter>
          <Button color="success" type="submit" form="formEditar">
            Confirmar
          </Button>
          <Button color="secondary" onClick={props.toggle}>
            Cancelar
          </Button>
        </ModalFooter>
      </Modal>
    );
}