import React, { useState, useEffect } from 'react';
import MUIDataTable from "mui-datatables";
import { MdBorderColor, MdDelete } from "react-icons/md";
import { useHistory } from "react-router-dom";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Skeleton from "react-loading-skeleton";

import {
    Row,
    Col,
    Container,
    Button,
    Alert
} from "reactstrap";

import DataTableText from "components/AdminComp/DataTableText.js";
import EmpresasModal from "components/AdminComp/EmpresasModal.jsx";
import api from "services/api";

export default function EmpresasContainer() {
    //Modal Empresa States
    const [modalEmpresas, setModalEmpresas] = useState(false);
    const toggleEmpresas = () => setModalEmpresas(!modalEmpresas);
    const [titleEmpresas, setTitleEmpresas] = useState("Adicionar Nova");
    const [idEmpresa, setIdEmpresa] = useState("");
    const [empresas, setEmpresas] = useState([]);
    const [formValues, setFormValues] = useState("");

    //alert states
    const [visibleAlerta, setVisibleAlerta] = useState(false);
    const onDismiss = () => setVisibleAlerta(false);
    const [mensagemAlerta, setMensagemAlerta] = useState("");
    const [colorAlerta, setColorAlerta] = useState("success");
    const [isLoaded, setIsLoaded] = useState(false);

    const history = useHistory();
    const token = localStorage.getItem("token");


    //Consts for MuiDataTable
    const cols = [
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
        },
      },
      "Nome",
      "Rua/Av",
      "Número do Edifício",
      "Cidade",
      "Informação Adicional",
      "Editar",
    ];

    DataTableText.textLabels.selectedRows.delete = "Apagar";
    const options = {
        download: false,
        print: false,
        filter: false,
        textLabels: DataTableText.textLabels,
        sort: false,
        responsive: "stackedFullHeight",
        rowsPerPageOptions: [15, 20, 30, 50],

        customToolbarSelect: (selectedRows,dis, set) => (
          <div>
            <Tooltip title="Apagar Empresa(s)">
              <IconButton
                onClick={() => {
                  handleApagar(selectedRows,dis);
                }}
              >
                <MdDelete />
              </IconButton>
            </Tooltip>
          </div>
         
        ),
    };

    useEffect(() => {
        api.get("empresa", {
            headers: {
                "x-auth-token": token,
            },
        })
        .then((response) => {
            setEmpresas(response.data);
            setIsLoaded(true);
        });

    },[mensagemAlerta, token, idEmpresa]);

    const handleAdicionarNova = () => {
        setFormValues('');
        setTitleEmpresas("Adicionar Nova");
        setIdEmpresa(false);
        toggleEmpresas();
    }

    async function handleEditar(id) {
      await api.get(`getEmpresa/${id}`, {
        headers: {
            "x-auth-token": token,
        }
      }).then((response) => {
        setFormValues(response.data);
        setTitleEmpresas("Editar");
        setIdEmpresa(id);
        toggleEmpresas();
      }, (error) => {
        setMensagemAlerta(`Tente novamente, ocorreu o seguinte erro no cadastro: ${error.message}`);
        setColorAlerta("danger");
        setVisibleAlerta(true);
      });
      setTitleEmpresas("Editar");
      setIdEmpresa(id);
      toggleEmpresas();
    }

    const handleApagar = (selectedRows, dis) => {

      for (let i = 0; i < selectedRows.data.length; i++) {
          const id = selectedRows.data[i].index;
          const idEmp =  dis[id].data[0];
          
          api.delete(`deleteEmpresa/${idEmp}`, {
              headers: {
                "x-auth-token": token
              },
              validateStatus: (status) => {
                return status;
              }
          })
          .then(response => {
              setIdEmpresa(idEmp);// make useEffect method reload the table data
              setColorAlerta('success');
              setMensagemAlerta(response.data.message);
              setVisibleAlerta(true); 
          }, (error) => {
              setMensagemAlerta(`Ocorreu um erro,tente novamente! ${error.message}`);
              setColorAlerta('danger');
              setVisibleAlerta(true);
          });
          history.push("#alerta");
      }
      
    }

    return (
      <Container>
        <Row>
          <Col xs="12">
            <Row>
              <Col>
                <Alert
                  id="alerta"
                  className="col-12"
                  color={colorAlerta}
                  isOpen={visibleAlerta}
                  toggle={onDismiss}
                >
                  {mensagemAlerta}
                </Alert>
              </Col>
            </Row>
            <Row>
              <Col xs="12">
                <Button
                  className="btn btn-round btn-outline-success"
                  onClick={handleAdicionarNova}
                >
                  Adicionar Nova
                </Button>
              </Col>
            </Row>
            <Row>
              <Col id="tabela-overflow-x">
                <div id="tabela-content">
                  {isLoaded ? (
                    <MUIDataTable
                      title={"Empresas"}
                      data={empresas.map((empresa) => [
                        empresa.id,
                        empresa.nome,
                        empresa.avRua,
                        empresa.numeroEdificio,
                        empresa.cidade,
                        empresa.infoAdicional,
                        <MdBorderColor
                          size={20}
                          color="#0000FF"
                          onClick={(e) => handleEditar(empresa.id)}
                          style={{ cursor: "pointer" }}
                        />,
                      ])}
                      columns={cols}
                      options={options}
                    />
                  ) : (
                    <div>
                      <Skeleton height={100} />
                      <Skeleton height={25} count={10} />
                    </div>
                  )}
                </div>
              </Col>
            </Row>
            <Row>
              <Col>
                <EmpresasModal
                  modalEmpresas={modalEmpresas}
                  toggleEmpresas={toggleEmpresas}
                  titleEmpresas={titleEmpresas}
                  idEmpresa={idEmpresa}
                  setMensagemAlerta={setMensagemAlerta}
                  setColorAlerta={setColorAlerta}
                  setVisibleAlerta={setVisibleAlerta}
                  history={history}
                  formValues={formValues}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
} 