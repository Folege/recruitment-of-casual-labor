import React from 'react';
import jwt from "jsonwebtoken";
import { AvForm, AvField, } from "availity-reactstrap-validation";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label } from 'reactstrap';
import api from 'services/api';

export default function NotificarModal(props) {

    //handle notification events
    async function handleNotificar(event, values) {
        const decoded = jwt.verify(props.token, "secretRestivaToken");
        values.idUsuario = decoded.user.id;
        values.idEstivas = props.idEstivas;

        await api.post('notificar', values, {
            headers: {
                "x-auth-token": props.token,
            },
            validateStatus: (status) => {
                return status;
            }
        }).then(response => {
            if (response.status === 200) {
                props.setAlertColor('success');
                props.setAlertMessage("");; //force useefect to reload data
            } else {
                props.setAlertColor("danger");
            }
            props.setAlertMessage(response.data.message);
            props.setAlertVisible(true);
            props.history.push("#alerta");
        }, (error) => {
            props.setAlertMessage(`Tente novamente, ocorreu o seguinte erro: ${error.message}`);
            props.setAlertColor("danger");
            props.setAlertVisible(true);
        });

        props.toggleModal();
        props.setQuantEstivas(1); //reinicia a quantidade de estivas selecionados
    }

    return (
        <div>
            <Modal
                isOpen={props.modal}
                toggle={props.toggleModal}
                backdrop="static"
            >
                <ModalHeader toggle={props.toggleModal}>
                    Preencha o Formulário
                </ModalHeader>

                <ModalBody>
                    <AvForm
                        id="formNotificar"
                        onValidSubmit={handleNotificar}
                        model={props.formValues}
                    >
                        <FormGroup>
                            <Label>
                                <b>
                                    {" "}
                                    {props.quantEstivas} candidato(s) seleccionado(s){" "}
                                </b>
                                , Indique a <b>data</b> e <b>hora </b>a
                                comparecer para finalizar a candidatura.
                              </Label>
                        </FormGroup>

                        <AvField 
                            label="Data" 
                            type="date"
                            name="data"
                            id="data"
                            min={new Date()}
                            placeholder="date placeholder"
                            required
                            validate={{
                                required: {
                                    errorMessage: "Campo Obrigatório.",
                                },
                                min: {
                                    errorMessage: "Data invalida.", 
                                }
                            }}
                        />

                        <AvField
                            label="Hora"
                            type="time"
                            name="hora"
                            id="hora"
                            placeholder="time placeholder"
                            required
                            validate={{
                                required: {
                                    errorMessage: "Campo Obrigatório.",
                                },
                            }}
                        />

                        <AvField
                            label="Empresa"
                            type="text"
                            name="empresa"
                            id="empresa"
                            disabled
                        />

                        <AvField
                            label="Endereço"
                            type="textarea"
                            name="endereco"
                            id="endereco"
                            disabled
                        />
                    </AvForm>
                </ModalBody>

                <ModalFooter>
                    <Button
                        color="success"
                        type="submit"
                        form="formNotificar"
                    >
                        Confirmar
                    </Button>{" "}
                    <Button
                        color="secondary"
                        onClick={props.handleCancelarNotificar}
                    >
                        Cancelar
                          </Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}