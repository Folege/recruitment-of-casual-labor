//texto das Mui Datatabeles
export default {
    textLabels: {
    body: {
        noMatch: " Desculpe, nenhum registro correspondente foi encontrado ",
        toolTip: " Classificar ",
        columnHeaderTooltip: (column) => ` Classificar para ${column.label} `,
    },
    pagination: {
        next: " Próxima página ",
        previous: " Página anterior ",
        rowsPerPage: "Linhas por página: ",
        displayRows: " de ",
    },
    toolbar: {
        search: " Pesquisa ",
        downloadCsv: " Baixar no Formato Excel ",
        print: " Imprimir em PDF",
        viewColumns: " Visualizar colunas ",
        filterTable: " Tabela de filtros ",
    },
    filter: {
        all: " todos ",
        title: " FILTROS ",
        reset: " REDEFENIR ",
    },
    viewColumns: {
        title: " Mostrar colunas ",
        titleAria: " Mostrar / ocultar colunas da tabela ",
    },
    selectedRows: {
        text: " linha(s) selecionadas ",
        delete: " Notificar ",
        deleteAria: " Notificar candidatos selecionados ",
    },
    },
};
