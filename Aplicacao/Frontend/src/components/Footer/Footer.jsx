/*eslint-disable*/
import React from "react";
import { Container, Row } from "reactstrap";
// used for making the prop types of this component
import PropTypes from "prop-types";

class Footer extends React.Component {
  render() {
    return (
      <footer
        className={"footer" + (this.props.default ? " footer-default" : "")}
      >
        <Container fluid={this.props.fluid ? true : false}>
          <Row>
            <nav className="footer-nav">
              <div className="copyright">
                &copy; {1900 + new Date().getYear()} <a className="text-success" href="http://www.portmaputo.com/" target="_blank">Porto de Maputo</a>, Todos Direitos Reservados.
              </div>
            </nav>
            <div className="credits ml-auto">
              <div className="copyright">
                Feito à mão & com <i className="nc-icon nc-favourite-28" /> , pela Kakana Code
              </div>
            </div>
          </Row>
        </Container>
      </footer>
    );
  }
}

Footer.propTypes = {
  default: PropTypes.bool,
  fluid: PropTypes.bool
};

export default Footer;
