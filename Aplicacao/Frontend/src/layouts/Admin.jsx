import React from "react";
import jwt from "jsonwebtoken";
import { loadProgressBar } from "axios-progress-bar";

// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";
import { Switch } from "react-router-dom";

import DemoNavbar from "components/Navbars/DemoNavbar.jsx";
import Footer from "components/Footer/Footer.jsx";
import Sidebar from "components/Sidebar/Sidebar.jsx";
import { PrivateRoute } from "components/AdminComp/PrivateRoute.jsx";
import NotFound from "views/NotFound.jsx"

import routes from "routes.js";
import api from "services/api";
import auth from "auth";

var ps;
var validRoute;

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: "black",
      activeColor: "success",
      userName: "Nome Apelido",
      imageUrl: "",
      empresa:"",
      validRoutes: routes,
      isLoaded: false
    };
    this.mainPanel = React.createRef();
    loadProgressBar({ showSpinner: false }, api);
  }
  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.mainPanel.current);
      document.body.classList.toggle("perfect-scrollbar-on");
    }

    if (auth.isAutheticated()) {
      const token = localStorage.getItem("token");
      const decoded = jwt.verify(token, "secretRestivaToken");
      const idTipo = decoded.user.id_tipo;
      api.get(`getRoles/${idTipo}`, {
        headers: {
          "x-auth-token": token,
        },
        validateStatus: (status) => {
          return status;
        }
      }).then((response) => {
        var validRoutes;

        if (response.status === 200) {
          validRoutes = routes.filter(elem=> {
            validRoute = false;
            response.data.map((role) => {
              if (role.designacao === elem.role || elem.role === "PADRAO" || 
                (role.designacao === "GERIR_USUARIOS" && elem.role === "VISUALIZAR_USUARIOS")
              )
                validRoute = elem;
              return "";
            });

            if (validRoute)
              return validRoute;
            else 
              return "";
          });

        } else {
          validRoutes = routes.filter(function (elem, index, routes) {
            return elem.role === "PADRAO";
          });
        }

        this.setState({ validRoutes: validRoutes });
        this.setState({ isLoaded: true });
      }, (error) => {
        console.log(error.message);
      });

      api.get("authUser", {
        headers: {
          "x-auth-token": token,
        },
      }).then((response) => {
        this.setState({ userName: response.data.nome });
        this.setState({imageUrl: response.data.url});
        this.setState({empresa: response.data.empresa});
      }, (error) => {
        console.log(error.message);
      });
    }
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
      document.body.classList.toggle("perfect-scrollbar-on");
    }
  }
  componentDidUpdate(e) {
    if (e.history.action === "PUSH") {
      this.mainPanel.current.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
    }
  }
  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };
  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };
  render() {
    return (
      <div className="wrapper">
        <Sidebar
          {...this.props}
          routes={this.state.validRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
          isLoaded={this.state.isLoaded}
        />
        <div className="main-panel" ref={this.mainPanel}>
          <DemoNavbar
            {...this.props}
            empresa={this.state.empresa}
            imageUrl={this.state.imageUrl}
            userName={this.state.userName}
            routes={this.state.validRoutes}
            isLoaded={this.state.isLoaded}
          />
          <Switch>
            {this.state.validRoutes.map((prop, key) => {
              return (
                <PrivateRoute
                  path={prop.layout + prop.path}
                  component={prop.component}
                  key={key}
                />
              );
            })}
            <PrivateRoute path="/admin/*" component={NotFound} />
          </Switch>
          <Footer fluid />
        </div>
      </div>
    );
  }
}

export default Dashboard;
