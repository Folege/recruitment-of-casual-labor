import React from 'react';
import { loadProgressBar } from "axios-progress-bar";

import {
    Row,
    Col,
    Container,
    Card,
    Alert,
} from 'reactstrap';

import logoPorto from "assets/logo/logo_mpdc.png"
import api from "services/api";

export default function LoginTheme(props) {
    loadProgressBar({ showSpinner: false }, api);
    
    document.body.style.backgroundColor = "#F2F2F2";
    
    return (
      <Container>
        <Row className="justify-content-center mt-5">
          <Col xl={6} lg={7} md={9} sm={12} className="mr-1">
            <Alert color={props.alertColor} isOpen={props.alertVisible} toggle={props.alertOnDismiss}>
              {props.alertMessage}
            </Alert>
          </Col>
          <Col xl={6} lg={7} md={9} sm={12} className="ml-2">
            <Card>
              <Row>
                <Col
                  className="bg-verde-r p-5"
                  xs={4}
                  style={{
                    borderTopLeftRadius: "12px",
                    borderBottomLeftRadius: "12px",
                  }}
                >
                  <Row className="justify-content-center">
                    <h4 className="mt-5 mb-5 text-white">RESTIVA</h4>
                    <img
                      src={logoPorto}
                      alt="Logo do Porto de Maputo"
                      className="mb-5"
                    />
                  </Row>
                </Col>
                <Col className="" xs={8}>
                  {props.children}
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
        <Row className="justify-content-center mt-5">
          <Col
            xl={6}
            lg={7}
            md={9}
            sm={12}
            className="text-center"
            style={{ fontSize: "11px" }}
          >
            <p>
              &copy; {1900 + new Date().getYear()}{" "}
              <a
                className="text-success"
                rel="noopener noreferrer"
                href="http://www.portmaputo.com/"
                target="_blank"
              >
                Porto de Maputo
              </a>
              , Todos Direitos Reservados.
            </p>
            <p>
              Feito à mão & com <i className="nc-icon nc-favourite-28" /> , pela
              Kakana Code
            </p>
          </Col>
        </Row>
      </Container>
    );
    
}