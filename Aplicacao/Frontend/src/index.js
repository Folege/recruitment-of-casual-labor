import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/paper-dashboard.scss?v=1.1.0";
import "assets/demo/demo.css";
import "perfect-scrollbar/css/perfect-scrollbar.css";
import "axios-progress-bar/dist/nprogress.css";
import "assets/css/restiva-style.css";

import AdminLayout from "layouts/Admin.jsx";
import { FrontRoutes } from "routes";
import NotFound from "views/NotFound.jsx";

const hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route
        exact
        path="/admin"
        render={() => <Redirect to="/admin/dashboard" />}
      />
      <Route exact path="/" render={() => <Redirect to="/login" />} />
      <Route
        exact
        path="/nova-senha"
        render={() => <Redirect to="/esqueceu-senha" />}
      />
      <Route path="/admin" render={(props) => <AdminLayout {...props} />} />
      <FrontRoutes />
      <Route  component={NotFound} />
    </Switch>
  </Router>,
  document.getElementById("root")
);
