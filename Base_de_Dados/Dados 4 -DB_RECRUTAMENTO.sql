USE [DB_RECRUTAMENTO]
GO
SET IDENTITY_INSERT [dbo].[estiva] ON 

INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (1, N'jorge Matavele', N'123456987125I', 845263987, N'Pendente', CAST(N'2012-06-18' AS Date), CAST(N'2012-06-18' AS Date))
INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (2, N'jorge Matavele', N'123456987125I', 845263987, N'Pendente', CAST(N'2012-06-18' AS Date), CAST(N'2012-06-18' AS Date))
INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (3, N'jorge Matavele', N'123456987125I', 845263987, N'Pendente', CAST(N'2012-06-18' AS Date), CAST(N'2012-06-18' AS Date))
INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (4, N'jorge Matavele', N'123456987125I', 845263987, N'Pendente', CAST(N'2012-06-18' AS Date), CAST(N'2012-06-18' AS Date))
INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (5, N'jorge Matavele', N'123456987125I', 845263987, N'Pendente', CAST(N'2012-06-18' AS Date), CAST(N'2012-06-18' AS Date))
INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (6, N'jorge Matavele', N'123456987125I', 845263987, N'Cancelada', CAST(N'2012-06-18' AS Date), CAST(N'2012-06-18' AS Date))
INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (7, N'jorge Matavele', N'123456987125I', 845263987, N'Cancelada', CAST(N'2012-06-18' AS Date), CAST(N'2012-06-18' AS Date))
INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (8, N'jorge Matavele', N'123456987125I', 845263987, N'Cancelada', CAST(N'2012-06-18' AS Date), CAST(N'2012-06-18' AS Date))
INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (9, N'jorge Matavele', N'123456987125I', 845263987, N'Cancelada', CAST(N'2012-06-18' AS Date), CAST(N'2012-06-18' AS Date))
INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (10, N'jorge Matavele', N'123456987125I', 845263987, N'Pendente', CAST(N'2012-06-18' AS Date), CAST(N'2012-06-18' AS Date))
INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (11, N'jorge Matavele', N'123456987125I', 845263987, N'Cancelada', CAST(N'2012-06-18' AS Date), NULL)
INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (12, N'jorge Matavele', N'123456987125I', 845263987, N'Confirmada', CAST(N'2012-06-18' AS Date), NULL)
INSERT [dbo].[estiva] ([id], [nome], [nr_BI], [telfone], [estadoCandidatura], [dataCandidatura], [dataCancelamento]) VALUES (13, N'jorge Matavele', N'123456987125I', 845263987, N'Pendente', CAST(N'2012-06-18' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[estiva] OFF
SET IDENTITY_INSERT [dbo].[endereco] ON 

INSERT [dbo].[endereco] ([id], [rua], [bairro], [quarteirao], [numeroEdificio]) VALUES (1, N'av. 25 de julho', N'Baixa', 27, 23233)
SET IDENTITY_INSERT [dbo].[endereco] OFF
SET IDENTITY_INSERT [dbo].[empresa] ON 

INSERT [dbo].[empresa] ([id], [nome], [id_endereco]) VALUES (1, N'RGB', 1)
SET IDENTITY_INSERT [dbo].[empresa] OFF
SET IDENTITY_INSERT [dbo].[tipo] ON 

INSERT [dbo].[tipo] ([id], [designacao]) VALUES (1, N'AnalistaRh')
INSERT [dbo].[tipo] ([id], [designacao]) VALUES (2, N'Administrador')
SET IDENTITY_INSERT [dbo].[tipo] OFF
SET IDENTITY_INSERT [dbo].[ficheiro] ON 

INSERT [dbo].[ficheiro] ([id], [nomeImagem], [nomeImagemOriginal], [url]) VALUES (1, N'ba8056ba183e19e98bf5781094eab0b6-IMG_20200317_123902_2 - Copy.jpg', N'IMG_20200317_123902_2 - Copy.jpg', N'http://localhost:3333/files/ba8056ba183e19e98bf5781094eab0b6-IMG_20200317_123902_2 - Copy.jpg')
INSERT [dbo].[ficheiro] ([id], [nomeImagem], [nomeImagemOriginal], [url]) VALUES (2, N'4b2f879d9add8be002b9dd7505a4b1b2-IMG_20200317_123902_2 - Copy.jpg', N'IMG_20200317_123902_2 - Copy.jpg', N'http://localhost:3333/files/694bb21e362236385658567e9c7dd8b4-logo-mpdc.jpg')
INSERT [dbo].[ficheiro] ([id], [nomeImagem], [nomeImagemOriginal], [url]) VALUES (3, N'9fbaf3d0b934a391d5faf7fa1d7578e4-coc-th10-war-base-739x510.jpg', N'coc-th10-war-base-739x510.jpg', N'http://localhost:3333/files/694bb21e362236385658567e9c7dd8b4-logo-mpdc.jpg')
INSERT [dbo].[ficheiro] ([id], [nomeImagem], [nomeImagemOriginal], [url]) VALUES (4, N'e404255e8916ea3201cf046c3a05b649-IMG_20200317_123902_2 - Copy.jpg', N'IMG_20200317_123902_2 - Copy.jpg', N'http://localhost:3333/files/694bb21e362236385658567e9c7dd8b4-logo-mpdc.jpg')
INSERT [dbo].[ficheiro] ([id], [nomeImagem], [nomeImagemOriginal], [url]) VALUES (5, N'ImagemPadrao.png', N'ImagemPadrao.png', N'http://localhost:3333/files/694bb21e362236385658567e9c7dd8b4-logo-mpdc.jpg')
INSERT [dbo].[ficheiro] ([id], [nomeImagem], [nomeImagemOriginal], [url]) VALUES (6, N'novaImagem.png', N'IMG_20200317_123902_2 - Copy.jpg', N'http://localhost:3333/files/694bb21e362236385658567e9c7dd8b4-logo-mpdc.jpg')
INSERT [dbo].[ficheiro] ([id], [nomeImagem], [nomeImagemOriginal], [url]) VALUES (7, N'694bb21e362236385658567e9c7dd8b4-logo-mpdc.jpg', N'logo-mpdc.jpg', N'http://localhost:3333/files/694bb21e362236385658567e9c7dd8b4-logo-mpdc.jpg')
SET IDENTITY_INSERT [dbo].[ficheiro] OFF
SET IDENTITY_INSERT [dbo].[analistaRh] ON 

INSERT [dbo].[analistaRh] ([id], [nome], [email], [userName], [estadoConta], [password], [passwordResetToken], [passwordResetExpires], [id_empresa], [id_tipo], [id_Ficheiro]) VALUES (1, N'Jossias Mupandza', N'mupandzaj@gmail.com', N'jossias', N'Activa', N'$2a$10$1R/3lx16kIw9dcS2YHgKBOtJEprHRMVI9sve1f43q/6EvWbAEJzF.', NULL, NULL, 1, 2, 1)
INSERT [dbo].[analistaRh] ([id], [nome], [email], [userName], [estadoConta], [password], [passwordResetToken], [passwordResetExpires], [id_empresa], [id_tipo], [id_Ficheiro]) VALUES (2, N'mmm Moz', N'mupandzajc@gmail.com', N'jossias.mupandza', N'Bloqueada', N'$2a$10$BE2t61uBiQ03stOCfD.3E.UXr92X3gOwufB8Exnd4FakMj8bRHE/K', NULL, NULL, 1, 1, 2)
INSERT [dbo].[analistaRh] ([id], [nome], [email], [userName], [estadoConta], [password], [passwordResetToken], [passwordResetExpires], [id_empresa], [id_tipo], [id_Ficheiro]) VALUES (3, N'Arnaldo Matusse', N'arnaldo@gmail.com', N'arnaldo', N'Bloqueada', N'$2a$10$FQrcCRUkjOKwa4kNQo83xuEdFFNQuVyjElkjFC2YTNfdXXHKsyI1e', N'5fadf1377c29d0e0567f1ab779ee79a0daffff4f', CAST(N'2020-05-14T02:52:58.9400000' AS DateTime2), 1, 2, 1)
INSERT [dbo].[analistaRh] ([id], [nome], [email], [userName], [estadoConta], [password], [passwordResetToken], [passwordResetExpires], [id_empresa], [id_tipo], [id_Ficheiro]) VALUES (4, N'Arnaldo Matusse', N'arnaldo2@gmail.com', N'arnaldo2', N'Activa', N'$2a$10$dEcW6ugPgX9Uy11vHulhdeUE4v/2Y31umkOeAfGFTDbUDgLRVTiU2', NULL, NULL, 1, 1, 1)
INSERT [dbo].[analistaRh] ([id], [nome], [email], [userName], [estadoConta], [password], [passwordResetToken], [passwordResetExpires], [id_empresa], [id_tipo], [id_Ficheiro]) VALUES (5, N'jota mito', N'mito@gmail.com', N'jota.mito', N'Activa', N'$2a$10$DdPdqLhU5ptB1GjvNoMGJeRIUF2kwfBtJyPtpP7RMuFeUJN0TSogW', NULL, NULL, 1, 1, 3)
INSERT [dbo].[analistaRh] ([id], [nome], [email], [userName], [estadoConta], [password], [passwordResetToken], [passwordResetExpires], [id_empresa], [id_tipo], [id_Ficheiro]) VALUES (6, N'joao jose', N'joao@gmail.com', N'joao', N'Activa', N'$2a$10$LR5.Ys1fQEYCvFP3naZhkepuJAh3N4fmolEt1t9DCHIkown8RBtSe', NULL, NULL, 1, 1, 4)
INSERT [dbo].[analistaRh] ([id], [nome], [email], [userName], [estadoConta], [password], [passwordResetToken], [passwordResetExpires], [id_empresa], [id_tipo], [id_Ficheiro]) VALUES (7, N'Jossias Ricardo', N'lknnnnnnnnnn@tyftyftf.mm', N'mupandza@gmail.com', N'Activa', N'$2a$10$Nm0J5ZJ9Rnmd6HV/3m.pn.ui.pdiICF4Sz6WmPO6BVqVD5Bm0BYju', NULL, NULL, 1, 1, 5)
INSERT [dbo].[analistaRh] ([id], [nome], [email], [userName], [estadoConta], [password], [passwordResetToken], [passwordResetExpires], [id_empresa], [id_tipo], [id_Ficheiro]) VALUES (8, N'jjjj andre', N'jorgitto@gmail.com', N'andre', N'Activa', N'$2a$10$xa8EbaaLAoZ/8Ye6dQGsbO/TUfwQp5n5PHN9vlQnLhVwyLQ1y5Pdq', NULL, NULL, 1, 1, 6)
INSERT [dbo].[analistaRh] ([id], [nome], [email], [userName], [estadoConta], [password], [passwordResetToken], [passwordResetExpires], [id_empresa], [id_tipo], [id_Ficheiro]) VALUES (9, N'Marcos Bavo', N'bavo@gmail.com', N'marcos.bavo', N'Bloqueada', N'$2a$10$kAAVj3r8cCKsmU7LQBkOW.4IYHsxWD0rMX723PIpKV3rSNGDVGgl2', NULL, NULL, 1, 2, 7)
SET IDENTITY_INSERT [dbo].[analistaRh] OFF
INSERT [dbo].[analistaRh_estiva] ([id_estiva], [id_analistaRh], [dataNotificacao], [dataInducao], [mensagem]) VALUES (8, 1, CAST(N'2021-03-02T00:00:00.0000000' AS DateTime2), CAST(N'2022-03-02T00:00:00.0000000' AS DateTime2), N'eeeeeeeee')
INSERT [dbo].[analistaRh_estiva] ([id_estiva], [id_analistaRh], [dataNotificacao], [dataInducao], [mensagem]) VALUES (11, 1, CAST(N'2020-05-18T00:00:00.0000000' AS DateTime2), CAST(N'2012-06-18T00:00:00.0000000' AS DateTime2), N'aaaaaaaaaaaaa aaaaaaaa aaaaaaaaa')
INSERT [dbo].[analistaRh_estiva] ([id_estiva], [id_analistaRh], [dataNotificacao], [dataInducao], [mensagem]) VALUES (12, 1, CAST(N'2020-05-14T00:00:00.0000000' AS DateTime2), CAST(N'2012-06-18T00:00:00.0000000' AS DateTime2), N'bbbbbbbbbbbbb')
INSERT [dbo].[analistaRh_estiva] ([id_estiva], [id_analistaRh], [dataNotificacao], [dataInducao], [mensagem]) VALUES (13, 1, CAST(N'2020-05-14T00:00:00.0000000' AS DateTime2), CAST(N'2012-06-18T00:00:00.0000000' AS DateTime2), N'cccccccccc')
SET IDENTITY_INSERT [dbo].[knex_migrations] ON 

INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (1, N'20200401141553_create_estiva.js', 1, CAST(N'2020-05-26T16:52:05.1966667' AS DateTime2))
INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (2, N'20200406101253_create_endereco.js', 1, CAST(N'2020-05-26T16:52:05.2133333' AS DateTime2))
INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (3, N'20200406104544_create_empresa.js', 1, CAST(N'2020-05-26T16:52:05.2266667' AS DateTime2))
INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (4, N'20200406150218_create_tipo.js', 1, CAST(N'2020-05-26T16:52:05.2466667' AS DateTime2))
INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (5, N'20200406150357_create_Ficheiro.js', 1, CAST(N'2020-05-26T16:52:05.5466667' AS DateTime2))
INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (6, N'20200406150436_create_analistaRH.js', 1, CAST(N'2020-05-26T16:52:05.6066667' AS DateTime2))
INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (7, N'20200401141553_create_estiva.js', 1, CAST(N'2020-05-27T00:29:48.6866667' AS DateTime2))
INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (8, N'20200406101253_create_endereco.js', 1, CAST(N'2020-05-27T00:29:48.7000000' AS DateTime2))
INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (9, N'20200406104544_create_empresa.js', 1, CAST(N'2020-05-27T00:29:48.7166667' AS DateTime2))
INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (10, N'20200406150218_create_tipo.js', 1, CAST(N'2020-05-27T00:29:48.7300000' AS DateTime2))
INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (11, N'20200406150357_create_Ficheiro.js', 1, CAST(N'2020-05-27T00:29:48.7400000' AS DateTime2))
INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (12, N'20200406150436_create_analistaRH.js', 1, CAST(N'2020-05-27T00:29:48.7600000' AS DateTime2))
INSERT [dbo].[knex_migrations] ([id], [name], [batch], [migration_time]) VALUES (13, N'20200406153056_create_analistaRh_estiva.js', 1, CAST(N'2020-05-27T00:29:48.8066667' AS DateTime2))
SET IDENTITY_INSERT [dbo].[knex_migrations] OFF
SET IDENTITY_INSERT [dbo].[knex_migrations_lock] ON 

INSERT [dbo].[knex_migrations_lock] ([index], [is_locked]) VALUES (1, 0)
SET IDENTITY_INSERT [dbo].[knex_migrations_lock] OFF
