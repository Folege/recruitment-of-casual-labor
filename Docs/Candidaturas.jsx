import React, { useState } from "react";
import { MDBDataTable, MDBInput} from "mdbreact";

import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Nav,
  NavItem,
  NavLink,
  Button,
  TabContent,
  TabPane,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from "reactstrap";

export default function Candidaturas() {

  //State of nav tabs
  const [activeTab, setActiveTab] = useState("1");

  //State of dropdowns
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggleDropdown = () => setDropdownOpen(prevState => !prevState);

  //State of Modal component
  const [modal, setModal] = useState(false);
  const toggleModal = () => setModal(!modal);

  //State of Alerts
  const [alertVisible, setAlertVisible] = useState(false);
  const onDismiss = () => setAlertVisible(false);

  //State of checkboxes
  const [areAllChecked, setAreAllChecked] = useState(false);
  const [checked, setChecked] = useState([
    {
      id: 1,
      checked: false,
    },
    {
      id: 2,
      checked: false,
    },
    {
      id: 3,
      checked: false,
    }
  ]);

  //handle select all event
  const handleCheckAll = () => {
    setAreAllChecked(!areAllChecked);
  };

  const handleCheck = (id) => {
    let currentChecked = [...checked];

    currentChecked.map((checkbox, index) => {
      if (index + 1 === id) {
        checkbox.checked = !checkbox.checked;
      }

      return checkbox;
    });
    //alert(currentChecked[0].checked);
    setChecked(currentChecked);
  };

  /*const handleCheckSome = (num) => {
    const uncheckedIds = []; //array de ids para checkboxes nao selecionados
    for( var i=0; i<checked.length; i++) {
      if (!checked[i].checked) { 
        uncheckedIds.push(checked[i].id);
      }
    }
    if (num <= uncheckedIds.length) {
      for (i = 0; i < num; i++) {
        handleCheck(uncheckedIds[i]); 
      }
    } else {
      for (i = 0; i < num; i++) {
        handleCheck(uncheckedIds[i]);
      }
    }
  }*/

  //handle notification events
  const handleNotificar = () => {
    toggleModal();
    setAlertVisible(true);
  }


    //Data for MDBDataTable Pendetes
    const tabelaPendetes = {
      columns: [
        {
          label: " ",
          field: "check",
          sort: "asc",
          width: 10,
        },
        {
          label: "Nome",
          field: "nome",
          sort: "asc",
          width: 150,
        },
        {
          label: "Telefone",
          field: "telefone",
          sort: "asc",
          width: 150,
        },
        {
          label: "Numero de BI",
          field: "nBI",
          sort: "asc",
          width: 150,
        },
        {
          label: "Data",
          field: "data",
          sort: "asc",
          width: 150,
        },
        {
          label: "",
          field: "acao",
          sort: "asc",
          width: 150,
        },
      ],
      rows: [
        {
          check: (
            <MDBInput
              label=" "
              type="checkbox"
              id="checkbox2"
              checked={areAllChecked ? true : checked[0].checked}
              onClick={() => handleCheck(1)}
            />
          ),
          nome: "Jossias Mupandza",
          telefone: "847596321",
          nBI: "7896541236I",
          data: "02/03/2020",
          acao: (
            <Button
              className="btn-round"
              outline
              color="success"
              onClick={toggleModal}
            >
              Notificar
            </Button>
          ),
        },
        {
          check: (
            <MDBInput
              label=" "
              type="checkbox"
              id="checkbox3"
              checked={areAllChecked ? true : checked[1].checked}
              onClick={() => handleCheck(2)}
            />
          ),
          nome: "Gabriel Timba",
          telefone: "847596321",
          nBI: "7896541236I",
          data: "02/03/2020",
          acao: (
            <Button
              className="btn-round"
              onClick={toggleModal}
              outline
              color="success"
            >
              Notificar
            </Button>
          ),
        },
        {
          check: (
            <MDBInput
              label=" "
              type="checkbox"
              id="checkbox4"
              checked={areAllChecked ? true : checked[2].checked}
              onClick={() => handleCheck(3)}
            />
          ),
          nome: "Folege Ricardp",
          telefone: "847596321",
          nBI: "7896541236I",
          data: "02/03/2020",
          acao: (
            <Button
              className="btn-round"
              onClick={toggleModal}
              outline
              color="success"
            >
              Notificar
            </Button>
          ),
        },
      ],
    };

    //Data for MDBDataTable Confirmadas
    const tabelaConfirmadas = {
      columns: [
        {
          label: "Nome",
          field: "nome",
          sort: "asc",
          width: 150,
        },
        {
          label: "Telefone",
          field: "telefone",
          sort: "asc",
          width: 150,
        },
        {
          label: "Numero de BI",
          field: "nBI",
          sort: "asc",
          width: 150,
        },
        {
          label: "Data de Notificação",
          field: "notificacao",
          sort: "asc",
          width: 150,
        },
        {
          label: "Data Confirmação",
          field: "confirmacao",
          sort: "asc",
          width: 150,
        },
      ],
      rows: [
        {
          nome: "Jossias Mupandza",
          telefone: "847596321",
          nBI: "7896541236I",
          notificacao: "02/03/2020 - 12:00",
          confirmacao: "03/03/2020 - 14:00",
        },
        {
          nome: "Gabriel Timba",
          telefone: "847596321",
          nBI: "7896541236I",
          notificacao: "02/03/2020 - 12:00",
          confirmacao: "03/03/2020 - 14:00",
        },
        {
          nome: "Folege Ricardp",
          telefone: "847596321",
          nBI: "7896541236I",
          notificacao: "02/03/2020 - 12:00",
          confirmacao: "03/03/2020 - 14:00",
        },
      ],
    };

    //Data for 
    const tabelaCanceladas = {
      columns: [
        {
          label: "Nome",
          field: "nome",
          sort: "asc",
          width: 150,
        },
        {
          label: "Telefone",
          field: "telefone",
          sort: "asc",
          width: 150,
        },
        {
          label: "Numero de BI",
          field: "nBI",
          sort: "asc",
          width: 150,
        },
        {
          label: "Data de Notificação",
          field: "notificacao",
          sort: "asc",
          width: 150,
        },
        {
          label: "Data Cancelamento",
          field: "cancelamento",
          sort: "asc",
          width: 150,
        },
      ],
      rows: [
        {
          nome: "Jossias Mupandza",
          telefone: "847596321",
          nBI: "7896541236I",
          notificacao: "02/03/2020 - 12:00",
          cancelamento: "03/03/2020 - 14:00",
        },
        {
          nome: "Gabriel Timba",
          telefone: "847596321",
          nBI: "7896541236I",
          notificacao: "02/03/2020 - 12:00",
          cancelamento: "03/03/2020 - 14:00",
        },
        {
          nome: "Folege Ricardp",
          telefone: "847596321",
          nBI: "7896541236I",
          notificacao: "02/03/2020 - 12:00",
          cancelamento: "03/03/2020 - 14:00",
        },
      ],
    };

    //************************************************************ tabs *******************************************/
    function TabPanel(props) {
      const { children, value, index, ...other } = props;

      return (
        <Typography
          component="div"
          role="tabpanel"
          hidden={value !== index}
          id={`full-width-tabpanel-${index}`}
          aria-labelledby={`full-width-tab-${index}`}
          {...other}
        >
          {value === index && <Box p={3}>{children}</Box>}
        </Typography>
      );
    }

    TabPanel.propTypes = {
      children: PropTypes.node,
      index: PropTypes.any.isRequired,
      value: PropTypes.any.isRequired,
    };

    function a11yProps(index) {
      return {
        id: `full-width-tab-${index}`,
        "aria-controls": `full-width-tabpanel-${index}`,
      };
    }

    

    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
      setValue(newValue);
    };

    //***************************************** fim dos tabs ********************************** */
    
    return (
      <div className="content">
        <Row>
          <Col lg="12">
            <Card>
              <CardHeader>
                <AppBar position="static" color="default">
                  <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    aria-label="full width tabs example"
                  >
                    <Tab label="Pendetes 1" {...a11yProps(0)} />
                    <Tab label="Confirmadas 5" {...a11yProps(1)} />
                    <Tab label="Canceladas 8" {...a11yProps(2)} />
                  </Tabs>
                </AppBar>
              </CardHeader>

              <CardBody>
                
                  <TabPanel value={value} index={0} >
                    <Container id="tabela-overflow-x">
                      <Row>
                        <Alert
                          className="col-12"
                          color="success"
                          isOpen={alertVisible}
                          toggle={onDismiss}
                        >
                          1 candidato foi notificado com sucesso
                        </Alert>
                      </Row>

                      <Row className="mb-2">
                        <Col>
                          <Button
                            className="btn btn-round float-right"
                            outline
                            color="success"
                            onClick={toggleModal}
                          >
                            Notificar
                          </Button>

                          <Dropdown
                            isOpen={dropdownOpen}
                            toggle={toggleDropdown}
                            className="float-right"
                          >
                            <DropdownToggle
                              caret
                              className="btn btn-round btn-outline-info"
                            >
                              Selecionar
                            </DropdownToggle>
                            <DropdownMenu>
                              <DropdownItem>Selecionar 5</DropdownItem>
                              <DropdownItem>Selecionar 10</DropdownItem>
                              <DropdownItem>Selecionar 20</DropdownItem>
                              <DropdownItem onClick={handleCheckAll}>
                                Selecionar Todos/Nenhum
                              </DropdownItem>
                            </DropdownMenu>
                          </Dropdown>
                        </Col>
                      </Row>

                      <MDBDataTable
                        striped
                        bordered
                        hover
                        btn
                        sorting={false}
                        data={tabelaPendetes}
                      />

                      <div>
                        <Modal isOpen={modal} toggle={toggleModal}>
                          <ModalHeader toggle={toggleModal}>
                            Preencha o Formulário
                          </ModalHeader>

                          <ModalBody>
                            <Form>
                              <FormGroup>
                                <Label>
                                  Indique a <b>data</b> e <b>hora</b>, que o
                                  <b> candidato seleccionado</b> deve comparecer
                                  para finalizar a sua candidatura.
                                </Label>
                              </FormGroup>

                              <FormGroup>
                                <Label for="dataNotificacao">Data</Label>
                                <Input
                                  type="date"
                                  name="dataNotificacao"
                                  id="dataNotificacao"
                                  placeholder="date placeholder"
                                />
                              </FormGroup>

                              <FormGroup>
                                <Label for="horaNotificacao">Hora</Label>
                                <Input
                                  type="time"
                                  name="horaNotificacao"
                                  id="horaNotificacao"
                                  placeholder="time placeholder"
                                />
                              </FormGroup>

                              <FormGroup>
                                <Label for="localNotificacao">Local</Label>
                                <Input
                                  type="textarea"
                                  name="localNotificacao"
                                  id="localNotificacao"
                                  disabled
                                  placeholder="EMPRESA: Manica, &#10;ENDEREÇO: xxxx,xxxx,xxx,x,xx. "
                                />
                              </FormGroup>
                            </Form>
                          </ModalBody>

                          <ModalFooter>
                            <Button color="primary" onClick={handleNotificar}>
                              Confirmar
                            </Button>{" "}
                            <Button color="secondary" onClick={toggleModal}>
                              Cancelar
                            </Button>
                          </ModalFooter>
                        </Modal>
                      </div>
                    </Container>
                  </TabPanel>
                  <TabPanel value={value} index={1} >
                    <Container id="tabela-overflow-x">
                      <MDBDataTable
                        striped
                        bordered
                        hover
                        btn
                        sorting={false}
                        data={tabelaConfirmadas}
                      />
                    </Container>
                  </TabPanel>
                  <TabPanel value={value} index={2} >
                    <Container  id="tabela-overflow-x">
                      <MDBDataTable
                        striped
                        bordered
                        hover
                        btn
                        sorting={false}
                        data={tabelaCanceladas}
                      />
                    </Container>
                  </TabPanel>

                <Nav tabs>
                  <NavItem>
                    <NavLink
                      className={activeTab === "1" ? "active" : ""}
                      onClick={() => setActiveTab("1")}
                    >
                      Pedentes
                    </NavLink>
                  </NavItem>

                  <NavItem>
                    <NavLink
                      className={activeTab === "2" ? "active" : ""}
                      onClick={() => setActiveTab("2")}
                    >
                      Confirmadas
                    </NavLink>
                  </NavItem>

                  <NavItem>
                    <NavLink
                      className={activeTab === "3" ? "active" : ""}
                      onClick={() => setActiveTab("3")}
                    >
                      Canceladas
                    </NavLink>
                  </NavItem>
                </Nav>

                <TabContent activeTab={activeTab}>
                  <TabPane tabId="1">
                    {/*Por Fazer: mudar idioma*, fazer opcoes de sellecao funcionarem, colocar pop up para confirmar notificao e
                      alertas para notificacoes sem selecao
                     */}
                  </TabPane>

                  <TabPane tabId="2"></TabPane>

                  <TabPane tabId="3"></TabPane>
                </TabContent>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );

}
